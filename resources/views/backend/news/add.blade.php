@extends('backend.master.master')
@section('title')
    Thêm bài viết
@endsection
@section('main')
    <div class="page-wrapper">
        <div class="page-content">
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Thêm bài viết</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="card">
                <div class="card-body p-4">
                    <div class="form-body mt-4">
                        <form method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="border border-3 p-4 rounded">
                                        <div class="mb-3">
                                            <label for="inputProductTitle" class="form-label">Tên bài viết</label>
                                            <input type="text" name="name" class="form-control" id="inputProductTitle"
                                                placeholder="Bắt buộc" value="{{old('name')}}">
                                        </div>
                                        {!! show_errors_request($errors, 'name') !!}
                                        <div class="mb-3">
                                            <label for="inputProductDescription" class="form-label">Giới thiệu</label>
                                            <textarea class="form-control ckeditor" id="description" rows="3"
                                                name="info">{{ old('info') }}</textarea>
                                        </div>
                                        {!! show_errors_request($errors, 'info') !!}
                                        <div class="row mb-3">
                                            <label class="col-form-label">Danh mục <span class="requiredLable">*</span></label>
                                            <select class="form-select" name="cat_id">
                                                @foreach ($categories as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {!! show_errors_request($errors, 'cat_id') !!}
                                        <div class="row mb-3">
                                            <label class="col-form-label">Tags <span class="requiredLable">*</span></label>
                                            <select id="people" class="form-select" name="tags[]" multiple>
                                                @foreach ($tags as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {!! show_errors_request($errors, 'tags') !!}
                                        <div class="mb-3">
                                            <label for="inputProductDescription" class="form-label">Mô tả</label>
                                            <textarea class="form-control ckeditor" id="description" rows="3"
                                                name="describe">{{ old('describe') }}</textarea>
                                        </div>
                                        {!! show_errors_request($errors, 'describe') !!}
                                        <div class="mb-3">
                                            <label for="inputProductType" class="form-label">Active</label>
                                            <select class="form-select" id="inputProductType" name="active">
                                                <option value="">--Chọn--</option>
                                                <option value="1">Có</option>
                                                <option value="0">Không</option>
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label for="inputProductDescription" class="form-label">Ảnh thương hiệu</label>
                                            <input id="img" type="file" name="image" class="form-control hidden"
                                                onchange="changeImg(this)">
                                            <img id="avatar" name="" class="thumbnail" width="200px" height="200px"
                                                src="../images/category/import-img.png">
                                        </div>
                                        {!! show_errors_request($errors, 'image') !!}
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="border border-3 p-4 rounded">
                                        <div class="row g-3">
                                            <div class="col-12">
                                                <div class="d-grid">
                                                    <button type="submit" class="btn btn-light">Lưu bài viết</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@section('style')
<link href="assets/css/example-styles.css" rel="stylesheet">

@endsection
@section('script')
    @parent
    <script>
        function changeImg(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#avatar').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function() {
            $('#avatar').click(function() {
                $('#img').click();
            });
        });
        $(function(){
            $('#people').multiSelect();
        });
    </script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('ckfinder/ckfinder.js') }}"></script>
    <script src="{{asset('backend/assets/js/jquery.multi-select.js') }}"></script>
@stop
@endsection
