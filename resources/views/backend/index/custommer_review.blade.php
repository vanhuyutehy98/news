<div class="row">
    <div class="col-12 col-lg-6 col-xl-8 d-flex">
        <div class="card radius-10 w-100">
            <div class="card-header border-bottom">
                <div class="d-flex align-items-center">
                    <div>
                        <h6 class="mb-0">Đánh giá của khách hàng</h6>
                    </div>
                    <div class="font-22 ms-auto text-white"><i class="bx bx-dots-horizontal-rounded"></i>
                    </div>
                </div>
            </div>
            <ul class="list-group list-group-flush">
                @foreach ($commentNews as $item)
                <li class="list-group-item bg-transparent">
                    <div class="d-flex align-items-center">
                        <img src="" alt="user avatar" class="rounded-circle"
                            width="55" height="55">
                        <div class="ms-3">
                            <h6 class="mb-0">{{$item->new->name}}<small class="ms-4">{{ date('d-m-Y', strtotime($item->created_at)) }}</small></h6>
                            <p class="mb-0 small-font">{{$item->name}} : {{$item->comment}}
                            </p>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
