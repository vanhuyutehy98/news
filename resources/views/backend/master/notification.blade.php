<li>
    <a class="has-arrow" href="javascript:;">
        <div class="parent-icon"><i class="lni lni-alarm"></i>
        </div>
        <div class="menu-title">Thông báo</div>
    </a>
    <ul>
        <li> <a href="{{route('admin.comment.index')}}"><i class="bx bx-right-arrow-alt"></i>Bình luận bài viết</a>
        </li>
        <li> <a href="{{route('admin.comment.success')}}"><i class="bx bx-right-arrow-alt"></i>Bình luận bài viết đã xử lý</a>
        </li>
    </ul>
</li>
