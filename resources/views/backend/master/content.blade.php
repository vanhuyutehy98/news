<li>
    <a class="has-arrow" href="javascript:;">
        <div class="parent-icon"><i class="lni lni-world"></i>
        </div>
        <div class="menu-title">Cấu hình Website</div>
    </a>
    <ul>
        <li> <a href="{{route('admin.setting', ['name' => 'logo'])}}"><i class="bx bx-right-arrow-alt"></i>Logo</a>
        </li>
        <li> <a href="{{route('admin.setting', ['name' => 'facebook'])}}"><i class="bx bx-right-arrow-alt"></i>Facebook link</a>
        </li>
        <li> <a href="{{route('admin.setting', ['name' => 'youtube'])}}"><i class="bx bx-right-arrow-alt"></i>Youtobe link</a>
        </li>
    </ul>
</li>
