<li>
    <a href="javascript:;" class="has-arrow">
        <div class="parent-icon"><i class='bx bx-cart'></i>
        </div>
        <div class="menu-title">Quản lý chung</div>
    </a>
    <ul>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="menu-title">Danh mục</div>
            </a>
            <ul>
                <li> <a href="{{route('admin.category.index')}}"><i class="bx bx-right-arrow-alt"></i>Danh mục</a>
                </li>
                <li> <a href="{{route('admin.category.add')}}"><i class="bx bx-right-arrow-alt"></i>Thêm danh mục</a>
                </li>
            </ul>
        </li>
        
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="menu-title">Bài viết</div>
            </a>
            <ul>
                <li> <a href="{{route('admin.news.index')}}"><i class="bx bx-right-arrow-alt"></i>Bài viết</a>
                </li>
                <li> <a href="{{route('admin.news.add')}}"><i class="bx bx-right-arrow-alt"></i>Thêm bài viết</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="menu-title">Thẻ tags</div>
            </a>
            <ul>
                <li> <a href="{{route('admin.tags.index')}}"><i class="bx bx-right-arrow-alt"></i>Thẻ tags</a>
                </li>
                <li> <a href="{{route('admin.tags.add')}}"><i class="bx bx-right-arrow-alt"></i>Thêm Thẻ tags</a>
                </li>
            </ul>
        </li>
    </ul>
</li>
