@extends('backend.master.master')
@section('title')
    Cấu hình Website
@endsection
@section('main')
    <div class="page-wrapper">
        <div class="page-content">
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Cấu hình Website</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="card">
                <div class="card-body p-4">
                    <div class="form-body mt-4">
                        <form method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-8">
                                    {!! alert_success(session('thong-bao-thanh-cong')) !!}
                                    <div class="border border-3 p-4 rounded">
                                        @if ($name == 'logo')
                                            <div class="mb-3">
                                                <label for="inputProductDescription" class="form-label">Logo</label>
                                                <input id="img" type="file" name="image" class="form-control hidden"
                                                    onchange="changeImg(this)">
                                                <img id="avatar" name="" class="thumbnail" width="200px" height="200px"
                                                    src="../images/setting/{{$setting->logo ? $setting->logo : 'import-img.png'}}">
                                            </div>
                                            {!! show_errors_request($errors, 'image') !!}
                                        @endif
                                        
                                        @if ($name == 'facebook')
                                            <div class="mb-3">
                                                <label for="inputProductTitle" class="form-label">Facebook link</label>
                                                <input type="text" name="facebook" class="form-control" id="inputProductTitle"
                                                    placeholder="Bắt buộc" value="{{old('facebook')}}">
                                            </div>
                                            {!! show_errors_request($errors, 'facebook') !!}
                                        @endif
                                        @if ($name == 'youtube')
                                            <div class="mb-3">
                                                <label for="inputProductTitle" class="form-label">Youtube link</label>
                                                <input type="text" name="youtube" class="form-control" id="inputProductTitle"
                                                    placeholder="Bắt buộc" value="{{old('youtube')}}">
                                            </div>
                                            {!! show_errors_request($errors, 'youtube') !!}
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="border border-3 p-4 rounded">
                                        <div class="row g-3">
                                            <div class="col-12">
                                                <div class="d-grid">
                                                    <button type="submit" class="btn btn-light">Lưu</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

@section('script')
    @parent
    <script>
        function changeImg(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#avatar').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function() {
            $('#avatar').click(function() {
                $('#img').click();
            });
        });
        
    </script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('ckfinder/ckfinder.js') }}"></script>
@stop
@endsection
