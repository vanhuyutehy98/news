@extends('backend.master.master')
@section('title')
    Sửa quản trị Website {{ $user->name }}
@endsection
@section('main')
    <div class="page-wrapper">
        <div class="page-content">
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Admin/Sửa quản trị
                                {{ $user->name }}
                            </li>

                        </ol>
                    </nav>
                </div>
            </div>
            <div class="card">
                <div class="card-body p-4">
                    <div class="form-body mt-4">
                        <div class="row">
                            {!! alert_warning(session('thong-bao-loi')) !!}
                            <form action="{{ route('admin.user_admin.update', ['id' => $user->id]) }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="col-lg-12">
                                    <div class="border border-3 p-4 rounded">
                                        <div class="mb-3">
                                            <label for="inputVendor" class="form-label">Tên</label>
                                            <input type="text" name="name" class="form-control" id="inputProductTitle"
                                                placeholder="Bắt buộc" value="{{ $user->name }}">
                                        </div>
                                        {!! show_errors_request($errors, 'name') !!}
                                        <div class="mb-3">
                                            <label for="inputProductTitle" class="form-label">Email</label>
                                            <input type="text" name="email" class="form-control" id="inputProductTitle"
                                                placeholder="Bắt buộc" value="{{ $user->email }}">
                                        </div>
                                        {!! show_errors_request($errors, 'email') !!}
                                        <div class="mb-3">
                                            <label for="inputProductDescription" class="form-label">Ảnh</label>
                                            <input id="img" type="file" name="image" class="form-control hidden"
                                                onchange="changeImg(this)">
                                            <img id="avatar" name="image" class="thumbnail" width="300px" height="300px"
                                                src="../images/users/{{ $user->image }}">
                                        </div>
                                        {!! show_errors_request($errors, 'image') !!}
                                        <div class="mb-3">
                                            <label for="inputProductDescription" class="form-label">Phân quyền quản trị</label>
                                            <!-- Table  -->
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>Xem</th>
                                                        <th>Thêm</th>
                                                        <th>Sửa</th>
                                                        <th>Xóa</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_new"
                                                                {!!show_ruler($user,'view_new')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xem bài viết</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_new"
                                                                {!!show_ruler($user,'add_new')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Thêm bài viết</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_new"
                                                                {!!show_ruler($user,'edit_new')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Sửa bài viết</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="remove_new"
                                                                {!!show_ruler($user,'remove_new')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xóa bài viết</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_user_admin"
                                                                {!!show_ruler($user,'view_user_admin')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xem người dùng</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_user_admin"
                                                                {!!show_ruler($user,'add_user_admin')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Thêm người dùng</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_user_admin"
                                                                {!!show_ruler($user,'edit_user_admin')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Sửa người dùng</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="remove_user_admin"
                                                                {!!show_ruler($user,'remove_user_admin')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xóa người dùng</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_tag"
                                                                {!!show_ruler($user,'view_tag')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xem tags</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_tag"
                                                                {!!show_ruler($user,'add_tag')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Thêm tags</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_tag"
                                                                {!!show_ruler($user,'edit_tag')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Sửa tags</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="remove_tag"
                                                                {!!show_ruler($user,'remove_tag')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xóa tags</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_category"
                                                                {!!show_ruler($user,'view_category')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xem danh mục</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_category"
                                                                {!!show_ruler($user,'add_category')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Thêm danh mục</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_category"
                                                                {!!show_ruler($user,'edit_category')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Sửa danh mục</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="remove_category"
                                                                {!!show_ruler($user,'remove_category')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xóa danh mục</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_comment"
                                                                {!!show_ruler($user,'view_comment')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xem bình luận</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_comment"
                                                                {!!show_ruler($user,'add_comment')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Thêm bình luận</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_comment"
                                                                {!!show_ruler($user,'edit_comment')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Sửa bình luận</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="remove_comment"
                                                                {!!show_ruler($user,'remove_comment')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xóa bình luận</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            <!-- Table  -->

                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="d-grid">
                                        <button type="submit" class="btn btn-light">Lưu thành viên</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@section('script')
    @parent
    <script>
        function changeImg(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#avatar').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function() {
            $('#avatar').click(function() {
                $('#img').click();
            });
        });

    </script>
@stop
@endsection
