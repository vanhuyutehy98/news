<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9" lang="en-US"> <![endif]-->
<html lang="en-US">
<!-- Mirrored from jellywp.com/html/sprasa-preview/my-account.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 28 May 2022 02:40:50 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->

<head>
    <base href="{{ asset('') }}home/">
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="author" content="" />
    <meta name="description" content="" />
    <meta name="keywords" content="" /><!-- Title-->
    <title>Đăng nhập</title><!-- Favicon-->
    <link rel="icon" href="img/favicon.png" type="image/x-icon" /><!-- Stylesheets-->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" /><!-- end head -->
</head>

<body class="woocommerce-account woocommerce-page woocommerce-js mobile_nav_class jl-has-sidebar">
    <div class="options_layout_wrapper jl_clear_at jl_radius jl_none_box_styles jl_border_radiuss jl_en_day_night">
        <div class="options_layout_container full_layout_enable_front">
            <header
                class="header-wraper jl_header_magazine_style two_header_top_style header_layout_style3_custom jl_cus_top_share">
                <div class="jl_blank_nav"></div>
                <div id="menu_wrapper" class="menu_wrapper jl_menu_sticky jl_stick">
                    <div class="container">
                        <div class="row">
                            <div class="main_menu col-md-12">
                                <div class="logo_small_wrapper_table">
                                    <div class="logo_small_wrapper">
                                        <!-- begin logo --><a class="logo_link" href="{{ route('index') }}"><img
                                                class="jl_logo_n" src="../../images/setting/{{ $setting->logo }}"
                                                alt="sprasa" /><img class="jl_logo_w"
                                                src="../../images/setting/{{ $setting->logo }}" alt="sprasa" /></a>
                                        <!-- end logo -->
                                    </div>
                                </div>
                                <div class="search_header_menu jl_nav_mobile">
                                    <div class="jl_day_night jl_day_en" style="margin-left: 20px;">
                                        @if (!Auth::guard('guest')->check())
                                            <a href="{{ route('index.login') }}">Đăng nhập</a>/
                                            <a href="{{ route('index.dk') }}">Đăng ký</a>
                                        @else
                                            <a href="{{ route('index.logout') }}">Đăng xuất</a>
                                        @endif
                                    </div>
                                    <div class="jl_day_night jl_day_en"> <span class="jl-night-toggle-icon"> <span
                                                class="jl_moon"> <i class="jli-moon"></i> </span><span
                                                class="jl_sun">
                                                <i class="jli-sun"></i> </span></span></div>

                                </div>
                                <div class="menu-primary-container navigation_wrapper jl_cus_share_mnu">
                                    <ul id="mainmenu" class="jl_main_menu">
                                        <li class="menu-item current-menu-item current_page_item"> <a
                                                href="{{ route('index') }}">Trang chủ<span
                                                    class="border-menu"></span></a></li>
                                        <li class="menu-item menu-item-has-children"> <a href="javascript:void(0)">Danh
                                                mục
                                            </a>
                                            <ul class="sub-menu">
                                                @foreach ($categories as $categorie)
                                                    <li class="menu-item"> <a
                                                            href="{{ route('index.category', ['slug' => $categorie->slug]) }}">{{ $categorie->name }}<span
                                                                class="border-menu"></span></a></li>
                                                @endforeach

                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div id="content_nav" class="jl_mobile_nav_wrapper">
                <div id="nav" class="jl_mobile_nav_inner">
                    <div class="menu_mobile_icons mobile_close_icons closed_menu"> <span class="jl_close_wapper"><span
                                class="jl_close_1"></span><span class="jl_close_2"></span></span></div>
                    <ul id="mobile_menu_slide" class="menu_moble_slide">
                        <li class="menu-item current-menu-item current_page_item"> <a href="{{ route('index') }}">Trang
                                chủ<span class="border-menu"></span></a></li>
                        <li class="menu-item menu-item-has-children"> <a href="#">Features<span
                                    class="border-menu"></span></a>
                            <ul class="sub-menu">
                                <li class="menu-item menu-item-has-children"> <a href="post-layout-1.html">Post
                                        Layout<span class="border-menu"></span></a>
                                    <ul class="sub-menu">
                                        <li class="menu-item"> <a href="post-layout-1.html">Post Layout 1<span
                                                    class="border-menu"></span></a></li>
                                        <li class="menu-item"> <a href="post-layout-2.html">Post Layout 2<span
                                                    class="border-menu"></span></a></li>
                                        <li class="menu-item"> <a href="post-layout-3.html">Post Layout 3<span
                                                    class="border-menu"></span></a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-has-children"> <a href="post-format-standard.html">Post
                                        Format<span class="border-menu"></span></a>
                                    <ul class="sub-menu">
                                        <li class="menu-item"> <a href="post-format-standard.html">Post format
                                                standard<span class="border-menu"></span></a></li>
                                        <li class="menu-item"> <a href="post-format-gallery.html">Post format
                                                gallery<span class="border-menu"></span></a></li>
                                        <li class="menu-item"> <a href="post-format-quote.html">Post Format
                                                Quote<span class="border-menu"></span></a></li>
                                        <li class="menu-item"> <a href="post-format-video.html">Post format
                                                video<span class="border-menu"></span></a></li>
                                        <li class="menu-item"> <a href="post-format-audio.html">Post format
                                                audio<span class="border-menu"></span></a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"> <a href="author.html">Author Page<span
                                            class="border-menu"></span></a></li>
                                <li class="menu-item"> <a href="category.html">Category Page<span
                                            class="border-menu"></span></a></li>
                                <li class="menu-item"> <a href="page-404.html">Page 404<span
                                            class="border-menu"></span></a></li>
                            </ul>
                        </li>
                        <li class="menu-item"> <a href="business.html">Business<span
                                    class="border-menu"></span></a>
                        </li>
                        <li class="menu-item menu-item-has-children"> <a href="shop.html">Shop<span
                                    class="border-menu"></span></a>
                            <ul class="sub-menu">
                                <li class="menu-item"> <a href="cart.html">Cart<span
                                            class="border-menu"></span></a>
                                </li>
                                <li class="menu-item"> <a href="my-account.html">My Account<span
                                            class="border-menu"></span></a></li>
                            </ul>
                        </li>
                    </ul>
                    <div id="sprasa_recent_post_text_widget-11" class="widget post_list_widget">
                        <div class="widget_jl_wrapper">
                            <div class="ettitle">
                                <div class="widget-title">
                                    <h2 class="jl_title_c">Recent Posts</h2>
                                </div>
                            </div>
                            <div class="bt_post_widget">
                                <div class="jl_m_right jl_sm_list jl_ml jl_clear_at">
                                    <div class="jl_m_right_w">
                                        <div class="jl_m_right_img jl_radus_e"><a href="#"><img width="120" height="120"
                                                    src="img/pexels-ichad-windhiagiri-3993407-scaled-120x120.jpg"
                                                    class="attachment-sprasa_small_feature size-sprasa_small_feature wp-post-image"
                                                    alt="" loading="lazy" /></a></div>
                                        <div class="jl_m_right_content">
                                            <h2 class="entry-title"><a href="#" tabindex="-1">Best inspire dark photo
                                                    in
                                                    the winter season</a></h2><span class="jl_post_meta"> <span
                                                    class="jl_author_img_w"><i class="jli-user"></i><a href="#"
                                                        title="Posts by Spraya" rel="author">Spraya</a></span><span
                                                    class="post-date"><i class="jli-pen"></i>July 23,
                                                    2016</span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="jl_m_right jl_sm_list jl_ml jl_clear_at">
                                    <div class="jl_m_right_w">
                                        <div class="jl_m_right_img jl_radus_e"><a href="#"><img width="120" height="120"
                                                    src="img/ben-o-sullivan-GNp7ng0lR-8-unsplash-scaled-120x120.jpg"
                                                    class="attachment-sprasa_small_feature size-sprasa_small_feature wp-post-image"
                                                    alt="" loading="lazy" /></a></div>
                                        <div class="jl_m_right_content">
                                            <h2 class="entry-title"><a href="#" tabindex="-1">Your job will be
                                                    perfect
                                                    if you concentrate</a></h2><span class="jl_post_meta"> <span
                                                    class="jl_author_img_w"><i class="jli-user"></i><a href="#"
                                                        title="Posts by Spraya" rel="author">Spraya</a></span><span
                                                    class="post-date"><i class="jli-pen"></i>July 23,
                                                    2016</span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="jl_m_right jl_sm_list jl_ml jl_clear_at">
                                    <div class="jl_m_right_w">
                                        <div class="jl_m_right_img jl_radus_e"><a href="#"><img width="120"
                                                    height="120" src="img/pexels-unviajesinmaleta-3404200-120x120.jpg"
                                                    class="attachment-sprasa_small_feature size-sprasa_small_feature wp-post-image"
                                                    alt="" loading="lazy" /></a></div>
                                        <div class="jl_m_right_content">
                                            <h2 class="entry-title"><a href="#" tabindex="-1">Enjoy the best time
                                                    with a
                                                    special person</a></h2><span class="jl_post_meta"> <span
                                                    class="jl_author_img_w"><i class="jli-user"></i><a href="#"
                                                        title="Posts by Spraya" rel="author">Spraya</a></span><span
                                                    class="post-date"><i class="jli-pen"></i>July 23,
                                                    2016</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="sprasa_about_us_widget-3" class="widget jellywp_about_us_widget">
                        <div class="widget_jl_wrapper about_widget_content">
                            <div class="jellywp_about_us_widget_wrapper">
                                <div class="social_icons_widget">
                                    <ul class="social-icons-list-widget icons_about_widget_display">
                                        <li> <a href="#" class="facebook" target="_blank"><i
                                                    class="jli-facebook"></i></a></li>
                                        <li> <a href="#" class="behance" target="_blank"><i
                                                    class="jli-behance"></i></a>
                                        </li>
                                        <li> <a href="#" class="vimeo" target="_blank"><i
                                                    class="jli-vimeo"></i></a>
                                        </li>
                                        <li> <a href="#" class="youtube" target="_blank"><i
                                                    class="jli-youtube"></i></a>
                                        </li>
                                        <li> <a href="#" class="instagram" target="_blank"><i
                                                    class="jli-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="search_form_menu_personal">
                <div class="menu_mobile_large_close"> <span
                        class="jl_close_wapper search_form_menu_personal_click"><span
                            class="jl_close_1"></span><span class="jl_close_2"></span></span></div>
                <form method="get" class="searchform_theme" action="#"><input type="text" placeholder="Search..."
                        value="" name="s" class="search_btn" /><button type="submit" class="button"><i
                            class="jli-search"></i></button></form>
            </div>
            <div class="mobile_menu_overlay"></div>
            <section id="content_main" class="clearfix">
                <div class="container">
                    <div class="row main_content">
                        <!-- begin content -->
                        <div class="page-full col-md-12 post-5076 page type-page status-publish hentry" id="content">
                            <div class="jl_cat_mid_title">
                                <h3 class="categories-title title">Tài khoản của tôi</h3>
                            </div>
                            <div class="content_single_page post-5076 page type-page status-publish hentry">
                                <div class="content_page_padding">
                                    <div class="woocommerce">
                                        <div class="woocommerce-notices-wrapper"></div>
                                        <h2>Đăng ký</h2>
                                        {{ alert_warning(session('thongbao')) }}
                                        {{ alert_success(session('thong-bao-thanh-cong')) }}
                                        <form class="woocommerce-form woocommerce-form-login login" method="post">
                                            @csrf
                                            <p
                                                class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                <label for="username">Tên người dùng&nbsp;<span
                                                        class="required">*</span></label><input type="text"
                                                    class="woocommerce-Input woocommerce-Input--text input-text"
                                                    name="name" id="name" autocomplete="username" value="">
                                            </p>
                                            <p
                                                class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                <label for="username">Địa chỉ email&nbsp;<span
                                                        class="required">*</span></label><input type="text"
                                                    class="woocommerce-Input woocommerce-Input--text input-text"
                                                    name="email" id="email" autocomplete="username" value="">
                                            </p>
                                            <p
                                                class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                <label for="password">Mật khẩu&nbsp;<span
                                                        class="required">*</span></label><span
                                                    class="password-input"><input
                                                        class="woocommerce-Input woocommerce-Input--text input-text"
                                                        type="password" name="password" id="password"
                                                        autocomplete="current-password"><span
                                                        class="show-password-input"></span></span>
                                            </p>
                                            <p class="form-row">
                                                <button type="submit"
                                                    class="woocommerce-button button woocommerce-form-login__submit"
                                                    name="login" value="Log in">Đăng ký</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- end content -->
            <!-- Start footer -->
            <footer id="footer-container" class="jl_footer_act enable_footer_columns_dark">
                <div class="footer-columns">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div id="sprasa_about_us_widget-2" class="widget jellywp_about_us_widget">
                                    <div class="widget_jl_wrapper about_widget_content">
                                        <div class="jellywp_about_us_widget_wrapper"><img class="footer_logo_about"
                                                src="../../images/setting/{{ $setting->logo }}" alt="" />
                                            <p>{{ $setting->info }}</p>
                                            <div class="social_icons_widget">
                                                <ul class="social-icons-list-widget icons_about_widget_display"></ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="sprasa_about_us_widget-4" class="widget jellywp_about_us_widget">
                                    <div class="widget_jl_wrapper about_widget_content">
                                        <div class="jellywp_about_us_widget_wrapper">
                                            <div class="social_icons_widget">
                                                <ul class="social-icons-list-widget icons_about_widget_display">
                                                    <li> <a href="{{ $setting->facebookLink }}" class="facebook"
                                                            target="_blank"><i class="jli-facebook"></i></a></li>
                                                    <li> <a href="{{ $setting->youtobeLink }}" class="youtube"
                                                            target="_blank"><i class="jli-youtube"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="sprasa_recent_post_text_widget-8" class="widget post_list_widget">
                                    <div class="widget_jl_wrapper">
                                        <div class="ettitle">
                                            <div class="widget-title">
                                                <h2 class="jl_title_c">Thêm từ chúng tôi</h2>
                                            </div>
                                        </div>
                                        <div class="bt_post_widget">
                                            @foreach ($newViewsFooter as $new)
                                                <div class="jl_m_right jl_sm_list jl_ml jl_clear_at">
                                                    <div class="jl_m_right_w">
                                                        <div class="jl_m_right_img jl_radus_e"><a
                                                                href="{{ route('index.detail', ['slug' => $new->slug]) }}"><img
                                                                    width="120" height="120"
                                                                    src="../../images/news/{{ $new->image }}"
                                                                    class="attachment-sprasa_small_feature size-sprasa_small_feature wp-post-image"
                                                                    alt="" loading="lazy" /></a></div>
                                                        <div class="jl_m_right_content">
                                                            <h2 class="entry-title"><a
                                                                    href="{{ route('index.detail', ['slug' => $new->slug]) }}"
                                                                    tabindex="-1">{{ $new->name }}</a></h2><span
                                                                class="jl_post_meta"> <span
                                                                    class="jl_author_img_w"><i
                                                                        class="jli-user"></i><a href="#"
                                                                        title="Posts by Spraya"
                                                                        rel="author">{{ $new->users->name }}</a></span><span
                                                                    class="post-date"><i
                                                                        class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="widget_jl_wrapper jl_clear_at">
                                    <div id="sprasa_category_image_widget_register-2" class="widget jellywp_cat_image">
                                        <div class="widget-title">
                                            <h2 class="jl_title_c">Danh mục</h2>
                                        </div>
                                        <div class="wrapper_category_image">
                                            <div class="category_image_wrapper_main">
                                                @foreach ($categoryFooter as $category)
                                                    <div class="jl_cat_img_w">
                                                        <div class="jl_cat_img_c"><a class="category_image_link"
                                                                id="category_color_2"
                                                                href="{{ route('index.category', ['slug' => $category->slug]) }}"></a>
                                                            <div class="category_image_bg_image jl_f_img_bg"
                                                                style="background-color: #4dcf8f; background-image: url('../../images/category/{{ $category->image }}');">
                                                            </div> <span class="jl_cm_overlay"><span
                                                                    class="jl_cm_name">{{ $category->name }}</span><span
                                                                    class="jl_cm_count">{{ $category->news()->count() }}
                                                                    bài viết</span></span>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer><!-- End footer -->
            <div id="go-top"> <a href="#go-top"><i class="jli-up-chevron"></i></a></div>
        </div>
    </div>
    <script src="js/jquery.js"></script>
</body>
<!-- Mirrored from jellywp.com/html/sprasa-preview/my-account.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 28 May 2022 02:40:50 GMT -->

</html>
