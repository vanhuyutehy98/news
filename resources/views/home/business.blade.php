@extends('home.layout.index')
@section('title')
    Danh muc
@endsection
@section('main')
    <div class="jl_post_loop_wrapper" id="wrapper_masonry">
        <div class="category_header_post_2col_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="jl_cat_head jl_clear_at">
                            <div class="jl_m_below_w jl_clear_at">
                                <div class="jl_m_below">
                                    <div class="jl_m_below_c">
                                        <div class="jl_m_below_img">
                                            <div class="jl_f_img_bg jl_radus_e"
                                                style="background-image: url('../../images/news/{{$oneNew->image}}')">
                                            </div> <a href="#" class="jl_f_img_link"></a> <span
                                                class="jl_post_type_icon"><i
                                                    class="jli-gallery"></i></span>
                                        </div>
                                        <div class="text-box"> <span class="jl_f_cat"><a
                                                    class="post-category-color-text" style="background:#eba845"
                                                    href="{{route('index.category', ['slug' => $oneNew->slug])}}">{{$oneNew->category->name}}</a></span>
                                            <h3 class="entry-title"><a href="{{route('index.detail', ['slug' => $oneNew->slug])}}" tabindex="-1">{{$oneNew->name}}</a></h3> <span
                                                class="jl_post_meta"><span class="jl_author_img_w"><i
                                                        class="jli-user"></i><a href="{{route('index.detail', ['slug' => $oneNew->slug])}}"
                                                        title="{{$oneNew->users->name}}"
                                                        rel="author">{{$oneNew->users->name}}</a></span><span
                                                    class="post-date"><i class="jli-pen"></i>{{ date('d-m-Y', strtotime($oneNew->created_at)) }}</span><span class="post-read-time"><i
                                                        class="jli-watch-2"></i>{{$oneNew->created_at->diffForHumans()}}</span></span>
                                            <p>{!!$oneNew->info!!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 grid-sidebar" id="content">
                    <div class="jl_cat_mid_title">
                        <h3 class="categories-title title">{{$categorie->name}}</h3>
                        <p>{!!$categorie->info!!}</p>
                    </div>
                    <div class="jl_wrapper_cat">
                        <div id="content_masonry" class="jl_cgrid pagination_infinite_style_cat ">
                            @foreach ($news as $new)
                            <div
                                class="box jl_grid_layout1 blog_grid_post_style post-2953 post type-post status-publish format-standard has-post-thumbnail hentry category-business tag-relaxing">
                                <div class="jl_grid_w">
                                    <div class="jl_img_box jl_radus_e"> <a href="{{route('index.detail', ['slug' => $new->slug])}}"> <img width="500"
                                                height="250px"
                                                style="height: 210px; width: 100%;"
                                                src="../../images/news/{{$new->image}}"
                                                class="attachment-sprasa_slider_grid_small size-sprasa_slider_grid_small wp-post-image"
                                                alt="" loading="lazy"> </a> <span class="jl_f_cat"><a
                                                class="post-category-color-text" style="background:#eba845"
                                                href="{{route('index.category', ['slug' => $new->slug])}}">{{$new->category->name}}</a></span> </div>
                                    <div class="text-box">
                                        <h3><a href="{{route('index.detail', ['slug' => $new->slug])}}" tabindex="-1">{{$new->name}}</a></h3> <span class="jl_post_meta"><span
                                                class="jl_author_img_w"><i class="jli-user"></i><a
                                                    href="#" title="Posts by Spraya"
                                                    rel="author">{{$new->users->name}}</a></span><span
                                                class="post-date"><i class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span><span class="post-read-time"><i
                                                    class="jli-watch-2"></i>{{$new->created_at->diffForHumans()}}</span></span>
                                        {!!$new->info!!}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <nav class="jellywp_pagination">
                            <ul class="page-numbers">
                                {{ $news->links() }}
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-md-4" id="sidebar">
                    <div class="jl_sidebar_w">
                        <div id="sprasa_widget_social_counter_c-2" class="widget jl-widget-social-counter">
                            <div class="widget-title">
                                <h2 class="jl_title_c">Chúng tôi ở đây</h2>
                            </div>
                            <div
                                class="jl_social_counter social_counter_wraper jl_count_style_2 social_counter_item>">
                                <div class="jlsocial-element jl-facebook jl_radus_e"><a target="_blank"
                                        href="{{$setting->facebookLink}}" class="facebook"
                                        title="facebook"></a> <span class="jlsocial-element-left"> <i
                                            class="jli-facebook" aria-hidden="true"></i> <span
                                            class="num-count">228.8k</span></span> <span
                                        class="jlsocial-element-right">Theo dõi</span></div>
                                <div class="jlsocial-element jl-youtube jl_radus_e"><a target="_blank"
                                        href="{{$setting->youtobeLink}}" title="Youtube"></a> <span
                                        class="jlsocial-element-left"> <i class="jli-youtube"
                                            aria-hidden="true"></i> <span
                                            class="num-count">65.5k</span></span> <span
                                        class="jlsocial-element-right">Đăng kí kênh</span></div>
                            </div>
                        </div>
                        <div id="sprasa_recent_post_text_widget-9" class="widget post_list_widget">
                            <div class="widget_jl_wrapper">
                                <div class="ettitle">
                                    <div class="widget-title">
                                        <h2 class="jl_title_c">Xem nhiều</h2>
                                    </div>
                                </div>
                                <div class="bt_post_widget">
                                    @foreach ($newViews as $new)
                                    <div class="jl_m_right jl_sm_list jl_ml jl_clear_at">
                                        <div class="jl_m_right_w">
                                            <div class="jl_m_right_img jl_radus_e"><a href="{{route('index.detail', ['slug' => $new->slug])}}"><img
                                                        width="120" height="120"
                                                        src="../../images/news/{{$new->image}}"
                                                        class="attachment-sprasa_small_feature size-sprasa_small_feature wp-post-image"
                                                        alt="" loading="lazy"></a></div>
                                            <div class="jl_m_right_content">
                                                <h2 class="entry-title"> <a href="{{route('index.detail', ['slug' => $new->slug])}}" tabindex="-1">{{$new->name}}</a></h2>
                                                <span class="jl_post_meta">Lượt xem: {{$new->view}}</span>
                                                <span
                                                    class="jl_post_meta"><span class="jl_author_img_w"><i
                                                            class="jli-user"></i><a href="{{route('index.detail', ['slug' => $new->slug])}}"
                                                            title="Posts by Spraya"
                                                            rel="author">{{$new->users->name}}</a></span><span
                                                        class="post-date"><i class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end content -->
@endsection
