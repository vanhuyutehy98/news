<header class="header-wraper jl_header_magazine_style two_header_top_style header_layout_style3_custom jl_cus_top_share">

    <div class="jl_blank_nav"></div>
    <div id="menu_wrapper" class="menu_wrapper jl_menu_sticky jl_stick">
        <div class="container">
            <div class="row">
                <div class="main_menu col-md-12">
                    <div class="logo_small_wrapper_table">
                        <div class="logo_small_wrapper">
                            <!-- begin logo --><a class="logo_link" href="{{route('index')}}"><img class="jl_logo_n"
                                    src="../../images/setting/{{$setting->logo}}" alt="sprasa" /><img class="jl_logo_w" src="../../images/setting/{{$setting->logo}}"
                                    alt="sprasa" /></a>
                            <!-- end logo -->
                        </div>
                    </div>
                    <div class="search_header_menu jl_nav_mobile">
                        <div class="jl_day_night jl_day_en" style="margin-left: 20px;">
                            @if (!Auth::guard('guest')->check())
                            <a href="{{route('index.login')}}">Đăng nhập</a>
                            @else
                            <a href="{{route('index.logout')}}">Đăng xuất</a>
                            @endif
                        </div>
                        <div class="search_header_wrapper search_form_menu_personal_click"><i class="jli-search"></i></div>
                        <div class="jl_day_night jl_day_en"> <span class="jl-night-toggle-icon"> <span
                                    class="jl_moon"> <i class="jli-moon"></i> </span><span
                                    class="jl_sun">
                                    <i class="jli-sun"></i> </span></span></div>
                        
                    </div>
                    <div class="menu-primary-container navigation_wrapper jl_cus_share_mnu">
                        <ul id="mainmenu" class="jl_main_menu">
                            <li class="menu-item current-menu-item current_page_item"> <a href="{{route('index')}}">Trang chủ<span
                                        class="border-menu"></span></a></li>
                            <li class="menu-item menu-item-has-children"> <a href="javascript:void(0)">Danh mục
                                </a>
                                <ul class="sub-menu">
                                    @foreach ($categories as $categorie)
                                    <li class="menu-item"> <a href="{{route('index.category', ['slug' => $categorie->slug])}}">{{$categorie->name}}<span
                                        class="border-menu"></span></a></li>
                                    @endforeach
                                   
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="search_form_menu_personal">
    <div class="menu_mobile_large_close"> <span
            class="jl_close_wapper search_form_menu_personal_click"><span class="jl_close_1"></span><span
                class="jl_close_2"></span></span></div>
    <form method="get" class="searchform_theme" action="{{route('index.search')}}"><input type="text" placeholder="Search..."
            value="" name="search" class="search_btn" /><button type="submit" class="button"><i
                class="jli-search"></i></button></form>
</div>
<div class="mobile_menu_overlay"></div>
