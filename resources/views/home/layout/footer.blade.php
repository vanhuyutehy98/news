<footer id="footer-container" class="jl_footer_act enable_footer_columns_dark">
    <div class="footer-columns">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div id="sprasa_about_us_widget-2" class="widget jellywp_about_us_widget">
                        <div class="widget_jl_wrapper about_widget_content">
                            <div class="jellywp_about_us_widget_wrapper"><img class="footer_logo_about"
                                    src="../../images/setting/{{$setting->logo}}" alt="" />
                                <p>{{$setting->info}}</p>
                                <div class="social_icons_widget">
                                    <ul class="social-icons-list-widget icons_about_widget_display"></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="sprasa_about_us_widget-4" class="widget jellywp_about_us_widget">
                        <div class="widget_jl_wrapper about_widget_content">
                            <div class="jellywp_about_us_widget_wrapper">
                                <div class="social_icons_widget">
                                    <ul class="social-icons-list-widget icons_about_widget_display">
                                        <li> <a href="{{$setting->facebookLink}}" class="facebook" target="_blank"><i
                                                    class="jli-facebook"></i></a></li>
                                        <li> <a href="{{$setting->youtobeLink}}" class="youtube" target="_blank"><i
                                                    class="jli-youtube"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="sprasa_recent_post_text_widget-8" class="widget post_list_widget">
                        <div class="widget_jl_wrapper">
                            <div class="ettitle">
                                <div class="widget-title">
                                    <h2 class="jl_title_c">Thêm từ chúng tôi</h2>
                                </div>
                            </div>
                            <div class="bt_post_widget">
                                @foreach ($newViewsFooter as $new)
                                <div class="jl_m_right jl_sm_list jl_ml jl_clear_at">
                                    <div class="jl_m_right_w">
                                        <div class="jl_m_right_img jl_radus_e"><a href="{{route('index.detail', ['slug' => $new->slug])}}"><img width="120"
                                                    height="120"
                                                    src="../../images/news/{{$new->image}}"
                                                    class="attachment-sprasa_small_feature size-sprasa_small_feature wp-post-image"
                                                    alt="" loading="lazy" /></a></div>
                                        <div class="jl_m_right_content">
                                            <h2 class="entry-title"><a href="{{route('index.detail', ['slug' => $new->slug])}}" tabindex="-1">{{$new->name}}</a></h2><span
                                                class="jl_post_meta"> <span class="jl_author_img_w"><i
                                                        class="jli-user"></i><a href="#"
                                                        title="Posts by Spraya"
                                                        rel="author">{{$new->users->name}}</a></span><span
                                                    class="post-date"><i class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span></span>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="widget_jl_wrapper jl_clear_at">
                        <div id="sprasa_category_image_widget_register-2" class="widget jellywp_cat_image">
                            <div class="widget-title">
                                <h2 class="jl_title_c">Danh mục</h2>
                            </div>
                            <div class="wrapper_category_image">
                                <div class="category_image_wrapper_main">
                                    @foreach ($categoryFooter as $category)
                                    <div class="jl_cat_img_w">
                                        <div class="jl_cat_img_c"><a class="category_image_link"
                                                id="category_color_2" href="{{route('index.category', ['slug' => $category->slug])}}"></a>
                                            <div class="category_image_bg_image jl_f_img_bg"
                                                style="background-color: #4dcf8f; background-image: url('../../images/category/{{$category->image}}');">
                                            </div> <span class="jl_cm_overlay"><span
                                                    class="jl_cm_name">{{$category->name}}</span><span
                                                    class="jl_cm_count">{{$category->news()->count()}} bài viết</span></span>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- End footer -->