<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9" lang="en-US"> <![endif]-->
<html lang="en-US">
<!-- Mirrored from jellywp.com/html/sprasa-preview/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 28 May 2022 02:40:10 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->

<head>
    <base href="{{ asset('') }}home/">
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="author" content="" />
    <meta name="description" content="" />
    <meta name="keywords" content="" /><!-- Title-->
    <title>@yield('title')</title><!-- Favicon-->
    <link rel="icon" href="img/favicon.png" type="image/x-icon" /><!-- Stylesheets-->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" /><!-- end head -->
    @yield('style')
</head>

<body class="mobile_nav_class jl-has-sidebar">
    <div class="options_layout_wrapper jl_clear_at jl_radius jl_none_box_styles jl_border_radiuss jl_en_day_night">
        <div class="options_layout_container full_layout_enable_front">
            @include('home.layout.header')
            
            @yield('main')
            <!-- Start footer -->
            @include('home.layout.footer')
            <div id="go-top"> <a href="#go-top"><i class="jli-up-chevron"></i></a></div>
        </div>
    </div>
    <script src="js/jquery.js"></script>
</body>
<!-- Mirrored from jellywp.com/html/sprasa-preview/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 28 May 2022 02:40:29 GMT -->

</html>