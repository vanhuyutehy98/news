@extends('home.layout.index')
@section('title')
    Trang chủ
@endsection
@section('style')
    <style>
        .jl_m_right_img .wp-post-image-custom {
            width: 100%;
            height: 120px;
        }
    </style>
@endsection
@section('main')
<div class="jl_home_bw">
    {{-- lay bai cua danh muc, moi danh muc 1 bai. lay du 5 bai --}}
    @if (!empty($newActives))
    <section class="home_section1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="jl_mright_wrapper jl_clear_at">
                        <div class="jl_mix_post">
                            <div class="jl_m_center blog-style-one blog-small-grid">
                                <div class="jl_m_center_w jl_radus_e">
                                    <div class="jl_f_img_bg"
                                        style="background-image: url('../../images/news/{{$newActives[0]->image}}');">
                                    </div><a href="{{route('index.detail', ['slug' => $newActives[0]->slug])}}" class="jl_f_img_link"></a> <span
                                        class="jl_post_type_icon"><i class="jli-gallery"></i></span>
                                    <div class="text-box"> <span class="jl_f_cat"><a
                                                class="post-category-color-text"
                                                style="background: #eba845;" href="{{route('index.category', ['slug' => $newActives[0]->slug])}}">{{$newActives[0]->category->name}}</a></span>
                                        <h3> <a href="{{route('index.detail', ['slug' => $newActives[0]->slug])}}">{{$newActives[0]->name}}</a> </h3><span class="jl_post_meta"> <span
                                                class="jl_author_img_w"><i class="jli-user"></i><a href="{{route('index.detail', ['slug' => $newActives[0]->slug])}}"
                                                    title="Posts by Spraya"
                                                    rel="author">{{$newActives[0]->users->name}}</a></span><span class="post-date"><i
                                                    class="jli-pen"></i>{{ date('d-m-Y', strtotime($newActives[0]->created_at)) }}</span><span
                                                class="post-read-time"><i class="jli-watch-2"></i>{{$newActives[0]->created_at->diffForHumans()}}</span></span>
                                    </div>
                                </div>
                            </div>
                            @foreach ($newActives as $key => $newActive)
                                @if ($key != 1)
                                <div class="jl_m_right">
                                    <div class="jl_m_right_w">
                                        <div class="jl_m_right_img jl_radus_e"><a href="{{route('index.detail', ['slug' => $newActive->slug])}}"><img 
                                                    src="../../images/news/{{$newActive->image}}"
                                                    class="attachment-sprasa_small_feature size-sprasa_small_feature wp-post-image wp-post-image-custom"
                                                    alt="" loading="lazy"></a></div>
                                        <div class="jl_m_right_content"> <span class="jl_f_cat"><a
                                                    class="post-category-color-text"
                                                    style="background: #91bd3a;" href="{{route('index.category', ['slug' => $newActive->slug])}}">{{$newActive->category->name}}</a></span>
                                            <h3 class="entry-title"><a href="{{route('index.detail', ['slug' => $newActive->slug])}}">{{$newActive->name}}</a></h3><span
                                                class="jl_post_meta"> <span class="jl_author_img_w"><i
                                                        class="jli-user"></i><a href="{{route('index.detail', ['slug' => $newActive->slug])}}" title="Posts by Spraya"
                                                        rel="author">{{$newActive->users->name}}</a></span><span class="post-date"><i
                                                        class="jli-pen"></i>{{ date('d-m-Y', strtotime($newActive->created_at)) }}</span></span>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                
                            @endforeach
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    @if (!empty($categoryActives))
    <section class="home_section2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="blockid_72be465" class="block-section jl-main-block"
                        data-blockid="blockid_72be465" data-name="jl_mgrid" data-page_max="11"
                        data-page_current="1" data-author="none" data-order="date_post"
                        data-posts_per_page="6" data-offset="5">
                        <div class="jl_grid_wrap_f jl_clear_at g_3col">
                            <div class="jl-roww content-inner jl-col3 jl-col-row">
                                <div class="jl_sec_title">
                                    <h3 class="jl_title_c"><span>{{$categoryActives[0]->name}}</span></h3>
                                    {!!$categoryActives[0]->info!!}
                                </div>
                                @foreach ($categoryActives[0]->news()->take(6)->get() as $new)
                                    <div class="jl-grid-cols">
                                        <div class="p-wraper post-2959">
                                            <div class="jl_grid_w">
                                                <div class="jl_img_box jl_radus_e"><a href="{{route('index.detail', ['slug' => $new->slug])}}"> <span
                                                            class="jl_post_type_icon"><i
                                                                class="jli-gallery"></i></span>
                                                        <img width="500"
                                                            height="350"
                                                            src="../../images/news/{{$new->image}}"
                                                            class="attachment-sprasa_slider_grid_small size-sprasa_slider_grid_small wp-post-image"
                                                            alt="" loading="lazy"></a> <span class="jl_f_cat"><a
                                                            class="post-category-color-text"
                                                            style="background: #62ce5c;"
                                                            href="{{route('index.category', ['slug' => $new->slug])}}">{{$new->category->name}}</a></span></div>
                                                <div class="text-box">
                                                    <h3><a href="{{route('index.detail', ['slug' => $new->slug])}}">{{$new->name}}</a></h3><span class="jl_post_meta"> <span
                                                            class="jl_author_img_w"><i class="jli-user"></i><a
                                                                href="{{route('index.detail', ['slug' => $new->slug])}}" title="Posts by Spraya"
                                                                rel="author">{{$new->users->name}}</a></span><span
                                                            class="post-date"><i class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span><span class="post-read-time"><i
                                                                class="jli-watch-2"></i>{{$new->created_at->diffForHumans()}}</span></span>
                                                    {!!$new->info!!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                
                            </div>
                            <div class="jl_lmore_wrap">
                                <div class="jl_lmore_c"> <a href="{{route('index.category', ['slug' => $categoryActives[0]->slug])}}" class="jl-load-link"><span>Xem Thêm</span></a><span
                                        class="jl-load-animation"><span></span></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="home_section3">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="jl_sec_title">
                        <h3 class="jl_title_c">{{$categoryActives[1]->name}}</h3>
                        {!!$categoryActives[1]->info!!}
                    </div>
                    <div class="jl_mg_wrapper jl_clear_at">
                        <div class="jl_mg_post jl_clear_at">
                            @foreach ($categoryActives[1]->news()->take(5)->get() as $key => $new)
                            @if ($key == 0)
                            <div class="jl_mg_main">
                                <div class="jl_mg_main_w">
                                    <div class="jl_img_box jl_radus_e"><a href="{{route('index.detail', ['slug' => $new->slug])}}">
                                        <img width="1000"
                                                height="650"
                                                src="../../images/news/{{$new->image}}"
                                                class="attachment-sprasa_feature_large size-sprasa_feature_large wp-post-image"
                                                alt="" loading="lazy"></a> <span class="jl_f_cat"><a
                                                class="post-category-color-text"
                                                style="background: #4dcf8f;" href="{{route('index.category', ['slug' => $new->slug])}}">{{$new->category->name}}</a></span>
                                    </div>
                                    <div class="text-box">
                                        <h3 class="entry-title"> <a href="{{route('index.detail', ['slug' => $new->slug])}}" tabindex="-1">{{$new->name}}</a> </h3><span
                                            class="jl_post_meta"> <span class="jl_author_img_w"><i
                                                    class="jli-user"></i><a href="{{route('index.detail', ['slug' => $new->slug])}}" title="Posts by Spraya"
                                                    rel="author">{{$new->users->name}}</a></span><span class="post-date"><i
                                                    class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span><span
                                                class="post-read-time"><i class="jli-watch-2"></i>{{$new->created_at->diffForHumans()}}</span></span>
                                        {!!$new->info!!}
                                    </div>
                                </div>
                            </div>
                            @endif
                            
                            <div class="jl_mg_sm">
                                <div class="jl_mg_sm_w">
                                    <div class="jl_f_img jl_radus_e"><a href="{{route('index.detail', ['slug' => $new->slug])}}">
                                        <img
                                            style="height: 210px; width: 255px;"
                                            src="../../images/news/{{$new->image}}"
                                            class="attachment-sprasa_feature_large size-sprasa_feature_large wp-post-image"
                                            alt="" loading="lazy">
                                            </a> <span class="jl_f_cat"><a
                                                class="post-category-color-text"
                                                style="background: #91bd3a;" href="{{route('index.category', ['slug' => $new->slug])}}">{{$new->category->name}}</a></span>
                                    </div>
                                    <div class="jl_mg_content">
                                        <h3 class="entry-title"><a href="{{route('index.detail', ['slug' => $new->slug])}}">{{$new->name}}</a></h3><span class="jl_post_meta"> <span
                                                class="jl_author_img_w"><i class="jli-user"></i><a href="{{route('index.detail', ['slug' => $new->slug])}}"
                                                    title="Posts by Spraya"
                                                    rel="author">{{$new->users->name}}</a></span><span class="post-date"><i
                                                    class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span></span>
                                                    {!!$new->info!!}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="home_section4">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="blockid_3f9d058" class="block-section jl-main-block">
                        <div class="jl_grid_wrap_f jl_clear_at g_4col">
                            <div class="jl-roww content-inner jl-col3 jl-col-row">
                                <div class="jl_sec_title">
                                    <h3 class="jl_title_c"><span>{{$categoryActives[2]->name}}</span></h3>
                                    <p>{!!$categoryActives[2]->info!!}</p>
                                </div>
                                @foreach ($categoryActives[2]->news()->take(4)->get() as $key => $new)
                                <div class="jl-grid-cols">
                                    <div class="p-wraper post-2691">
                                        <div class="jl_grid_w">
                                            <div class="jl_img_box jl_radus_e"><a href="{{route('index.detail', ['slug' => $new->slug])}}">
                                                    <img 
                                                    style="height: 150px; width: 255px;"
                                                        src="../../images/news/{{$new->image}}"
                                                        class="attachment-sprasa_slider_grid_small size-sprasa_slider_grid_small wp-post-image"
                                                        alt="" loading="lazy"></a> <span class="jl_f_cat"><a
                                                        class="post-category-color-text"
                                                        style="background: #379e63;"
                                                        href="{{route('index.category', ['slug' => $new->slug])}}">{{$new->category->name}}</a></span></div>
                                            <div class="text-box">
                                                <h3><a href="{{route('index.detail', ['slug' => $new->slug])}}">{{$new->name}}</a></h3><span class="jl_post_meta"> <span
                                                        class="jl_author_img_w"><i class="jli-user"></i><a
                                                            href="{{route('index.detail', ['slug' => $new->slug])}}" title="Posts by Spraya"
                                                            rel="author">{{$new->users->name}}</a></span><span
                                                        class="post-date"><i class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span><span class="post-read-time"><i
                                                            class="jli-watch-2"></i>{{$new->created_at->diffForHumans()}}</span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- lay bai viet moi nhat tuan nay --}}
    <section class="home_section5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="blockid_5ee403b" class="block-section jl-main-block">
                        <div class="jl_slide_wrap_f jl_clear_at">
                            <div class="jl-roww content-inner jl-col-none jl-col-row">
                                <div class="jl_sec_title">
                                    <h3 class="jl_title_c"><span>{{$categoryActives[2]->name}}</span></h3>
                                    <p>{!!$categoryActives[2]->info!!}</p>
                                </div>
                                <div class="jl_ar_top">
                                    <div class="jl-w-slider jl_full_feature_w">
                                        <div class="jl-eb-slider jelly_loading_pro" data-arrows="true"
                                            data-play="true" data-effect="false" data-speed="500"
                                            data-autospeed="7000" data-loop="true" data-dots="true"
                                            data-swipe="true" data-items="1" data-xs-items="1"
                                            data-sm-items="1" data-md-items="1" data-lg-items="1"
                                            data-xl-items="1">
                                            @foreach ($categoryActives[2]->news()->take(4)->get() as $key => $new)
                                            <div class="item-slide jl_radus_e">
                                                <div class="slide-inner">
                                                    <div class="jl_full_feature">
                                                        <div class="jl_f_img_bg"
                                                            style="background-image: url('../../images/news/{{$new->image}}')">
                                                        </div><a href="{{route('index.detail', ['slug' => $new->slug])}}" class="jl_f_img_link"></a>
                                                        <div class="jl_f_postbox"> <span class="jl_f_cat"><a
                                                                    class="post-category-color-text"
                                                                    style="background:#4dcf8f"
                                                                    href="{{route('index.category', ['slug' => $new->slug])}}">{{$new->category->name}}</a></span>
                                                            <h3 class="jl_f_title"> <a href="{{route('index.detail', ['slug' => $new->slug])}}"
                                                                    tabindex="-1">{{$new->name}}</a> </h3> <span
                                                                class="jl_post_meta"><span
                                                                    class="jl_author_img_w"><i
                                                                        class="jli-user"></i><a href="{{route('index.detail', ['slug' => $new->slug])}}"
                                                                        title="Posts by Spraya"
                                                                        rel="author">{{$new->users->name}}</a></span><span
                                                                    class="post-date"><i
                                                                        class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span><span
                                                                    class="post-read-time"><i
                                                                        class="jli-watch-2"></i>{{$new->created_at->diffForHumans()}}</span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- Bài đăng gần đây 8 bai --}}
    <section class="home_section6">
        <div class="container">
            <div class="row">
                <div class="col-md-8 grid-sidebar" id="content">
                    <div id="blockid_e3cb9ed" class="block-section jl-main-block"
                        data-blockid="blockid_e3cb9ed" data-name="jl_mgrid" data-page_max="3"
                        data-page_current="1" data-categories="4,6,9" data-author="none"
                        data-order="date_post" data-posts_per_page="8">
                        <div class="jl_grid_wrap_f jl_clear_at g_2col">
                            <div class="jl-roww content-inner jl-col3 jl-col-row">
                                <div class="jl_sec_title">
                                    <h3 class="jl_title_c"><span>Bài đăng gần đây</span></h3>
                                </div>
                                @foreach ($news as $new)
                                <div class="jl-grid-cols">
                                    <div class="p-wraper post-2949">
                                        <div class="jl_grid_w">
                                            <div class="jl_img_box jl_radus_e"><a href="{{route('index.detail', ['slug' => $new->slug])}}"> <span
                                                        class="jl_post_type_icon"><i
                                                            class="jli-gallery"></i></span><img width="500"
                                                        height="350"
                                                        style="height: 199px; width: 335px;"
                                                        src="../../images/news/{{$new->image}}"
                                                        class="attachment-sprasa_slider_grid_small size-sprasa_slider_grid_small wp-post-image"
                                                        alt="" loading="lazy"></a> <span class="jl_f_cat"><a
                                                        class="post-category-color-text"
                                                        style="background: #7ebdb4;"
                                                        href="{{route('index.category', ['slug' => $new->slug])}}">{{$new->category->name}}</a></span></div>
                                            <div class="text-box">
                                                <h3><a href="{{route('index.detail', ['slug' => $new->slug])}}">{{$new->name}}</a></h3><span class="jl_post_meta">
                                                    <span class="jl_author_img_w"><i class="jli-user"></i><a
                                                            href="{{route('index.detail', ['slug' => $new->slug])}}" title="Posts by Spraya"
                                                            rel="author">{{$new->users->name}}</a></span><span
                                                        class="post-date"><i class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span><span class="post-read-time"><i
                                                            class="jli-watch-2"></i>{{$new->created_at->diffForHumans()}}</span></span>
                                                <p>{!!$new->info!!}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="jl_lmore_wrap">
                                <div class="jl_lmore_c"> <a href="{{route('index.category', ['slug' => $categoryActives[0]->slug])}}" class="jl-load-link"><span>Xem thêm</span></a><span
                                        class="jl-load-animation"><span></span></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" id="sidebar">
                    <div class="jl_sidebar_w">
                        <div id="sprasa_widget_social_counter_c-2" class="widget jl-widget-social-counter">
                            <div class="widget-title">
                                <h2 class="jl_title_c">Chúng tôi ở đây</h2>
                            </div>
                            <div
                                class="jl_social_counter social_counter_wraper jl_count_style_2 social_counter_item>">
                                <div class="jlsocial-element jl-facebook jl_radus_e"><a target="_blank"
                                        href="{{$setting->facebookLink}}" class="facebook"
                                        title="facebook"></a> <span class="jlsocial-element-left"> <i
                                            class="jli-facebook" aria-hidden="true"></i> <span
                                            class="num-count">228.8k</span></span> <span
                                        class="jlsocial-element-right">Theo dõi</span></div>
                                <div class="jlsocial-element jl-youtube jl_radus_e"><a target="_blank"
                                        href="{{$setting->youtobeLink}}" title="Youtube"></a>
                                    <span class="jlsocial-element-left"> <i class="jli-youtube"
                                            aria-hidden="true"></i> <span
                                            class="num-count">65.5k</span></span> <span
                                        class="jlsocial-element-right">Đăng kí kênh</span></div>
                            </div>
                        </div>
                        <div id="sprasa_recent_post_text_widget-9" class="widget post_list_widget">
                            <div class="widget_jl_wrapper">
                                <div class="ettitle">
                                    <div class="widget-title">
                                        <h2 class="jl_title_c">Xem nhiều</h2>
                                    </div>
                                </div>
                                <div class="bt_post_widget">
                                    @foreach ($newViews as $new)
                                    <div class="jl_m_right jl_sm_list jl_ml jl_clear_at">
                                        <div class="jl_m_right_w">
                                            <div class="jl_m_right_img jl_radus_e"><a href="{{route('index.detail', ['slug' => $new->slug])}}"><img
                                                        width="120" height="120"
                                                        src="../../images/news/{{$new->image}}"
                                                        class="attachment-sprasa_small_feature size-sprasa_small_feature wp-post-image"
                                                        alt="" loading="lazy"></a></div>
                                            <div class="jl_m_right_content">
                                                <h2 class="entry-title"> <a href="{{route('index.detail', ['slug' => $new->slug])}}" tabindex="-1">{{$new->name}}</a></h2>
                                                <span class="jl_post_meta">Lượt xem: {{$new->view}}</span>
                                                <span
                                                    class="jl_post_meta"><span class="jl_author_img_w"><i
                                                            class="jli-user"></i><a href="{{route('index.detail', ['slug' => $new->slug])}}"
                                                            title="Posts by Spraya"
                                                            rel="author">{{$new->users->name}}</a></span><span
                                                        class="post-date"><i class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    {{-- Câu chuyện hôm nay --}}
    <section class="home_section7">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="blockid_84d79c5" class="block-section jl-main-block">
                        <div class="jl_grid_wrap_f jl_sf_grid jl_clear_at">
                            <div class="jl-roww content-inner jl-col3 jl-col-row">
                                <div class="jl_sec_title">
                                    <h3 class="jl_title_c"><span>Câu chuyện hôm nay</span></h3>
                                </div>
                                @foreach ($newsDays as $new)
                                <div class="jl-grid-cols">
                                    <div class="p-wraper post-1614">
                                        <div class="jl_m_right jl_sm_list jl_ml jl_clear_at">
                                            <div class="jl_m_right_w">
                                                <div class="jl_m_right_img jl_radus_e"><a href="{{route('index.detail', ['slug' => $new->slug])}}"><img
                                                            width="450" height="450"
                                                            style="height: 150px; width: 435px;"
                                                            src="../../images/news/{{$new->image}}"
                                                            class="attachment-sprasa_feature_small size-sprasa_feature_small wp-post-image"
                                                            alt="" loading="lazy"></a></div>
                                                <div class="jl_m_right_content"> <span class="jl_f_cat"><a
                                                            class="post-category-color-text"
                                                            style="background: #eba845;"
                                                            href="{{route('index.category', ['slug' => $new->slug])}}">{{$new->category->name}}</a></span>
                                                    <h2 class="entry-title"><a href="{{route('index.detail', ['slug' => $new->slug])}}">{{$new->name}}</a></h2><span
                                                        class="jl_post_meta"> <span
                                                            class="jl_author_img_w"><i
                                                                class="jli-user"></i><a href="{{route('index.detail', ['slug' => $new->slug])}}"
                                                                title="Posts by Spraya"
                                                                rel="author">{{$new->users->name}}</a></span><span
                                                            class="post-date"><i class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span><span class="post-read-time"><i
                                                                class="jli-watch-2"></i>{{$new->created_at->diffForHumans()}}</span></span>
                                                                <p>{!!$new->info!!}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div><!-- end content -->
@endsection
