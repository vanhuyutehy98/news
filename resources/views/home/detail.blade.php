@extends('home.layout.index')
@section('title')
    chi tiet
@endsection
@section('style')
    <style>
        .jl_m_right_img .wp-post-image-custom {
            width: 100%;
            height: 90px;
        }
        .jl_m_right_img .wp-post-image1 {
            width: 100%;
            height: 220px;
        }
    </style>
@endsection
@section('main')
    <section id="content_main" class="clearfix jl_spost">
        <div class="container">
            <div class="row main_content">
                <div class="col-md-8 loop-large-post" id="content">
                    <div class="widget_container content_page">
                        <!-- start post -->
                        <div class="post-2970 post type-post status-publish format-gallery has-post-thumbnail hentry category-business tag-inspiration tag-morning tag-tip tag-tutorial post_format-post-format-gallery"
                            id="post-2970">
                            <div class="single_section_content box blog_large_post_style">
                                <div class="jl_single_style2">
                                    <div
                                        class="single_post_entry_content single_bellow_left_align jl_top_single_title jl_top_title_feature">
                                        <span class="meta-category-small single_meta_category"><a
                                                class="post-category-color-text" style="background:#eba845"
                                                href="{{route('index.category', ['slug' => $new->category->slug])}}">{{$new->category->name}}</a></span>
                                        <h1 class="single_post_title_main">{{$new->name}}</h1>
                                        {{-- {!!$new->info!!} --}}
                                        <p class="post_subtitle_text"></p> <span
                                            class="jl_post_meta"><span class="jl_author_img_w"><i
                                                    class="jli-user"></i><a href="#"
                                                    title="Posts by Spraya" rel="author">{{$new->users->name}}</a></span><span
                                                class="post-date"><i class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span><span class="post-read-time"><i
                                                    class="jli-watch-2"></i>{{$new->created_at->diffForHumans()}}</span><span
                                                class="meta-comment"><i class="jli-comments"></i><a
                                                    href="#respond">{{$new->comment->count()}}</a></span></span>
                                    </div>
                                    <div class="jl_slide_wrap_s jl_clear_at">
                                        <div class="jl_ar_top jl_clear_at">
                                            <div class="jl-w-slider jl_full_feature_w">
                                                <div class="jl-eb-slider jelly_loading_pro" data-arrows="true"
                                                    data-play="true" data-effect="false" data-speed="500"
                                                    data-autospeed="7000" data-loop="true" data-dots="true"
                                                    data-swipe="true" data-items="1" data-xs-items="1"
                                                    data-sm-items="1" data-md-items="1" data-lg-items="1"
                                                    data-xl-items="1">
                                                    <div class="slide">
                                                        <div class="slide-inner jl_radus_e"><img
                                                                width="100%"
                                                                src="../../images/news/{{$new->image}}"
                                                                alt="">
                                                            <div class="background_over_image"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post_content_w">
                                    <div class="post_sw">
                                        <div class="post_s">
                                            <div class="jl_single_share_wrapper jl_clear_at">
                                                <ul class="single_post_share_icon_post">
                                                    <li class="single_post_share_facebook"><a href="{{$setting->facebookLink}}"
                                                            target="_blank"><i class="jli-facebook"></i></a>
                                                    </li>
                                                    <li class="single_post_share_twitter"><a href="{{$setting->youtobeLink}}"
                                                            target="_blank"><i class="jli-youtube"
                                                            aria-hidden="true"></i></a>
                                                    </li>
                                                </ul>
                                            </div><span class="single-post-meta-wrapper jl_sfoot">
                                                <span
                                                    class="view_options"><i
                                                        class="jli-view-o"></i><span>{{$new->view}}</span></span></span>
                                        </div>
                                    </div>
                                    <div class="post_content jl_content">
                                        {!!$new->describe!!}
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="single_tag_share ">
                                    <div class="tag-cat">
                                        <ul class="single_post_tag_layout">
                                            @foreach ($tags as $tag)
                                                <li><a href="{{route('index.category', ['slug' => $new->category->slug, 'tag' => $tag->id])}}" rel="tag">{{$tag->name}}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="postnav_w">
                                    <div class="postnav_left">
                                        <div class="single_post_arrow_content"><a href="#" id="prepost"> <span
                                                    class="jl_cpost_nav"> <span class="jl_post_nav_link"><i
                                                            class="jli-left-arrow"></i>Bài đang xem</span><span class="jl_cpost_title">{{$new->name}}</span></span></a></div>
                                    </div>
                                </div>
                                @foreach ($comments as $comment)
                                <div class="auth">
                                    <div class="author-info jl_info_auth">
                                        <div class="author-avatar"><img
                                                src="img/woman-sitting-and-smiling-1858175.jpg" width="165"
                                                height="165" alt="Spraya"
                                                class="avatar avatar-165 wp-user-avatar wp-user-avatar-165 alignnone photo">
                                        </div>
                                        <div class="author-description">
                                            <h5><a href="#">{{$comment->name}}</a></h5>
                                            <ul class="jl_auth_link clearfix">
                                                <li><a href="#" target="_blank"><i
                                                            class="jli-link"></i></a></li>
                                                <li><a href="#" target="_blank"><i
                                                            class="jli-linkedin"></i></a></li>
                                                <li><a href="#" target="_blank"><i
                                                            class="jli-rss"></i></a></li>
                                            </ul>
                                            <p>{{$comment->comnent}}</p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                
                                <div id="respond" class="comment-respond">
                                    {!! alert_success(session('thong-bao-thanh-cong')) !!}
                                    @if (Auth::guard('guest')->check())
                                         <h3 id="reply-title" class="comment-reply-title">Để lại một bình luận</h3>
                                        <form action="{{route('index.addComment', ['id' => $new->id])}}" action="https://jellywp.com/html/sprasa-preview/post-layout-1.html"
                                            method="post" id="commentform" class="comment-form">
                                            @csrf
                                            <p class="comment-notes"><span id="email-notes">Địa chỉ email của bạn sẽ không được công bố. Các trường bắt buộc được đánh dấu *</span></p>
                                            <p class="comment-form-comment">
                                                <textarea class="u-full-width" id="comment" name="comment" cols="45" rows="8" aria-required="true"
                                                    placeholder="Bình luận"></textarea>
                                            </p>
                                            
                                            <div class="form-fields row"><span
                                                    class="comment-form-author col-md-4"><input id="author" disabled
                                                        name="author" type="text" value="{{Auth::guard('guest')->user()->name}}" size="30"
                                                        placeholder="Họ và tên"></span><span
                                                    class="comment-form-email col-md-4"><input id="email" disabled
                                                        name="email" type="text"value="{{Auth::guard('guest')->user()->email}}" size="30"
                                                        placeholder="Email"></span>
                                            </div>
                                            <p class="form-submit"><input name="submit" type="submit"
                                                    id="submit" class="submit" value="Bình luận"><input
                                                    type="hidden" name="comment_post_ID" id="comment_post_ID"></p>
                                        </form>
                                    @else
                                    <p class="comment-reply-title">Bạn chưa đăng nhập, <a style="color: #eba845" href="{{route('index.login')}}">đăng nhập</a> để viết bình luận!</p>
                                    @endif
                                </div>
                                <div class="related-posts">
                                    <h4>Những bài viết liên quan</h4>
                                    <div class="single_related_post">
                                        @foreach ($relatedArticles as $new)
                                        <div class="jl_m_right jl_m_list jl_m_img">
                                            <div class="jl_m_right_w">
                                                <div class="jl_m_right_img jl_radus_e"><a href="{{route('index.detail', ['slug' => $new->slug])}}"><img
                                                            width="500" height="350"
                                                            src="../../images/news/{{$new->image}}"
                                                            class="attachment-sprasa_slider_grid_small size-sprasa_slider_grid_small wp-post-image wp-post-image1"
                                                            alt="" loading="lazy"></a></div>
                                                <div class="jl_m_right_content"> <span
                                                        class="jl_f_cat"><a
                                                            class="post-category-color-text"
                                                            style="background:#eba845"
                                                            href="{{route('index.category', ['slug' => $new->slug])}}">{{$new->category->name}}</a></span>
                                                    <h2 class="entry-title"> <a href="{{route('index.detail', ['slug' => $new->slug])}}" tabindex="-1">{{$new->name}}</a></h2><span
                                                        class="jl_post_meta"><span
                                                            class="jl_author_img_w"><i
                                                                class="jli-user"></i><a href="{{route('index.category', ['slug' => $new->slug])}}"
                                                                title="Posts by Spraya"
                                                                rel="author">{{$new->users->name}}</a></span><span
                                                            class="post-date"><i
                                                                class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span><span class="post-read-time"><i
                                                                class="jli-watch-2"></i>{{$new->created_at->diffForHumans()}}</span></span>
                                                    <p>{!!$new->info!!}</p>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        
                                    </div>
                                </div>

                            </div>
                        </div><!-- end post -->
                        <div class="brack_space"></div>
                    </div>
                </div>
                <div class="col-md-4" id="sidebar">
                    <div class="jl_sidebar_w">
                        <div id="sprasa_widget_social_counter_c-2" class="widget jl-widget-social-counter">
                            <div class="widget-title">
                                <h2 class="jl_title_c">Chúng tôi ở đây</h2>
                            </div>
                            <div
                                class="jl_social_counter social_counter_wraper jl_count_style_2 social_counter_item>">
                                <div class="jlsocial-element jl-facebook jl_radus_e"><a target="_blank"
                                        href="{{$setting->facebookLink}}" class="facebook"
                                        title="facebook"></a> <span class="jlsocial-element-left"> <i
                                            class="jli-facebook" aria-hidden="true"></i> <span
                                            class="num-count">228.8k</span></span> <span
                                        class="jlsocial-element-right">Theo dõi</span></div>
                                <div class="jlsocial-element jl-youtube jl_radus_e"><a target="_blank"
                                        href="{{$setting->youtobeLink}}" title="Youtube"></a> <span
                                        class="jlsocial-element-left"> <i class="jli-youtube"
                                            aria-hidden="true"></i> <span
                                            class="num-count">65.5k</span></span> <span
                                        class="jlsocial-element-right">Đăng kí kênh</span></div>
                            </div>
                        </div>
                        <div id="sprasa_recent_post_text_widget-9" class="widget post_list_widget">
                            <div class="widget_jl_wrapper">
                                <div class="ettitle">
                                    <div class="widget-title">
                                        <h2 class="jl_title_c">Xem nhiều</h2>
                                    </div>
                                </div>
                                <div class="bt_post_widget">
                                    @foreach ($newViews as $new)
                                    <div class="jl_m_right jl_sm_list jl_ml jl_clear_at">
                                        <div class="jl_m_right_w">
                                            <div class="jl_m_right_img jl_radus_e"><a href="{{route('index.detail', ['slug' => $new->slug])}}"><img
                                                        width="120" height="120"
                                                        src="../../images/news/{{$new->image}}"
                                                        class="attachment-sprasa_small_feature size-sprasa_small_feature wp-post-image wp-post-image-custom"
                                                        alt="" loading="lazy"></a></div>
                                            <div class="jl_m_right_content">
                                                <h2 class="entry-title"> <a href="{{route('index.detail', ['slug' => $new->slug])}}" tabindex="-1">{{$new->name}}</a></h2>
                                                <span class="jl_post_meta">Lượt xem: {{$new->view}}</span>
                                                <span
                                                    class="jl_post_meta"><span class="jl_author_img_w"><i
                                                            class="jli-user"></i><a href="{{route('index.detail', ['slug' => $new->slug])}}"
                                                            title="Posts by Spraya"
                                                            rel="author">{{$new->users->name}}</a></span><span
                                                        class="post-date"><i class="jli-pen"></i>{{ date('d-m-Y', strtotime($new->created_at)) }}</span></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- end content -->
@endsection

