@extends('home.layout.index')
@section('title')
    404
@endsection
@section('main')
    <section id="content_main" class="clearfix">
        <div class="container">
            <div class="row main_content">
                <!-- begin content -->
                <div class="col-md-12 page_error_404">
                    <h1 class="big"> 404 </h1>
                    <p class="description">OOOOOPS! The page you are looking for not exist</p> <a
                        class="link_home404" href="#"> Back to Home </a>
                </div>
            </div>
        </div>
    </section>
@endsection
