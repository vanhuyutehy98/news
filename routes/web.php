<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '', 'namespace' => 'Home'], function () {
    Route::get('', 'HomeController@index')->name('index');
    Route::get('xem/{slug}', 'HomeController@detail')->name('index.detail');
    Route::get('danh-muc/{slug}', 'HomeController@category')->name('index.category');
    Route::get('login', 'LoginController@login')->name('index.login');
    Route::post('login', 'LoginController@loginPost');

    Route::get('dang-ki', 'LoginController@dk')->name('index.dk');
    Route::post('dang-ki', 'LoginController@dkPost');
    Route::get('dang-xuat', 'LoginController@logout')->name('index.logout');
    Route::post('add-comment/{id}', 'HomeController@addComment')->name('index.addComment');
    Route::get('404', 'HomeController@page404')->name('index.page404');
    Route::get('timkiem', 'HomeController@search')->name('index.search');
});
Route::get('authenticate', 'Backend\Authentication\LoginController@SignIn')->name('admin.sigin')->middleware('CheckLogout');;
Route::post('authenticate', 'Backend\Authentication\LoginController@SignInPost');
Route::group(['prefix' => '', 'namespace' => 'Backend', 'middleware' => 'CheckLogin'], function () {
    Route::get('admin', 'HomeController@index')->name('admin');
    Route::get('search', 'HomeController@search')->name('admin.search');
    Route::get('logout', 'Authentication\LoginController@Logout')->name('admin.logout');
    // quan ly user admin
    Route::group(['prefix' => '', 'middleware' => ['role:super-admin']], function () {
        Route::group(['prefix' => '', 'middleware' => 'can:view_user_admin'], function () {
            Route::get('danh-sach-quan-tri.html', 'UserAdminController@index')->name('admin.user_admin.index');
            Route::get('chi-tiet-quan-tri/{id}', 'UserAdminController@detail')->name('admin.user_admin.detail');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:add_user_admin'], function () {
            Route::get('them-quan-tri', 'UserAdminController@add')->name('admin.user_admin.add');
            Route::post('them-quan-tri', 'UserAdminController@store')->name('admin.user_admin.post');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:edit_user_admin'], function () {
            Route::get('sua-quan-tri/{id}', 'UserAdminController@edit')->name('admin.user_admin.edit');
            Route::post('sua-quan-tri/{id}', 'UserAdminController@update')->name('admin.user_admin.update');
        });
        Route::get('xoa-quan-tri/{id}', 'UserAdminController@remove')->name('admin.user_admin.remove')->middleware('can:remove_user_admin');
    });
    // quan ly user
    Route::group(['prefix' => '', 'namespace' => 'User'], function () {
        Route::group(['prefix' => '', 'middleware' => 'can:view_user'], function () {
            Route::get('danh-sach-user.html', 'UserController@index')->name('admin.user.index');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:add_user'], function () {
            Route::get('them-user', 'UserController@add')->name('admin.user.add');
            Route::post('them-user', 'UserController@store')->name('admin.user.post');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:edit_user'], function () {
            Route::get('sua-user/{id}', 'UserController@edit')->name('admin.user.edit');
            Route::post('sua-user/{id}', 'UserController@update')->name('admin.user.update');
        });
        Route::get('xoa-user/{id}', 'UserController@removev')->name('admin.user.remove')->middleware('can:remove_user');
    });
    // quan ly tags
    Route::group(['prefix' => ''], function () {
        Route::group(['prefix' => '', 'middleware' => 'can:view_tag'], function () {
            Route::get('danh-sach-tag.html', 'TagsController@index')->name('admin.tags.index');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:add_tag'], function () {
            Route::get('them-tag', 'TagsController@add')->name('admin.tags.add');
            Route::post('them-tag', 'TagsController@store')->name('admin.tags.post');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:edit_tag'], function () {
            Route::get('sua-tag/{id}', 'TagsController@edit')->name('admin.tags.edit');
            Route::post('sua-tag/{id}', 'TagsController@update')->name('admin.tags.update');
        });
        Route::get('xoa-tag/{id}', 'TagsController@remove')->name('admin.tags.remove')->middleware('can:remove_tag');
    });
    // quan ly danh muc
    Route::group(['prefix' => ''], function () {
        Route::group(['prefix' => '', 'middleware' => 'can:view_category'], function () {
            Route::get('danh-sach-danh-muc.html', 'CategoryController@index')->name('admin.category.index');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:add_category'], function () {
            Route::get('them-danh-muc', 'CategoryController@add')->name('admin.category.add');
            Route::post('them-danh-muc', 'CategoryController@store')->name('admin.category.post');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:edit_category'], function () {
            Route::get('sua-danh-muc/{slug}', 'CategoryController@edit')->name('admin.category.edit');
            Route::post('sua-danh-muc/{id}', 'CategoryController@update')->name('admin.category.update');
        });
        Route::get('xoa-danh-muc/{id}', 'CategoryController@remove')->name('admin.category.remove')->middleware('can:remove_category');
    });
    // quan ly bai viet
    Route::group(['prefix' => ''], function () {
        Route::group(['prefix' => '', 'middleware' => 'can:view_new'], function () {
            Route::get('danh-sach-bai-viet.html', 'NewController@index')->name('admin.news.index');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:add_new'], function () {
            Route::get('them-bai-viet', 'NewController@add')->name('admin.news.add');
            Route::post('them-bai-viet', 'NewController@store')->name('admin.news.post');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:edit_new'], function () {
            Route::get('sua-bai-viet/{id}', 'NewController@edit')->name('admin.news.edit');
            Route::post('sua-bai-viet/{id}', 'NewController@update')->name('admin.news.update');
        });
        Route::get('xoa-bai-viet/{id}', 'NewController@remove')->name('admin.news.remove')->middleware('can:remove_new');
    });
    // quan ly comment bai viet
    Route::group(['prefix' => ''], function () {
        Route::group(['prefix' => '', 'middleware' => 'can:view_comment'], function () {
            Route::get('danh-sach-binh-luan', 'CommentController@index')->name('admin.comment.index');
            Route::get('danh-sach-binh-luan-da-xu-ly', 'CommentController@success')->name('admin.comment.success');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:add_comment'], function () {
            Route::get('them-binh-luan', 'CommentController@add')->name('admin.comment.add');
            Route::post('them-binh-luan', 'CommentController@store')->name('admin.comment.post');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:edit_comment'], function () {
            Route::get('sua-binh-luan/{id}', 'CommentController@edit')->name('admin.comment.edit');
            Route::post('sua-binh-luan/{id}', 'CommentController@update')->name('admin.comment.update');
        });
        Route::get('xoa-binh-luan/{id}', 'CommentController@remove')->name('admin.comment.remove')->middleware('can:remove_comment');
    });

    Route::get('setting/{name}', 'HomeController@setting')->name('admin.setting');
    Route::post('setting/{name}', 'HomeController@settingPost');
});