-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 16, 2022 lúc 03:18 PM
-- Phiên bản máy phục vụ: 10.4.22-MariaDB
-- Phiên bản PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `news`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `describe` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `use_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `image`, `info`, `describe`, `active`, `use_id`, `created_at`, `updated_at`) VALUES
(5, 'Tuyển sinh', 'tuyen-sinh', 'tuyen.jpg', '<h1>Phương &aacute;n tuyển sinh của hai th&agrave;nh vi&ecirc;n ĐH Quốc gia HN</h1>', '<p>Khoa Luật v&agrave; Trường Quốc tế đều sử dụng chủ yếu điểm thi tốt nghiệp THPT để x&eacute;t tuyển (khoảng 55-60%)</p>\r\n\r\n<p>Năm 2022,&nbsp;<strong>Khoa Luật</strong>, Đại học Quốc gia H&agrave; Nội tuyển 750 chỉ ti&ecirc;u cho bốn chương tr&igrave;nh đ&agrave;o tạo gồm: Luật, Luật chất lượng cao, Luật kinh doanh v&agrave; Luật Thương mại quốc tế. Trường sử dụng nhiều phương thức nhằm hướng tới nhiều đối tượng th&iacute; sinh. Tuy nhi&ecirc;n, phương thức ch&iacute;nh vẫn l&agrave; sử dụng kết quả thi tốt nghiệp THPT (chiếm 450/750 chỉ ti&ecirc;u).</p>\r\n\r\n<p>Tiếp đến l&agrave; tuyển bằng kết quả thi đ&aacute;nh gi&aacute; năng lực do Đại học Quốc gia H&agrave; Nội tổ chức với 150 chỉ ti&ecirc;u. Ở phương thức n&agrave;y, th&iacute; sinh phải đạt điểm tối thiểu 80/150.</p>\r\n\r\n<p>Với 150 chỉ ti&ecirc;u c&ograve;n lại, Khoa Luật x&eacute;t tuyển tuyển thẳng, ưu ti&ecirc;n x&eacute;t tuyển theo quy chế của Bộ Gi&aacute;o dục v&agrave; Đ&agrave;o tạo; c&aacute;c em c&oacute; chứng chỉ A-Level với kết quả ba m&ocirc;n (bắt buộc c&oacute; To&aacute;n hoặc Ngữ văn) đạt 60/100 trở l&ecirc;n, SAT từ 1100/1600 hoặc ACT 22/36. Khoa c&ograve;n x&eacute;t tuyển kết hợp th&iacute; sinh c&oacute; chứng chỉ IELTS 5.5 trở l&ecirc;n hoặc tương đương v&agrave; điểm thi hai m&ocirc;n c&ograve;n lại trong tổ hợp x&eacute;t tuyển đạt tối thiểu 14 trong kỳ thi tốt nghiệp THPT năm 2022.</p>\r\n\r\n<p>Ri&ecirc;ng với SAT, th&iacute; sinh lưu &yacute; khai b&aacute;o m&atilde; đăng k&yacute; của Đại học Quốc gia H&agrave; Nội l&agrave; 7853-Vietnam National University-Hanoi với The College Board.</p>\r\n\r\n<p>Ngo&agrave;i ra, khoa c&ograve;n tuyển th&iacute; sinh l&agrave; người nước ngo&agrave;i đ&aacute;p ứng y&ecirc;u cầu của Bộ Gi&aacute;o dục v&agrave; Đ&agrave;o tạo v&agrave; Đại học Quốc gia H&agrave; Nội.</p>\r\n\r\n<p>Khoa Luật lưu &yacute; ri&ecirc;ng với ng&agrave;nh Luật chất lượng cao, điều kiện ngoại ngữ đầu v&agrave;o l&agrave; kết quả m&ocirc;n Tiếng Anh của kỳ thi tốt nghiệp THPT phải đạt tối thiểu 6, kết quả học tập từng kỳ ở bậc THPT (6 học kỳ) m&ocirc;n tiếng Anh đạt tối thiểu 7 hoặc sử dụng chứng chỉ quốc tế tương đương.</p>', 1, 1, '2022-06-14 07:15:09', '2022-06-14 07:18:54'),
(6, 'Tin tức', 'tin-tuc', 'tu-van.jpg', '<h1>Băn khoăn về ng&agrave;nh An to&agrave;n th&ocirc;ng tin</h1>', '<p>Em x&aacute;c định đăng k&yacute; x&eacute;t tuyển v&agrave;o ng&agrave;nh An to&agrave;n th&ocirc;ng tin trong đợt tuyển sinh đại học sắp tới nhưng vẫn c&ograve;n một số băn khoăn.</p>\r\n\r\n<p>Chỉ c&ograve;n một th&aacute;ng nữa, em tham gia kỳ thi tốt nghiệp THPT v&agrave; sẽ sử dụng kết quả thi để x&eacute;t tuyển đại học. Em đ&atilde; x&aacute;c định được ng&agrave;nh em chọn l&agrave; An to&agrave;n th&ocirc;ng tin nhưng c&ograve;n một số thắc mắc:</p>\r\n\r\n<p>1. Ng&agrave;nh An to&agrave;n th&ocirc;ng tin v&agrave; An ninh mạng c&oacute; điểm kh&aacute;c biệt hay kh&ocirc;ng?</p>\r\n\r\n<p>2. Trường c&ocirc;ng lập n&agrave;o c&oacute; ng&agrave;nh n&agrave;y với mức học ph&iacute; ph&ugrave; hợp? Em muốn hỏi c&acirc;u n&agrave;y v&igrave; năm nay c&aacute;c đại học đều c&oacute; xu hướng tăng học ph&iacute;, g&acirc;y kh&oacute; khăn cho gia đ&igrave;nh em.</p>\r\n\r\n<p>3. Hiện, em c&oacute; laptop nhưng đời kh&aacute; cũ. Trong hai năm đầu theo học ng&agrave;nh An to&agrave;n th&ocirc;ng tin, em c&oacute; cần sử dụng laptop thường xuy&ecirc;n kh&ocirc;ng v&agrave; cấu h&igrave;nh laptop như n&agrave;o l&agrave; đủ để học ng&agrave;nh n&agrave;y?</p>\r\n\r\n<p>4. Em c&oacute; cần phải giỏi về lập tr&igrave;nh khi học ng&agrave;nh n&agrave;y kh&ocirc;ng?</p>\r\n\r\n<p>Rất mong nhận được chia sẻ từ mọi người.</p>', 1, 1, '2022-06-14 07:18:43', '2022-06-14 07:45:58'),
(7, 'Giáo dục 4.0', 'giao-duc-40', 'giao-duc-4.jpg', '<h1>Pixta Vietnam hợp t&aacute;c đ&agrave;o tạo với đơn vị gi&aacute;o dục Việt</h1>', '<p>Pixta Vietnam hợp tác đào tạo và tuy&ecirc;̉n dụng cùng FUNiX nhằm t&igrave;m kiếm v&agrave; x&acirc;y dựng nguồn nh&acirc;n sự chất lượng cao.</p>\r\n\r\n<p>Pixta l&agrave; tập đo&agrave;n Nhật Bản hoạt động trong lĩnh vực cung cấp ảnh, video, audio... c&oacute; bản quyền. Với chức năng ph&aacute;t triển sản phẩm cho tập đo&agrave;n, Pixta Vietnam - chi nh&aacute;nh tại Việt Nam đang cần tuyển nhiều nh&acirc;n sự c&ocirc;ng nghệ th&ocirc;ng tin như Data Scientist engineer, Full Stack developer, DevOps Engineer, Scrum Master... với dải lương từ 500 l&ecirc;n đến 4.500 USD.</p>\r\n\r\n<p>Với mục ti&ecirc;u t&igrave;m kiếm nguồn nh&acirc;n sự chất lượng, Pixta Vietnam hợp t&aacute;c c&ugrave;ng đơn vị đ&agrave;o tạo trực tuyến FUNiX từ th&aacute;ng 4. Theo đ&oacute;, doanh nghiệp cam kết cử chuy&ecirc;n gia tham gia x&acirc;y dựng chương tr&igrave;nh đ&agrave;o tạo, l&agrave;m mentor (cố vấn) tại FUNiX, đồng thời, ch&agrave;o đ&oacute;n sinh vi&ecirc;n đến thực tập v&agrave; l&agrave;m việc.</p>\r\n\r\n<p>&nbsp;</p>', 1, 1, '2022-06-14 07:20:38', '2022-06-14 07:20:48'),
(8, 'Du học', 'du-hoc', 'du-hoc.jpg', '<h1>Cơ hội giao lưu với đại diện Đại học Y khoa SGU - Mỹ</h1>', '<p>TP HCMC&ocirc;ng ty Unimates c&ugrave;ng Đại học Y khoa St. George - SGU (Mỹ) tổ chức hội thảo về du học v&agrave; h&agrave;nh nghề y ở Mỹ với sự tham gia của đại diện trường.</p>\r\n\r\n<p>Học v&agrave; h&agrave;nh nghề y kh&ocirc;ng phải l&agrave; h&agrave;nh tr&igrave;nh dễ d&agrave;ng đối với c&aacute;c bạn trẻ ở bất cứ quốc gia n&agrave;o, đặc biệt l&agrave; Mỹ. Do đ&oacute;, hai đơn vị tổ chức sự kiện nhằm gi&uacute;p sinh vi&ecirc;n Việt Nam t&igrave;m hiểu, nắm bắt cơ hội trở th&agrave;nh b&aacute;c sĩ tại xứ sở cờ hoa th&ocirc;ng qua cuộc gặp gỡ trực tiếp với c&ocirc; Nalinee Jiemvithayanukul - Trưởng Bộ phận Tuyển sinh khu vực Đ&ocirc;ng Nam &Aacute; của SGU (St. George&rsquo;s School of Medicine).</p>\r\n\r\n<p>Hội thảo diễn ra l&uacute;c 16h ng&agrave;y 27/6 tại Unimates Education, tầng 12, to&agrave; nh&agrave; International Plaza, 343 Phạm Ngũ L&atilde;o, Quận 1, TP HCM. Học sinh lớp 11, 12; sinh vi&ecirc;n năm 1,2 hoặc đ&atilde; tốt nghiệp c&aacute;c trường nh&oacute;m ng&agrave;nh y dược, ho&aacute;, sinh v&agrave; phụ huynh muốn h&agrave;nh nghề tại Mỹ c&oacute; thể&nbsp;<a href=\"https://www.unimates.edu.vn/events/hoi-thao-du-hoc-hanhh-nghe-y-tai-my-canada-cung-sgu/\" rel=\"nofollow\">đăng k&yacute;</a>&nbsp;tham gia miễn ph&iacute;. Tại hội thảo, người tham dự c&oacute; thể mang theo bảng điểm 4 năm gần nhất để được đ&aacute;nh gi&aacute; cơ hội tr&uacute;ng tuyển v&agrave;o trường v&agrave; tư vấn lộ tr&igrave;nh th&iacute;ch hợp.</p>\r\n\r\n<p>Theo b&aacute;o c&aacute;o năm 2021 của Cục thống k&ecirc; lao động - Bộ Lao động Mỹ, mức thu nhập của c&aacute;c b&aacute;c sĩ tại nước n&agrave;y đạt khoảng 208.000 USD một năm, tương đương với hơn 4,8 tỷ đồng. Trong khi đ&oacute;, Mỹ c&oacute; khoảng 727.000 b&aacute;c sĩ đang h&agrave;nh nghề. Số việc l&agrave;m cho ng&agrave;nh cũng dự kiến tăng 3% v&agrave; cần bổ sung th&ecirc;m 22,700 b&aacute;c sĩ mỗi năm, đặc biệt l&agrave; sau Covid-19.</p>\r\n\r\n<p>&quot;Do đ&oacute;, hiện tại l&agrave; thời điểm &#39;v&agrave;ng&#39; để c&aacute;c bạn trẻ nắm bắt cơ hội, theo đuổi ước mơ h&agrave;nh nghề y tại Mỹ&quot;, đại diện ban tổ chức chia sẻ.</p>', 1, 1, '2022-06-14 07:22:03', '2022-06-14 07:22:03');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `state` int(11) DEFAULT NULL,
  `new_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `comnent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `comments`
--

INSERT INTO `comments` (`id`, `state`, `new_id`, `user_id`, `comnent`, `created_at`, `updated_at`, `name`) VALUES
(3, 0, 11, NULL, 'awdawdawd', '2022-06-15 06:22:37', '2022-06-15 06:22:37', 'Đinh Xuân Thu'),
(4, 1, 11, NULL, 'oke thi oke', '2022-06-15 06:24:09', '2022-06-15 06:24:09', 'Đinh Xuân Thu');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_08_19_000000_create_failed_jobs_table', 1),
(2, '2021_05_19_083552_create_permission_tables', 1),
(3, '2022_05_28_035019_create_tags_table', 1),
(4, '2022_05_28_035130_create_useradmin_table', 1),
(5, '2022_05_28_035153_create_categories_table', 1),
(6, '2022_05_28_035205_create_news_table', 1),
(7, '2022_05_28_035218_create_user_table', 1),
(8, '2022_05_28_035234_create_comments_table', 1),
(9, '2022_05_28_111949_create_tag_new_table', 1),
(10, '2022_05_28_155257_create_setting_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\userAdmin', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `describe` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `user_adnin` int(10) UNSIGNED NOT NULL,
  `cat_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`id`, `name`, `slug`, `image`, `info`, `describe`, `view`, `active`, `user_adnin`, `cat_id`, `created_at`, `updated_at`) VALUES
(5, 'Phương án tuyển sinh của hai thành viên ĐH Quốc gia HN', 'phuong-an-tuyen-sinh-cua-hai-thanh-vien-dh-quoc-gia-hn', 'phuong-an-tuyen-sinh-cua-hai-thanh-vien-dh-quoc-gia-hn.jpg', '<p>Khoa Luật v&agrave; Trường Quốc tế đều sử dụng chủ yếu điểm thi tốt nghiệp THPT để x&eacute;t tuyển (khoảng 55-60%)</p>', '<p>Năm 2022,&nbsp;<strong>Khoa Luật</strong>, Đại học Quốc gia H&agrave; Nội tuyển 750 chỉ ti&ecirc;u cho bốn chương tr&igrave;nh đ&agrave;o tạo gồm: Luật, Luật chất lượng cao, Luật kinh doanh v&agrave; Luật Thương mại quốc tế. Trường sử dụng nhiều phương thức nhằm hướng tới nhiều đối tượng th&iacute; sinh. Tuy nhi&ecirc;n, phương thức ch&iacute;nh vẫn l&agrave; sử dụng kết quả thi tốt nghiệp THPT (chiếm 450/750 chỉ ti&ecirc;u).</p>\r\n\r\n<p>Tiếp đến l&agrave; tuyển bằng kết quả thi đ&aacute;nh gi&aacute; năng lực do Đại học Quốc gia H&agrave; Nội tổ chức với 150 chỉ ti&ecirc;u. Ở phương thức n&agrave;y, th&iacute; sinh phải đạt điểm tối thiểu 80/150.</p>\r\n\r\n<p>Với 150 chỉ ti&ecirc;u c&ograve;n lại, Khoa Luật x&eacute;t tuyển tuyển thẳng, ưu ti&ecirc;n x&eacute;t tuyển theo quy chế của Bộ Gi&aacute;o dục v&agrave; Đ&agrave;o tạo; c&aacute;c em c&oacute; chứng chỉ A-Level với kết quả ba m&ocirc;n (bắt buộc c&oacute; To&aacute;n hoặc Ngữ văn) đạt 60/100 trở l&ecirc;n, SAT từ 1100/1600 hoặc ACT 22/36. Khoa c&ograve;n x&eacute;t tuyển kết hợp th&iacute; sinh c&oacute; chứng chỉ IELTS 5.5 trở l&ecirc;n hoặc tương đương v&agrave; điểm thi hai m&ocirc;n c&ograve;n lại trong tổ hợp x&eacute;t tuyển đạt tối thiểu 14 trong kỳ thi tốt nghiệp THPT năm 2022.</p>\r\n\r\n<p>Ri&ecirc;ng với SAT, th&iacute; sinh lưu &yacute; khai b&aacute;o m&atilde; đăng k&yacute; của Đại học Quốc gia H&agrave; Nội l&agrave; 7853-Vietnam National University-Hanoi với The College Board.</p>\r\n\r\n<p>Ngo&agrave;i ra, khoa c&ograve;n tuyển th&iacute; sinh l&agrave; người nước ngo&agrave;i đ&aacute;p ứng y&ecirc;u cầu của Bộ Gi&aacute;o dục v&agrave; Đ&agrave;o tạo v&agrave; Đại học Quốc gia H&agrave; Nội.</p>\r\n\r\n<p>Khoa Luật lưu &yacute; ri&ecirc;ng với ng&agrave;nh Luật chất lượng cao, điều kiện ngoại ngữ đầu v&agrave;o l&agrave; kết quả m&ocirc;n Tiếng Anh của kỳ thi tốt nghiệp THPT phải đạt tối thiểu 6, kết quả học tập từng kỳ ở bậc THPT (6 học kỳ) m&ocirc;n tiếng Anh đạt tối thiểu 7 hoặc sử dụng chứng chỉ quốc tế tương đương.</p>\r\n\r\n<p>Tương tự c&aacute;c th&agrave;nh vi&ecirc;n kh&aacute;c của Đại học Quốc gia H&agrave; Nội, trường x&eacute;t tuyển bằng kết quả thi tốt nghiệp THPT l&agrave; ch&iacute;nh với 55% tổng chỉ ti&ecirc;u. Ngo&agrave;i ra, trường tuyển thẳng v&agrave; ưu ti&ecirc;n x&eacute;t tuyển, dựa v&agrave;o kỳ thi đ&aacute;nh gi&aacute; năng lực (HSA) v&agrave; x&eacute;t tuyển theo c&aacute;c phương thức kh&aacute;c như dựa v&agrave;o chứng chỉ A-Level, SAT, ACT, IELTS, TOEFL iBT, bằng T&uacute; t&agrave;i quốc tế (IB) v&agrave; kết quả thi đ&aacute;nh gi&aacute; tư duy của Đại học B&aacute;ch khoa H&agrave; Nội.</p>\r\n\r\n<p>Với phương thức x&eacute;t kết quả thi đ&aacute;nh gi&aacute; năng lực của Đại học Quốc gia H&agrave; Nội, th&iacute; sinh cần đạt 80/150 điểm trở l&ecirc;n, tương tự Khoa Luật. Tuy nhi&ecirc;n, th&iacute; sinh cần đ&aacute;p ứng điều kiện phụ l&agrave; c&oacute; điểm trung b&igrave;nh 5 học kỳ đầu bậc THPT của m&ocirc;n Tiếng Anh, Ph&aacute;p hoặc Nhật từ 7 trở l&ecirc;n.</p>\r\n\r\n<p>C&aacute;c em c&oacute; thể sử dụng kết quả kỳ thi đ&aacute;nh gi&aacute; tư duy của Đại học B&aacute;ch khoa H&agrave; Nội với số điểm 18/30 điểm ở tổ hợp To&aacute;n - Đọc hiểu - Khoa học tự nhi&ecirc;n cộng điều kiện phụ về m&ocirc;n Tiếng Anh bậc THPT hoặc To&aacute;n - Đọc hiểu - Tiếng Anh. Tuy nhi&ecirc;n, kết quả từ kỳ thi đ&aacute;nh gi&aacute; tư duy chỉ &aacute;p dụng với một số ng&agrave;nh gồm Hệ thống th&ocirc;ng tin quản l&yacute;, Tin học v&agrave; kỹ thuật m&aacute;y t&iacute;nh, Ph&acirc;n t&iacute;ch dữ liệu kinh doanh, Tự động h&oacute;a v&agrave; Tin học, C&ocirc;ng nghệ th&ocirc;ng tin ứng dụng, Kỹ thuật hệ thống c&ocirc;ng nghiệp v&agrave; Logistics, C&ocirc;ng nghệ t&agrave;i ch&iacute;nh v&agrave; Kinh doanh số.</p>\r\n\r\n<p>Nếu c&oacute; chứng chỉ IELTS 5.5 trở l&ecirc;n hoặc TOEFL iBT từ 72, th&iacute; sinh cần đạt tổng điểm thi tốt nghiệp THPT hai m&ocirc;n thi c&ograve;n lại trong tổ hợp từ 14 trở l&ecirc;n để đủ điều kiện x&eacute;t tuyển.</p>\r\n\r\n<p>Đại học Quốc gia H&agrave; Nội c&oacute; 12 trường, khoa trực thuộc. Trước đ&oacute;, c&aacute;c th&agrave;nh vi&ecirc;n kh&aacute;c của đơn vị n&agrave;y đ&atilde; c&ocirc;ng bố phương &aacute;n tuyển sinh, chỉ c&ograve;n Đại học Y Dược chưa c&ocirc;ng bố.</p>\r\n\r\n<p>&nbsp;</p>', NULL, 0, 1, 5, '2022-06-14 07:30:17', '2022-06-15 06:05:37'),
(6, 'Phương án tuyển sinh của 5 trường thuộc ĐH Quốc gia Hà Nội', 'phuong-an-tuyen-sinh-cua-5-truong-thuoc-dh-quoc-gia-ha-noi', 'phuong-an-tuyen-sinh-cua-5-truong-thuoc-dh-quoc-gia-ha-noi.jpg', '<p>C&aacute;c trường thuộc Đại học Quốc gia H&agrave; Nội đều sử dụng kết quả thi đ&aacute;nh gi&aacute; năng lực nhưng vẫn d&agrave;nh một nửa chỉ ti&ecirc;u x&eacute;t bằng kết quả thi tốt nghiệp THPT</p>', '<p>Đại học Quốc gia H&agrave; Nội c&oacute; 12 trường, khoa trực thuộc. Hiện, 5 trong số n&agrave;y đ&atilde; c&ocirc;ng bố phương &aacute;n tuyển sinh năm 2022 với nhiều điểm tương đồng trong phương thức x&eacute;t tuyển v&agrave; ph&acirc;n bổ chỉ ti&ecirc;u.</p>\r\n\r\n<p><strong>Trường&nbsp;</strong><strong>Đại học C&ocirc;ng nghệ</strong>&nbsp;tuyển 980 chỉ ti&ecirc;u cho 10 ng&agrave;nh đ&agrave;o tạo chuẩn v&agrave; 700 chỉ ti&ecirc;u cho ba ng&agrave;nh đ&agrave;o tạo chất lượng cao.</p>\r\n\r\n<p>Trường sử dụng s&aacute;u phương thức x&eacute;t tuyển gồm x&eacute;t theo kết quả thi tốt nghiệp THPT, kết quả kỳ thi chuẩn h&oacute;a SAT hoặc ACT, chứng chỉ quốc tế A-Level của Trung t&acirc;m Khảo th&iacute; Đại học Cambridge, chứng chỉ tiếng Anh quốc tế (IELTS, TOEFL) kết hợp với điểm hai m&ocirc;n To&aacute;n v&agrave; Vật l&yacute; trong kỳ thi THPT, kết quả thi đ&aacute;nh gi&aacute; năng lực do Đại học Quốc gia H&agrave; Nội tổ chức, v&agrave; x&eacute;t tuyển thẳng, ưu ti&ecirc;n x&eacute;t tuyển học sinh giỏi quốc gia, tỉnh/th&agrave;nh phố, học sinh hệ chuy&ecirc;n.</p>\r\n\r\n<p><strong>Trường Đại học Gi&aacute;o dục</strong>&nbsp;tuyển 16 ng&agrave;nh đ&agrave;o tạo bằng ba nh&oacute;m phương thức gồm: x&eacute;t tuyển thẳng theo quy định của Bộ Gi&aacute;o dục v&agrave; Đ&agrave;o tạo, quy định của Đại học Quốc gia H&agrave; Nội, kết hợp chứng chỉ tiếng Anh quốc tế với kết quả thi THPT (dự kiến 50 chỉ ti&ecirc;u); dựa v&agrave;o kết quả thi tốt nghiệp THPT (750); x&eacute;t theo kết quả thi đ&aacute;nh gi&aacute; năng lực năm 2022 của Đại học Quốc gia H&agrave; Nội (200).</p>\r\n\r\n<p>Đối với th&iacute; sinh đăng k&yacute; x&eacute;t tuyển ng&agrave;nh Gi&aacute;o dục mầm non, Đại học Gi&aacute;o dục tổ chức đ&aacute;nh gi&aacute; năng khiếu. Th&iacute; sinh chỉ được c&ocirc;ng nhận tr&uacute;ng tuyển khi đạt điểm chuẩn theo quy định v&agrave; đạt ở phần đ&aacute;nh gi&aacute; năng khiếu n&agrave;y.</p>', 1, 1, 1, 5, '2022-06-14 07:30:26', '2022-06-15 06:38:58'),
(7, 'Học viện Ngân hàng thêm phương thức tuyển sinh', 'hoc-vien-ngan-hang-them-phuong-thuc-tuyen-sinh', 'hoc-vien-ngan-hang-them-phuong-thuc-tuyen-sinh.jpg', '<p>Năm 2022, Học viện Ng&acirc;n h&agrave;ng sử dụng 5 phương thức tuyển sinh, nhiều hơn năm ngo&aacute;i n&ecirc;n chỉ ti&ecirc;u dựa v&agrave;o điểm thi tốt nghiệp THPT giảm nhẹ.</p>', '<p>Giống như năm ngo&aacute;i, Học vi&ecirc;n Ng&acirc;n h&agrave;ng vẫn sử dụng c&aacute;c phương thức x&eacute;t tuyển thẳng, x&eacute;t kết quả học tập THPT (dự kiến 25% chỉ ti&ecirc;u), chứng chỉ ngoại ngữ quốc tế (15%) v&agrave; kết quả kỳ thi tốt nghiệp THPT (50%). So với năm ngo&aacute;i, tỷ lệ chỉ ti&ecirc;u x&eacute;t bằng kết quả thi tốt nghiệp giảm 10% do trường th&ecirc;m phương thức dựa v&agrave;o kết quả kỳ thi đ&aacute;nh gi&aacute; năng lực của Đại học Quốc gia H&agrave; Nội.</p>\r\n\r\n<p>Với x&eacute;t tuyển<strong>&nbsp;dựa v&agrave;o kết quả học tập THPT</strong>, th&iacute; sinh cần c&oacute; điểm trung b&igrave;nh cộng ba năm học của từng m&ocirc;n thuộc tổ hợp đạt từ 8 trở l&ecirc;n.</p>\r\n\r\n<p>Điểm x&eacute;t tuyển l&agrave; tổng điểm trung b&igrave;nh cộng ba m&ocirc;n c&ugrave;ng điểm ưu ti&ecirc;n v&agrave; điểm cộng đối tượng. Trong đ&oacute;, điểm ưu ti&ecirc;n khu vực, đối tượng theo quy chế tuyển sinh hiện h&agrave;nh. Điểm cộng đối tượng do trường quy định. Th&iacute; sinh được cộng hai điểm nếu học hệ chuy&ecirc;n của trường chuy&ecirc;n quốc gia; một điểm với th&iacute; sinh hệ kh&ocirc;ng chuy&ecirc;n của trường chuy&ecirc;n quốc gia v&agrave; hệ chuy&ecirc;n của trường chuy&ecirc;n tỉnh/th&agrave;nh phố.</p>\r\n\r\n<p>Ở phương thức&nbsp;<strong>dựa tr&ecirc;n chứng chỉ ngoại ngữ quốc tế</strong>, th&iacute; sinh cần c&oacute; một trong c&aacute;c chứng chỉ IELTS (Academic) đạt từ 6.0 trở l&ecirc;n, TOEFL iBT từ 72 điểm, chứng chỉ tiếng Nhật từ N3 (ri&ecirc;ng đối với ng&agrave;nh Kế to&aacute;n định hướng Nhật Bản v&agrave; Hệ thống th&ocirc;ng tin quản l&yacute; định hướng Nhật Bản).</p>\r\n\r\n<p>Với&nbsp;<strong>x&eacute;t kết quả thi tốt nghiệp THPT</strong>, Học viện Ng&acirc;n h&agrave;ng vẫn x&eacute;t tuyển dựa v&agrave;o tổng điểm ba m&ocirc;n trong tổ hợp cộng điểm ưu ti&ecirc;n.</p>\r\n\r\n<p>Phương thức mới được Học viện Ng&acirc;n h&agrave;ng sử dụng năm nay l&agrave;&nbsp;<strong>dựa v&agrave;o kết quả kỳ thi đ&aacute;nh gi&aacute; năng lực</strong>&nbsp;của Đại học Quốc gia H&agrave; Nội. Những th&iacute; sinh c&oacute; kết quả kỳ thi n&agrave;y đạt từ 100 điểm trở l&ecirc;n sẽ đủ điều kiện tham gia x&eacute;t tuyển.</p>\r\n\r\n<p>Ở tất cả phương thức (trừ x&eacute;t tuyển thẳng), Học viện Ng&acirc;n h&agrave;ng đều c&oacute; ngưỡng đảm bảo chất lượng v&agrave; y&ecirc;u cầu th&iacute; sinh phải đạt được. Ngưỡng n&agrave;y sẽ được th&ocirc;ng b&aacute;o sau khi c&oacute; kết quả thi tốt nghiệp THPT.</p>', 1, 1, 1, 5, '2022-06-14 07:30:33', '2022-06-14 07:42:16'),
(8, 'Thêm 3 thành viên ĐH Quốc gia HN công bố phương án tuyển sinh', 'them-3-thanh-vien-dh-quoc-gia-hn-cong-bo-phuong-an-tuyen-sinh', 'them-3-thanh-vien-dh-quoc-gia-hn-cong-bo-phuong-an-tuyen-sinh.jpg', '<p>Đại học Kinh tế, Đại học Việt Nhật v&agrave; Khoa C&aacute;c khoa học li&ecirc;n ng&agrave;nh sử dụng nhiều phương thức x&eacute;t tuyển giống nhau v&agrave; mở mới một số ng&agrave;nh</p>', '<p>Trường&nbsp;<strong>Đại học Kinh tế</strong>&nbsp;tuyển 1.800 chỉ ti&ecirc;u hệ đại học ch&iacute;nh quy v&agrave; 700 hệ li&ecirc;n kết quốc tế.</p>\r\n\r\n<p>Trường sử dụng c&aacute;c phương thức: x&eacute;t kết quả thi tốt nghiệp THPT; kết quả thi đ&aacute;nh gi&aacute; năng lực học sinh THPT năm 2022 (HSA); x&eacute;t tuyển thẳng v&agrave; ưu ti&ecirc;n x&eacute;t tuyển; dựa v&agrave;o chứng chỉ quốc tế; x&eacute;t tuyển dự bị đại học, c&aacute;c huyện ngh&egrave;o, d&acirc;n tộc &iacute;t người; x&eacute;t tuyển sinh vi&ecirc;n quốc tế; x&eacute;t tuyển ng&agrave;nh Quản trị kinh doanh d&agrave;nh cho c&aacute;c t&agrave;i năng thể thao.</p>\r\n\r\n<p>Trường&nbsp;<strong>Đại học Việt Nhật</strong>&nbsp;năm nay tuyển 260 chỉ ti&ecirc;u cho bốn ng&agrave;nh gồm tr&igrave;nh độ cử nh&acirc;n ng&agrave;nh Nhật Bản học, Khoa học v&agrave; kỹ thuật m&aacute;y t&iacute;nh, v&agrave; tr&igrave;nh độ kỹ sư ng&agrave;nh Kỹ thuật x&acirc;y dựng, N&ocirc;ng nghiệp th&ocirc;ng minh v&agrave; bền vững. Trong đ&oacute;, hai ng&agrave;nh đ&agrave;o tạo tr&igrave;nh độ kỹ sư mới tuyển sinh năm nay.</p>\r\n\r\n<p>Trường sử dụng c&aacute;c phương thức: x&eacute;t tuyển dựa tr&ecirc;n kết quả thi THPT (30%, x&eacute;t hồ sơ năng lực của th&iacute; sinh kết hợp phỏng vấn - 30%; dựa v&agrave;o kết quả kỳ thi đ&aacute;nh gi&aacute; năng lực của Đại học Quốc gia H&agrave; Nội - 20%; c&ugrave;ng một số phương thức kh&aacute;c (20%) gồm x&eacute;t tuyển thẳng v&agrave; ưu ti&ecirc;n x&eacute;t tuyển, x&eacute;t chứng chỉ ngoại ngữ quốc tế kết hợp điểm hai m&ocirc;n thi tốt nghiệp THPT, dựa v&agrave;o kết quả kỳ thi chuẩn h&oacute;a SAT, ACT hay chứng chỉ A-Level.</p>\r\n\r\n<p>Giống như Đại học Việt Nhật,&nbsp;<strong>Khoa C&aacute;c khoa học li&ecirc;n ng&agrave;nh</strong>&nbsp;năm nay cũng mở th&ecirc;m ng&agrave;nh mới. Khoa tuyển sinh th&ecirc;m hai ng&agrave;nh Quản l&yacute; giải tr&iacute; v&agrave; sự kiện, Quản trị đ&ocirc; thị th&ocirc;ng minh v&agrave; bền vững; b&ecirc;n cạnh hai ng&agrave;nh đ&atilde; tuyển từ năm trước l&agrave; Quản trị thương hiệu v&agrave; Quản trị t&agrave;i nguy&ecirc;n di sản.</p>\r\n\r\n<p>Về phương thức tuyển sinh, đơn vị n&agrave;y sử dụng c&aacute;c phương thức tương tự Đại học Việt Nhật, chỉ kh&ocirc;ng x&eacute;t hồ sơ năng lực của th&iacute; sinh kết hợp phỏng vấn. Hiện, trường chưa c&ocirc;ng bố chỉ ti&ecirc;u ph&acirc;n bố cũng như mức s&agrave;n x&eacute;t tuyển đối với từng phương thức.</p>', NULL, 1, 1, 5, '2022-06-14 07:30:39', '2022-06-14 07:42:06'),
(9, 'Cả lớp đỗ trường chuyên ở Hà Nội', 'ca-lop-do-truong-chuyen-o-ha-noi', 'ca-lop-do-truong-chuyen-o-ha-noi.jpg', '<p>29/29 học sinh lớp 9C1, THCS Archimedes c&ugrave;ng đỗ chuy&ecirc;n; trong đ&oacute; 19 em đỗ hai trường</p>', '<p>Thầy V&otilde; Quốc B&aacute; Cẩn, chủ nhiệm lớp 9C1, cho biết, lớp c&oacute; 29 học sinh. 21 em đỗ Chuy&ecirc;n Sư phạm; 27 em tr&uacute;ng tuyển Chuy&ecirc;n Khoa học Tự nhi&ecirc;n v&agrave; 19 học sinh đỗ cả hai trường n&agrave;y.</p>\r\n\r\n<p>Trong c&aacute;c học sinh tr&uacute;ng tuyển chuy&ecirc;n Sư phạm, B&ugrave;i Hồng Phương Linh l&agrave; thủ khoa chuy&ecirc;n To&aacute;n với 37/40 điểm v&agrave; Phạm Ngọc Kh&aacute;nh v&agrave;o top 9.</p>\r\n\r\n<p>Ở chuy&ecirc;n Khoa học Tự nhi&ecirc;n, Nguyễn Gia Khi&ecirc;m l&agrave; &aacute; khoa lớp chuy&ecirc;n To&aacute;n với 27/30 điểm. Nguyễn Trần Ki&ecirc;n v&agrave; Nguyễn Triết Khoa lần lượt giữ vị tr&iacute; top 3 v&agrave; top 9 chuy&ecirc;n To&aacute;n.</p>\r\n\r\n<p>Nguyễn H&agrave; Minh Hiếu l&agrave; người c&oacute; điểm số cao thứ hai lớp chuy&ecirc;n Tin của cả hai trường chuy&ecirc;n Khoa học Tự nhi&ecirc;n v&agrave; chuy&ecirc;n Sư phạm.</p>\r\n\r\n<p>Trưa 13/6, sau khi biết tin cả lớp đỗ chuy&ecirc;n, thầy Cẩn mới thở ph&agrave;o nhẹ nh&otilde;m. Điện thoại của thầy từ l&uacute;c đ&oacute; trở n&ecirc;n bận rộn với những lời ch&uacute;c mừng v&agrave; cảm ơn của phụ huynh v&agrave; học sinh. Những ng&agrave;y qua, thầy li&ecirc;n tục &ocirc;m m&aacute;y t&iacute;nh để canh kết quả thi cho học tr&ograve;.</p>\r\n\r\n<p>&quot;T&ocirc;i đ&atilde; rất lo lắng cho đến tận s&aacute;ng nay. Cuối c&ugrave;ng, những bạn khiến t&ocirc;i hồi hộp nhất cũng đ&atilde; vượt qua&quot;, thầy Cẩn chia sẻ.</p>\r\n\r\n<p>Với thầy Cẩn, năm nay đặc biệt v&igrave; học sinh c&oacute; thời gian d&agrave;i học online li&ecirc;n tiếp. So với c&aacute;c kh&oacute;a trước, thầy kh&ocirc;ng tự tin với kh&oacute;a n&agrave;y. Thầy gi&aacute;o dạy To&aacute;n cho biết, học trực tuyến khiến c&aacute;c em chểnh mảng, thiếu tập trung. Kh&ocirc;ng &iacute;t em gặp vấn đề t&acirc;m l&yacute; v&igrave; ở nh&agrave; nhiều.</p>', 1, 1, 1, 6, '2022-06-14 07:30:55', '2022-06-14 08:23:01'),
(10, 'Đại học Hoa Sen hợp tác cùng Nhạc viện Sydney', 'dai-hoc-hoa-sen-hop-tac-cung-nhac-vien-sydney', 'dai-hoc-hoa-sen-hop-tac-cung-nhac-vien-sydney.jpg', '<p>Đại học Hoa Sen (HSU) hợp t&aacute;c c&ugrave;ng Nhạc viện Sydney (Đại học Sydney) từ ng&agrave;y 13/6 nhằm mở ra cơ hội l&agrave;m việc l&acirc;u d&agrave;i trong lĩnh vực &acirc;m nhạc - gi&aacute;o dục.</p>', '<p>Sự kiện k&yacute; kết c&oacute; sự tham gia của đại diện l&atilde;nh đạo trường v&agrave; GS. Anna Reid - Gi&aacute;m đốc Nhạc viện Sydney. PGS. TS. V&otilde; Thị Ngọc Th&uacute;y, Hiệu trưởng Đại học Hoa Sen cho biết trường hiểu được tầm quan trọng của &acirc;m nhạc trong cuộc sống v&agrave; những lợi &iacute;ch to lớn của việc gi&aacute;o dục &acirc;m nhạc đối với th&agrave;nh c&ocirc;ng trong học tập của sinh vi&ecirc;n.</p>\r\n\r\n<p>&quot;Đại học Hoa Sen muốn hướng tới việc t&iacute;ch hợp gi&aacute;o dục &acirc;m nhạc v&agrave;o chương tr&igrave;nh đ&agrave;o tạo, đ&aacute;p ứng nhu cầu ph&aacute;t triển to&agrave;n diện cho thế hệ trẻ giỏi về chuy&ecirc;n m&ocirc;n v&agrave; c&oacute; khả năng cảm thụ &acirc;m nhạc&quot;, b&agrave; n&oacute;i th&ecirc;m.</p>\r\n\r\n<p>Trước đ&oacute;, ng&agrave;y 23/5, Đại học Hoa Sen v&agrave; Hội đồng Khảo th&iacute; &Acirc;m nhạc Quốc gia Australia (AMEB) đ&atilde; k&yacute; Bi&ecirc;n bản Ghi nhớ hợp t&aacute;c. Đ&acirc;y l&agrave; tiền đề để hướng đến li&ecirc;n kết giữa Đại học Sydney v&agrave; Đại học Hoa Sen.</p>\r\n\r\n<p>Theo đ&oacute;, Đại học Hoa Sen sẽ phối hợp với AMEB v&agrave; Nhạc viện Sydney giảng dạy, đ&agrave;o tạo &acirc;m nhạc cho sinh vi&ecirc;n, giảng vi&ecirc;n, c&aacute;n bộ nh&acirc;n vi&ecirc;n. HSU sẽ gi&uacute;p người học tiếp cận nền gi&aacute;o dục &acirc;m nhạc thế giới. Trường cũng dự kiến phối hợp với hai đơn vị n&agrave;y x&acirc;y dựng c&aacute;c kh&oacute;a học &acirc;m nhạc, cấp chứng chỉ cho người học, g&oacute;p phần ươm mầm t&agrave;i năng &acirc;m nhạc trẻ của Việt Nam.</p>\r\n\r\n<p>Với định hướng trở th&agrave;nh trường đại học đẳng cấp quốc tế, Đại học Hoa Sen đ&agrave;o tạo sinh vi&ecirc;n theo phương ch&acirc;m &quot;thực học, thực l&agrave;m&quot;, đồng thời, tạo m&ocirc;i trường để c&aacute;c bạn thẩm thấu văn h&oacute;a - nghệ thuật, ph&aacute;t triển to&agrave;n diện v&agrave; n&acirc;ng cao tr&aacute;ch nhiệm với x&atilde; hội.</p>\r\n\r\n<p>Do đ&oacute;, sự kiện n&agrave;y sẽ đ&aacute;nh dấu cột mốc quan trọng g&oacute;p phần đưa Đại học Hoa Sen thực hiện mục ti&ecirc;u trở th&agrave;nh trường đại học đẳng cấp quốc tế, c&oacute; thể đ&agrave;o tạo &acirc;m nhạc cho sinh vi&ecirc;n v&agrave; giảng vi&ecirc;n.</p>\r\n\r\n<p>Trước hợp t&aacute;c n&agrave;y, trường đ&atilde; đ&agrave;o tạo n&ecirc;n nhiều t&agrave;i năng v&agrave; l&agrave; lựa chọn của nhiều ng&ocirc;i sao &acirc;m nhạc trẻ như ca sĩ Mon Ho&agrave;ng Anh v&agrave; Hana Diệu H&acirc;n của nh&oacute;m P336 hay Hồ Văn Cường.</p>', 2, 1, 1, 6, '2022-06-14 07:31:02', '2022-06-14 08:34:14'),
(11, 'Thủ khoa lớp 10 chuyên Sư phạm đạt gần 9,5 điểm mỗi môn', 'thu-khoa-lop-10-chuyen-su-pham-dat-gan-95-diem-moi-mon', 'thu-khoa-lop-10-chuyen-su-pham-dat-gan-95-diem-moi-mon.jpg', '<p>Với tổng điểm 37,5, Nguyễn Thị Thu Minh trở th&agrave;nh thủ khoa đầu v&agrave;o lớp 10 trường THPT chuy&ecirc;n Đại học Sư phạm, thừa 13,75 so với điểm chuẩn.</p>', '<p>Tối 12/6, trường THPT chuy&ecirc;n Đại học Sư phạm H&agrave; Nội (Đại học Sư phạm H&agrave; Nội) c&ocirc;ng bố&nbsp;<a href=\"https://vnexpress.net/diem-chuan-vao-lop-10-thpt-chuyen-su-pham-4475249.html\" rel=\"dofollow\">điểm chuẩn 7 lớp chuy&ecirc;n</a>. Điểm x&eacute;t tuyển l&agrave; tổng điểm To&aacute;n v&agrave; Ngữ văn chung c&ugrave;ng m&ocirc;n chuy&ecirc;n nh&acirc;n hệ số hai, kh&ocirc;ng &aacute;p dụng điểm cộng hay ưu ti&ecirc;n. Theo c&aacute;ch t&iacute;nh n&agrave;y, điểm tối đa l&agrave; 40.</p>\r\n\r\n<p>Thu Minh (H&agrave; Nội), dự thi v&agrave;o lớp chuy&ecirc;n Vật l&yacute;, đạt 10 điểm To&aacute;n chung v&agrave; Vật l&yacute; chuy&ecirc;n; 7,5 Văn. Với tổng điểm 37,5, nữ sinh thừa 13,75 so với điểm chuẩn của lớp L&yacute;. T&iacute;nh trung b&igrave;nh, Minh đạt 9,375 điểm mỗi m&ocirc;n v&agrave; trở th&agrave;nh thủ khoa đầu v&agrave;o năm nay.</p>\r\n\r\n<p>Mức điểm của Thu Minh tương đương với thủ khoa năm ngo&aacute;i, chỉ k&eacute;m 0,5 điểm Văn. Thủ khoa chuy&ecirc;n Sư phạm 2021 đạt 10 To&aacute;n chung v&agrave; To&aacute;n chuy&ecirc;n, 8 Văn.</p>\r\n\r\n<p>Th&iacute; sinh điểm cao thứ hai l&agrave; B&ugrave;i Hồng Phương Linh (H&agrave; Nội) - 37 điểm. Nữ sinh đăng k&yacute; lớp chuy&ecirc;n To&aacute;n, lần lượt đạt 9,75 To&aacute;n chung v&agrave; To&aacute;n chuy&ecirc;n v&agrave; 7,75 Văn. Trung b&igrave;nh, mỗi m&ocirc;n của Phương Linh l&agrave; 9,25 điểm.</p>\r\n\r\n<p>C&aacute;c th&iacute; sinh c&ograve;n lại của top 5 đạt 35,5-36 điểm, đều tr&uacute;ng tuyển lớp chuy&ecirc;n To&aacute;n. Trong đ&oacute;, một học sinh đến từ Th&aacute;i B&igrave;nh.</p>\r\n\r\n<p>Trường THPT chuy&ecirc;n Sư phạm tuyển sinh to&agrave;n quốc, chỉ ti&ecirc;u 305 cho bảy lớp chuy&ecirc;n. Trừ To&aacute;n tuyển 70 học sinh, Tiếng Anh 60, c&aacute;c lớp Tin học, Vật l&yacute;, H&oacute;a học, Sinh học, Ngữ văn, mỗi lớp 35 em.</p>\r\n\r\n<p>Tổng số th&iacute; sinh đăng k&yacute; dự thi l&agrave; 5.477, đ&ocirc;ng nhất trong c&aacute;c trường THPT chuy&ecirc;n thuộc đại học. Việc n&agrave;y khiến trường c&oacute; tỷ lệ chọi v&agrave;o c&aacute;c lớp chuy&ecirc;n lu&ocirc;n ở mức cạnh tranh khốc liệt. Cụ thể, tiếng Anh 1/32, H&oacute;a 1/20, Văn 1/18, c&aacute;c lớp c&ograve;n lại đều kh&ocirc;ng dưới một chọi 10.</p>\r\n\r\n<p>Th&iacute; sinh phải l&agrave;m b&agrave;i thi viết ba m&ocirc;n trong ng&agrave;y 1/6, gồm To&aacute;n, Văn v&agrave;o buổi s&aacute;ng, m&ocirc;n chuy&ecirc;n buổi chiều. Hai m&ocirc;n điều kiện c&oacute; thời gian l&agrave;m b&agrave;i 90 ph&uacute;t, ri&ecirc;ng m&ocirc;n chuy&ecirc;n 120 ph&uacute;t. Th&iacute; sinh thi chuy&ecirc;n Tin sẽ l&agrave;m b&agrave;i thi chuy&ecirc;n To&aacute;n.</p>\r\n\r\n<p><a href=\"https://vnexpress.net/de-thi-6-mon-chuyen-vao-lop-10-truong-su-pham-ha-noi-4470780.html\" rel=\"dofollow\"><strong>Đề thi 6 m&ocirc;n chuy&ecirc;n v&agrave;o THPT chuy&ecirc;n Sư phạm 2022</strong></a></p>\r\n\r\n<p><strong>Về điểm chuẩn năm 2022,</strong>&nbsp;To&aacute;n vươn l&ecirc;n đứng đầu với điểm chuẩn 27,5, cao hơn năm ngo&aacute;i gần 6 điểm. Đ&acirc;y cũng l&agrave; lớp duy nhất c&oacute; điểm chuẩn học bổng tr&ecirc;n 30. Để tr&uacute;ng tuyển v&agrave;o lớp chuy&ecirc;n To&aacute;n, th&iacute; sinh phải đạt trung b&igrave;nh 7 điểm mỗi m&ocirc;n. C&ograve;n để gi&agrave;nh học bổng, điểm trung b&igrave;nh tối thiểu l&ecirc;n tới 8,25.</p>\r\n\r\n<p>Bốn lớp Sinh, H&oacute;a, Văn v&agrave; Tiếng Anh lấy điểm chuẩn tương đương nhau, dao động 25-25,25. Lớp Tin v&agrave; L&yacute; lần lượt 23,25 v&agrave; 23,75 điểm. So với năm ngo&aacute;i, một số lớp c&oacute; điểm chuẩn giảm nhẹ, nhưng mức cao nhất v&agrave; thấp nhất đều tăng.</p>', 10, 0, 1, 6, '2022-06-14 07:31:08', '2022-06-15 06:36:28'),
(12, 'Bộ GD&ĐT yêu cầu \'không vận động phụ huynh mua sách tham khảo\'', 'bo-gddt-yeu-cau-khong-van-dong-phu-huynh-mua-sach-tham-khao', 'bo-gddt-yeu-cau-khong-van-dong-phu-huynh-mua-sach-tham-khao.jpg', '<p>Bộ Gi&aacute;o dục v&agrave; Đ&agrave;o tạo y&ecirc;u cầu c&aacute;c trường kh&ocirc;ng vận động học sinh, phụ huynh mua xuất bản phẩm ngo&agrave;i danh mục s&aacute;ch gi&aacute;o khoa dưới bất kỳ h&igrave;nh thức n&agrave;o.</p>', '<p>Bộ Gi&aacute;o dục v&agrave; Đ&agrave;o tạo vừa ra chỉ thị, k&yacute; ng&agrave;y 10/6, y&ecirc;u cầu trường học kh&ocirc;ng lập danh mục, đ&oacute;ng g&oacute;i th&agrave;nh bộ s&aacute;ch gi&aacute;o khoa k&egrave;m s&aacute;ch b&agrave;i tập, s&aacute;ch tham khảo v&agrave; c&aacute;c t&agrave;i liệu kh&aacute;c để học sinh, phụ huynh mua v&agrave; sử dụng. Bộ đồng thời qu&aacute;n triệt kh&ocirc;ng vận động, tư vấn để phụ huynh mua những xuất bản phẩm ngo&agrave;i s&aacute;ch gi&aacute;o khoa trong danh mục đ&atilde; được ph&ecirc; duyệt.</p>\r\n\r\n<p>Đ&acirc;y l&agrave; quy định đ&atilde; được đề cập trong Th&ocirc;ng tư số 21 năm 2014 của Bộ Gi&aacute;o dục v&agrave; Đ&agrave;o tạo. Tuy nhi&ecirc;n, tại Kỳ họp thứ 3, Quốc hội kh&oacute;a XV diễn ra v&agrave;o đầu th&aacute;ng 6, c&aacute;c đại biểu quốc hội cho rằng những năm qua quy định n&agrave;y chưa được thực hiện triệt để v&igrave; nhiều l&yacute; do.</p>\r\n\r\n<p><a href=\"https://vnexpress.net/dai-bieu-dong-tinh-dua-sach-giao-khoa-vao-danh-muc-quan-ly-gia-4471098.html?fbclid=IwAR1ueVEY1Y1CUc4RKYzxs94g8rd0G1_iTO_x-IK9dkW8eNbBWzZQqd68QJA\" rel=\"dofollow\">Đại biểu</a>&nbsp;đề nghị Bộ Gi&aacute;o dục v&agrave; Đ&agrave;o tạo chỉ đạo c&aacute;c trường tăng cường truyền th&ocirc;ng để người d&acirc;n hiểu s&aacute;ch gi&aacute;o khoa c&oacute; hai loại, gồm s&aacute;ch bắt buộc v&agrave; s&aacute;ch bổ trợ, tham khảo. Khi đ&oacute;, t&ugrave;y điều kiện v&agrave; nhu cầu của mỗi học sinh, phụ huynh c&oacute; thể quyết định mua hoặc kh&ocirc;ng mua s&aacute;ch tham khảo.</p>\r\n\r\n<p>&quot;N&ecirc;n thống k&ecirc; danh mục s&aacute;ch gi&aacute;o khoa bắt buộc v&agrave; s&aacute;ch tham khảo, v&igrave; hiện nay số lượng đầu s&aacute;ch cho học sinh, kể cả cấp tiểu học đều qu&aacute; nhiều. Trong số n&agrave;y, c&oacute; những cuốn chỉ l&agrave; tham khảo, nhưng v&igrave; kh&ocirc;ng c&oacute; hướng dẫn n&ecirc;n phụ huynh kh&ocirc;ng hiểu r&otilde; phải lựa chọn đầu s&aacute;ch n&agrave;o&quot;, đại biểu Nguyễn Thị Việt Nga, Ph&oacute; đo&agrave;n Hải Dương, n&oacute;i trong buổi thảo luận về kinh tế - x&atilde; hội tại Quốc hội s&aacute;ng 2/6.</p>\r\n\r\n<p>Đại biểu Nguyễn L&acirc;n Hiếu (Gi&aacute;m đốc Bệnh viện Đại học Y H&agrave; Nội) cũng đề nghị cấm b&aacute;n s&aacute;ch tham khảo trong nh&agrave; trường.</p>\r\n\r\n<p>Cũng tại Chỉ chị n&agrave;y, Bộ Gi&aacute;o dục v&agrave; Đ&agrave;o tạo y&ecirc;u cầu c&aacute;c địa phương tuy&ecirc;n truyền tới gi&aacute;o vi&ecirc;n, học sinh về việc giữ g&igrave;n, bảo quản s&aacute;ch gi&aacute;o khoa, kh&ocirc;ng viết, vẽ v&agrave;o s&aacute;ch để sử dụng l&acirc;u bền.</p>\r\n\r\n<p>Ngo&agrave;i ra, c&aacute;c trường cần bố tr&iacute; kinh ph&iacute; hợp l&yacute; để mua s&aacute;ch cho thư viện, gi&uacute;p học sinh mượn, đồng thời vận động học sinh quy&ecirc;n g&oacute;p, ủng hộ s&aacute;ch gi&aacute;o khoa cũ cho c&aacute;c kh&oacute;a sau sử dụng.</p>\r\n\r\n<p>Ngo&agrave;i quy định với trường v&agrave; địa phương, Bộ cũng y&ecirc;u cầu c&aacute;c nh&agrave; xuất bản phải r&agrave; so&aacute;t, đ&aacute;nh gi&aacute; việc in ấn, ph&aacute;t h&agrave;nh nhằm giảm gi&aacute; v&agrave; chuẩn bị đủ số lượng s&aacute;ch gi&aacute;o khoa cần thiết. Trước khi ph&aacute;t h&agrave;nh hoặc t&aacute;i bản s&aacute;ch, nh&agrave; xuất bản phải b&aacute;o c&aacute;o Bộ về kết quả r&agrave; so&aacute;t v&agrave; phương &aacute;n giảm gi&aacute; s&aacute;ch.</p>', NULL, 1, 1, 6, '2022-06-14 07:31:14', '2022-06-14 07:59:51'),
(13, 'Pixta Vietnam hợp tác đào tạo với đơn vị giáo dục Việt', 'pixta-vietnam-hop-tac-dao-tao-voi-don-vi-giao-duc-viet', 'pixta-vietnam-hop-tac-dao-tao-voi-don-vi-giao-duc-viet.jpg', '<p>Pixta Vietnam hợp tác đào tạo và tuy&ecirc;̉n dụng cùng FUNiX nhằm t&igrave;m kiếm v&agrave; x&acirc;y dựng nguồn nh&acirc;n sự chất lượng cao</p>', '<p>Pixta l&agrave; tập đo&agrave;n Nhật Bản hoạt động trong lĩnh vực cung cấp ảnh, video, audio... c&oacute; bản quyền. Với chức năng ph&aacute;t triển sản phẩm cho tập đo&agrave;n, Pixta Vietnam - chi nh&aacute;nh tại Việt Nam đang cần tuyển nhiều nh&acirc;n sự c&ocirc;ng nghệ th&ocirc;ng tin như Data Scientist engineer, Full Stack developer, DevOps Engineer, Scrum Master... với dải lương từ 500 l&ecirc;n đến 4.500 USD.</p>\r\n\r\n<p>Với mục ti&ecirc;u t&igrave;m kiếm nguồn nh&acirc;n sự chất lượng, Pixta Vietnam hợp t&aacute;c c&ugrave;ng đơn vị đ&agrave;o tạo trực tuyến FUNiX từ th&aacute;ng 4. Theo đ&oacute;, doanh nghiệp cam kết cử chuy&ecirc;n gia tham gia x&acirc;y dựng chương tr&igrave;nh đ&agrave;o tạo, l&agrave;m mentor (cố vấn) tại FUNiX, đồng thời, ch&agrave;o đ&oacute;n sinh vi&ecirc;n đến thực tập v&agrave; l&agrave;m việc.</p>\r\n\r\n<p>CEO Pixta Vietnam Ryo Kobari cho biết, hợp t&aacute;c được th&uacute;c đẩy bởi trải nghiệm nh&acirc;n lực thực tế tại đơn vị. Một số th&agrave;nh vi&ecirc;n của c&ocirc;ng ty c&oacute; xuất ph&aacute;t điểm tr&aacute;i ng&agrave;nh hoặc kh&ocirc;ng tốt nghiệp đại học nhưng vẫn c&oacute; sự ph&aacute;t triển rất tốt khi được định hướng ch&iacute;nh x&aacute;c. Trong qu&aacute; tr&igrave;nh tiếp x&uacute;c v&agrave; l&agrave;m việc c&ugrave;ng c&aacute;c th&agrave;nh vi&ecirc;n n&agrave;y, &ocirc;ng nhận thấy trước đ&acirc;y c&aacute;c bạn gặp rất nhiều kh&oacute; khăn trong việc x&aacute;c định m&igrave;nh cần học g&igrave;, học ở đ&acirc;u.</p>\r\n\r\n<p>&quot;Do đ&oacute;, t&ocirc;i đ&aacute;nh gi&aacute; cao m&ocirc; h&igrave;nh đ&agrave;o tạo của FUNiX bởi c&aacute;ch học linh hoạt, lộ tr&igrave;nh b&agrave;i bản, tạo điều kiện cho c&aacute;c bạn từ nhiều ng&agrave;nh kh&aacute;c nhau chuyển nghề sang IT dễ d&agrave;ng hơn&quot;, CEO Pixta Vietnam n&oacute;i th&ecirc;m.</p>\r\n\r\n<p>Theo đại diện Pixta Vietnam, b&ecirc;n cạnh yếu tố kinh nghiệm, doanh nghiệp muốn t&igrave;m kiếm những ứng vi&ecirc;n c&oacute; niềm đam m&ecirc; với c&ocirc;ng nghệ v&agrave; tinh thần tự học, t&igrave;m t&ograve;i nghi&ecirc;n cứu những giải ph&aacute;p tốt nhất để tối ưu h&oacute;a trải nghiệm của người d&ugrave;ng.</p>\r\n\r\n<p>Tại lễ k&yacute; kết, nh&agrave; s&aacute;ng lập FUNiX - &ocirc;ng Nguyễn Th&agrave;nh Nam cũng khẳng định m&ocirc; h&igrave;nh&nbsp;<a href=\"https://funix.edu.vn/tuyen-sinh/\" rel=\"nofollow\">đ&agrave;o tạo c&ocirc;ng nghệ th&ocirc;ng tin online</a>&nbsp;của đơn vị đảm bảo r&egrave;n luyện cho sinh vi&ecirc;n khả năng tự học, t&igrave;m c&aacute;ch giải quyết vấn đề khi gặp kh&oacute; khăn.</p>\r\n\r\n<p>&quot;Đ&acirc;y l&agrave; những phẩm chất quan trọng trong bất cứ ng&agrave;nh nghề n&agrave;o, đặc biệt l&agrave; IT, khi c&ocirc;ng nghệ thay đổi từng ng&agrave;y v&agrave; kh&ocirc;ng c&oacute; ai c&oacute; thể dạy người lao động tất cả những thứ sẽ cần đến trong qu&aacute; tr&igrave;nh l&agrave;m việc&quot;, &ocirc;ng chia sẻ.</p>', NULL, 1, 1, 7, '2022-06-14 07:31:28', '2022-06-14 08:01:21'),
(14, 'Thực tế ảo góp phần định hình tương lai giáo dục', 'thuc-te-ao-gop-phan-dinh-hinh-tuong-lai-giao-duc', 'thuc-te-ao-gop-phan-dinh-hinh-tuong-lai-giao-duc.jpg', '<p>Thực tế ảo (VR) tăng cường t&iacute;nh trải nghiệm trong học tập, n&acirc;ng cao khả năng tiếp thu v&agrave; định h&igrave;nh tương lai gi&aacute;o dục to&agrave;n cầu</p>', '<p>Nội dung được chia sẻ trong khu&ocirc;n khổ Hội nghị thường ni&ecirc;n Davos 2022 của Diễn đ&agrave;n Kinh tế Thế giới diễn ra tại Thụy Sĩ. C&aacute;c diễn giả cho rằng, sự ph&aacute;t triển của c&ocirc;ng nghệ đang tạo ra những thay đổi kh&aacute;c biệt trong gi&aacute;o dục, sẽ đến nhanh hơn trong thập kỷ tới.</p>\r\n\r\n<p>Những th&agrave;nh tựu c&ocirc;ng nghệ gi&aacute;o dục (edtech) đ&atilde; được chứng minh trong đại dịch Covid-19. Để đảm bảo việc học diễn ra li&ecirc;n tục, UNESCO đ&atilde; hỗ trợ c&aacute;c quốc gia tr&ecirc;n thế giới c&aacute;c c&ocirc;ng cụ, chương tr&igrave;nh v&agrave; nguồn lực kỹ thuật số đa dạng trong giai đoạn n&agrave;y</p>\r\n\r\n<p>Tuy vậy, c&aacute;c giải ph&aacute;p đ&atilde; c&oacute; hầu như chỉ tập trung v&agrave;o việc truyền tải kiến thức chứ kh&ocirc;ng hỗ trợ để người học c&oacute; được c&aacute;c trải nghiệm thực tế v&agrave; trực tiếp - những yếu tố rất cần thiết để gi&uacute;p người học nắm bắt được c&aacute;c kh&aacute;i niệm. &quot;Ch&uacute;ng ta cần những tiến bộ c&ocirc;ng nghệ đột ph&aacute; hơn nữa để ph&aacute; vỡ cấu tr&uacute;c cũ của nền gi&aacute;o dục. Việc ứng dụng những tiến bộ n&agrave;y v&agrave;o hệ thống gi&aacute;o dục ng&agrave;y c&agrave;ng trở n&ecirc;n cấp thiết.&quot;, c&aacute;c chuy&ecirc;n gia nhận định.</p>\r\n\r\n<p>Nội dung được chia sẻ trong khu&ocirc;n khổ Hội nghị thường ni&ecirc;n Davos 2022 của Diễn đ&agrave;n Kinh tế Thế giới diễn ra tại Thụy Sĩ. C&aacute;c diễn giả cho rằng, sự ph&aacute;t triển của c&ocirc;ng nghệ đang tạo ra những thay đổi kh&aacute;c biệt trong gi&aacute;o dục, sẽ đến nhanh hơn trong thập kỷ tới.</p>\r\n\r\n<p>Những th&agrave;nh tựu c&ocirc;ng nghệ gi&aacute;o dục (edtech) đ&atilde; được chứng minh trong đại dịch Covid-19. Để đảm bảo việc học diễn ra li&ecirc;n tục, UNESCO đ&atilde; hỗ trợ c&aacute;c quốc gia tr&ecirc;n thế giới c&aacute;c c&ocirc;ng cụ, chương tr&igrave;nh v&agrave; nguồn lực kỹ thuật số đa dạng trong giai đoạn n&agrave;y</p>\r\n\r\n<p>Tuy vậy, c&aacute;c giải ph&aacute;p đ&atilde; c&oacute; hầu như chỉ tập trung v&agrave;o việc truyền tải kiến thức chứ kh&ocirc;ng hỗ trợ để người học c&oacute; được c&aacute;c trải nghiệm thực tế v&agrave; trực tiếp - những yếu tố rất cần thiết để gi&uacute;p người học nắm bắt được c&aacute;c kh&aacute;i niệm. &quot;Ch&uacute;ng ta cần những tiến bộ c&ocirc;ng nghệ đột ph&aacute; hơn nữa để ph&aacute; vỡ cấu tr&uacute;c cũ của nền gi&aacute;o dục. Việc ứng dụng những tiến bộ n&agrave;y v&agrave;o hệ thống gi&aacute;o dục ng&agrave;y c&agrave;ng trở n&ecirc;n cấp thiết.&quot;, c&aacute;c chuy&ecirc;n gia nhận định.</p>', 2, 1, 1, 7, '2022-06-14 07:31:39', '2022-06-14 08:53:03'),
(15, 'Nhiều bạn trẻ chinh phục đam mê AI nhờ học bổng Cohost.AI', 'nhieu-ban-tre-chinh-phuc-dam-me-ai-nho-hoc-bong-cohostai', 'nhieu-ban-tre-chinh-phuc-dam-me-ai-nho-hoc-bong-cohostai.jpg', '<p>Nhiều bạn trẻ học tr&aacute;i ng&agrave;nh t&iacute;ch lũy nền tảng kiến thức về tr&iacute; tuệ nh&acirc;n tạo, c&oacute; c&ocirc;ng việc ưng &yacute; nhờ học bổng FUNiX AI Connect with Cohost.AI trị gi&aacute; 200 triệu đồng</p>', '<p>FUNiX v&agrave;&nbsp;<a href=\"https://www.cohost.vn/\" rel=\"nofollow\">Cohost.AI</a>&nbsp;k&yacute; kết hợp t&aacute;c đ&agrave;o tạo - tuyển dụng v&agrave;o cuối th&aacute;ng 1/2021 v&agrave; bắt đầu triển khai chương tr&igrave;nh học bổng t&agrave;i trợ 100% học ph&iacute;. Học bổng nhằm kh&iacute;ch lệ người trẻ, đồng thời, chia sẻ nguồn lực với c&aacute;c c&ocirc;ng ty, doanh nghiệp đang &quot;kh&aacute;t&quot; người t&agrave;i. Ứng vi&ecirc;n nhận học bổng được tham gia kh&oacute;a học trực tuyến Data Science hoặc Machine Learning tại FUNiX.</p>\r\n\r\n<p>10 học bổng từ Cohost.AI đ&atilde; được lựa chọn trao cho c&aacute;c học vi&ecirc;n ph&ugrave; hợp theo từng th&aacute;ng trong suốt một năm qua. Trong số 10 học vi&ecirc;n theo học, c&oacute; một học vi&ecirc;n đ&atilde; tốt nghiệp trước thời hạn v&agrave; đ&atilde; đi l&agrave;m trong lĩnh vực Data Science. C&aacute;c sinh vi&ecirc;n đều sẽ được hai đơn vị hỗ trợ t&igrave;m được c&ocirc;ng việc đ&uacute;ng chuy&ecirc;n m&ocirc;n với mức lương hấp dẫn tại c&aacute;c doanh nghiệp đối t&aacute;c của FUNiX.</p>\r\n\r\n<p>Nguyễn Phạm Th&ugrave;y Trinh (sinh năm 2000, Đồng Nai) l&agrave; một trong những ứng vi&ecirc;n đầu ti&ecirc;n nhận được học bổng Cohost.AI từ th&aacute;ng 11/2021. Hiện, c&ocirc; học năm cuối đại học v&agrave; tiếp tục ho&agrave;n th&agrave;nh kh&oacute;a Data Science, c&ocirc;ng nghệ thuộc lĩnh vực AI, bởi kiến thức trong lĩnh vực c&oacute; ứng dụng rộng r&atilde;i, cả trong ng&agrave;nh học ch&iacute;nh của m&igrave;nh - Kiểm To&aacute;n.</p>\r\n\r\n<p>Nữ sinh chia sẻ bản th&acirc;n đăng k&yacute; học bổng ngay khi biết th&ocirc;ng tin bởi được t&agrave;i trợ 100% học ph&iacute; cho&nbsp;<a href=\"https://funix.edu.vn/khoa-hoc-data-science-190822/\" rel=\"nofollow\" target=\"_blank\">kh&oacute;a học Data Science</a>&nbsp;tại FUNiX. Th&ugrave;y Trinh vượt qua nhiều đối thủ để c&oacute; được suất học bổng đ&uacute;ng v&agrave;o l&uacute;c c&ocirc; cần đến nhất. Học tập trực tuyến, c&ocirc; được hướng dẫn bởi mạng lưới mentor (chuy&ecirc;n gia c&ocirc;ng nghệ) chất lượng, c&aacute;ch học linh hoạt v&agrave; tiếp cận nhiều cơ hội nghề nghiệp hấp dẫn sau khi học xong.</p>\r\n\r\n<p>&quot;D&ugrave; bận rộn thực tập kiểm to&aacute;n, chưa c&oacute; nhiều kiến thức nền tảng, m&igrave;nh vẫn quyết t&acirc;m chinh phục kh&oacute;a học để c&oacute; thể tự tin hơn sau khi tốt nghiệp đại học, bước v&agrave;o kỷ nguy&ecirc;n số - thời đại b&ugrave;ng nổ của &lsquo;dữ liệu lớn&rsquo; v&agrave; AI n&oacute;i chung&quot;</p>\r\n\r\n<p>FUNiX v&agrave;&nbsp;<a href=\"https://www.cohost.vn/\" rel=\"nofollow\">Cohost.AI</a>&nbsp;k&yacute; kết hợp t&aacute;c đ&agrave;o tạo - tuyển dụng v&agrave;o cuối th&aacute;ng 1/2021 v&agrave; bắt đầu triển khai chương tr&igrave;nh học bổng t&agrave;i trợ 100% học ph&iacute;. Học bổng nhằm kh&iacute;ch lệ người trẻ, đồng thời, chia sẻ nguồn lực với c&aacute;c c&ocirc;ng ty, doanh nghiệp đang &quot;kh&aacute;t&quot; người t&agrave;i. Ứng vi&ecirc;n nhận học bổng được tham gia kh&oacute;a học trực tuyến Data Science hoặc Machine Learning tại FUNiX.</p>\r\n\r\n<p>10 học bổng từ Cohost.AI đ&atilde; được lựa chọn trao cho c&aacute;c học vi&ecirc;n ph&ugrave; hợp theo từng th&aacute;ng trong suốt một năm qua. Trong số 10 học vi&ecirc;n theo học, c&oacute; một học vi&ecirc;n đ&atilde; tốt nghiệp trước thời hạn v&agrave; đ&atilde; đi l&agrave;m trong lĩnh vực Data Science. C&aacute;c sinh vi&ecirc;n đều sẽ được hai đơn vị hỗ trợ t&igrave;m được c&ocirc;ng việc đ&uacute;ng chuy&ecirc;n m&ocirc;n với mức lương hấp dẫn tại c&aacute;c doanh nghiệp đối t&aacute;c của FUNiX.</p>\r\n\r\n<p>Nguyễn Phạm Th&ugrave;y Trinh (sinh năm 2000, Đồng Nai) l&agrave; một trong những ứng vi&ecirc;n đầu ti&ecirc;n nhận được học bổng Cohost.AI từ th&aacute;ng 11/2021. Hiện, c&ocirc; học năm cuối đại học v&agrave; tiếp tục ho&agrave;n th&agrave;nh kh&oacute;a Data Science, c&ocirc;ng nghệ thuộc lĩnh vực AI, bởi kiến thức trong lĩnh vực c&oacute; ứng dụng rộng r&atilde;i, cả trong ng&agrave;nh học ch&iacute;nh của m&igrave;nh - Kiểm To&aacute;n.</p>\r\n\r\n<p>Nữ sinh chia sẻ bản th&acirc;n đăng k&yacute; học bổng ngay khi biết th&ocirc;ng tin bởi được t&agrave;i trợ 100% học ph&iacute; cho&nbsp;<a href=\"https://funix.edu.vn/khoa-hoc-data-science-190822/\" rel=\"nofollow\" target=\"_blank\">kh&oacute;a học Data Science</a>&nbsp;tại FUNiX. Th&ugrave;y Trinh vượt qua nhiều đối thủ để c&oacute; được suất học bổng đ&uacute;ng v&agrave;o l&uacute;c c&ocirc; cần đến nhất. Học tập trực tuyến, c&ocirc; được hướng dẫn bởi mạng lưới mentor (chuy&ecirc;n gia c&ocirc;ng nghệ) chất lượng, c&aacute;ch học linh hoạt v&agrave; tiếp cận nhiều cơ hội nghề nghiệp hấp dẫn sau khi học xong.</p>\r\n\r\n<p>&quot;D&ugrave; bận rộn thực tập kiểm to&aacute;n, chưa c&oacute; nhiều kiến thức nền tảng, m&igrave;nh vẫn quyết t&acirc;m chinh phục kh&oacute;a học để c&oacute; thể tự tin hơn sau khi tốt nghiệp đại học, bước v&agrave;o kỷ nguy&ecirc;n số - thời đại b&ugrave;ng nổ của &lsquo;dữ liệu lớn&rsquo; v&agrave; AI n&oacute;i chung&quot;</p>', NULL, 1, 1, 7, '2022-06-14 07:31:47', '2022-06-14 08:07:39'),
(16, 'Tiki hợp tác cùng FUNiX phát triển nguồn nhân lực IT', 'tiki-hop-tac-cung-funix-phat-trien-nguon-nhan-luc-it', 'tiki-hop-tac-cung-funix-phat-trien-nguon-nhan-luc-it.jpg', '<p>Nền tảng thương mại điện tử Tiki k&yacute; hợp t&aacute;c đ&agrave;o tạo, tuyển dụng c&ugrave;ng FUNiX nhằm ph&aacute;t triển nguồn nh&acirc;n lực c&ocirc;ng nghệ th&ocirc;ng tin chất lượng cao.</p>', '<p>Theo đ&oacute;, Tiki dự kiến sẽ cử c&aacute;c chuy&ecirc;n gia c&ocirc;ng nghệ tham gia l&agrave;m mentor (cố vấn), đ&oacute;ng g&oacute;p v&agrave;o qu&aacute; tr&igrave;nh x&acirc;y dựng, cải tiến nội dung chương tr&igrave;nh đ&agrave;o tạo b&aacute;m s&aacute;t nhu cầu thị trường. Đồng thời, hợp t&aacute;c cũng tạo điều kiện cho sinh vi&ecirc;n FUNiX đến tham quan, thực tập v&agrave; l&agrave;m việc tại doanh nghiệp.</p>\r\n\r\n<p>Ph&iacute;a FUNiX, đơn vị cam kết cung cấp nguồn nh&acirc;n lực chất lượng, đ&aacute;p ứng c&aacute;c ti&ecirc;u chuẩn tuyển dụng của Tiki. Ph&aacute;t biểu tại lễ k&yacute; kết, nh&agrave; s&aacute;ng lập Nguyễn Th&agrave;nh Nam kỳ vọng Tiki, c&ugrave;ng c&aacute;c doanh nghiệp hợp t&aacute;c sẽ l&agrave; những &quot;ngọn hải đăng&quot; để định hướng v&agrave; tạo động lực cho học vi&ecirc;n&nbsp;<a href=\"https://funix.edu.vn/\" rel=\"nofollow\">học lập tr&igrave;nh trực tuyến</a>&nbsp;vượt qua c&aacute;c thử th&aacute;ch khi tự học.</p>\r\n\r\n<p>&quot;Ch&uacute;ng t&ocirc;i muốn th&uacute;c đẩy m&ocirc;i trường học tập hướng đến doanh nghiệp, trong đ&oacute; học vi&ecirc;n c&oacute; cơ hội tiếp x&uacute;c v&agrave; hiểu về nh&agrave; tuyển dụng ngay từ những ng&agrave;y đầu ti&ecirc;n&quot;, &ocirc;ng chia sẻ.</p>\r\n\r\n<p>B&agrave; Nguyễn Thị Hồng Th&uacute;y - Ph&oacute; tổng gi&aacute;m đốc Nh&acirc;n sự Tiki cũng cho biết c&ocirc;ng ty mong muốn chi&ecirc;u mộ v&agrave; đ&agrave;o tạo được những bạn trẻ tiềm năng để chung tay ph&aacute;t triển Tiki trong giai đoạn b&ugrave;ng nổ về tăng trưởng sắp tới.</p>\r\n\r\n<p>&quot;Hợp t&aacute;c n&agrave;y sẽ l&agrave; một nền tảng kết nối hiệu quả giữa doanh nghiệp v&agrave; c&aacute;c bạn trẻ đam m&ecirc; c&ocirc;ng nghệ, mở ra cơ hội việc l&agrave;m cho học vi&ecirc;n v&agrave; ch&iacute;nh doanh nghiệp&quot;, b&agrave; n&oacute;i th&ecirc;m.</p>\r\n\r\n<p>Hiện, Tiki c&oacute; kế hoạch ưu ti&ecirc;n đầu tư v&agrave;o logistics v&agrave; c&ocirc;ng nghệ - hai mảnh gh&eacute;p cốt l&otilde;i của ng&agrave;nh thương mại điện tử. Trong đ&oacute; c&oacute; nền tảng Bảo hiểm trực tuyến Tiki360; Tini App - giải ph&aacute;p c&ocirc;ng nghệ mở cho ph&eacute;p c&aacute;c c&aacute; nh&acirc;n, đơn vị kinh doanh v&agrave; cộng đồng IT tự ph&aacute;t h&agrave;nh ứng dụng ri&ecirc;ng ngay tr&ecirc;n Tiki; chương tr&igrave;nh Mua sắm c&oacute; lời, thưởng điểm Astra theo hạng th&agrave;nh vi&ecirc;n ứng dụng c&ocirc;ng nghệ blockchain.</p>\r\n\r\n<p>Để phục vụ mục ti&ecirc;u ph&aacute;t triển n&agrave;y, Tiki cần&nbsp;<a href=\"https://tuyendung.tiki.vn/\" rel=\"nofollow\">tuyển</a>&nbsp;rất nhiều vị tr&iacute;, chi&ecirc;u mộ cho nhiều đội nh&oacute;m với số lượng v&agrave; ti&ecirc;u ch&iacute; về kỹ năng chuy&ecirc;n m&ocirc;n kh&aacute;c nhau.</p>', NULL, 1, 1, 7, '2022-06-14 07:31:54', '2022-06-14 08:10:57'),
(17, 'Cơ hội giao lưu với đại diện Đại học Y khoa SGU - Mỹ', 'co-hoi-giao-luu-voi-dai-dien-dai-hoc-y-khoa-sgu-my', 'co-hoi-giao-luu-voi-dai-dien-dai-hoc-y-khoa-sgu-my.jpg', '<p>TP HCM-C&ocirc;ng ty Unimates c&ugrave;ng Đại học Y khoa St. George - SGU (Mỹ) tổ chức hội thảo về du học v&agrave; h&agrave;nh nghề y ở Mỹ với sự tham gia của đại diện trường.</p>', '<p>Học v&agrave; h&agrave;nh nghề y kh&ocirc;ng phải l&agrave; h&agrave;nh tr&igrave;nh dễ d&agrave;ng đối với c&aacute;c bạn trẻ ở bất cứ quốc gia n&agrave;o, đặc biệt l&agrave; Mỹ. Do đ&oacute;, hai đơn vị tổ chức sự kiện nhằm gi&uacute;p sinh vi&ecirc;n Việt Nam t&igrave;m hiểu, nắm bắt cơ hội trở th&agrave;nh b&aacute;c sĩ tại xứ sở cờ hoa th&ocirc;ng qua cuộc gặp gỡ trực tiếp với c&ocirc; Nalinee Jiemvithayanukul - Trưởng Bộ phận Tuyển sinh khu vực Đ&ocirc;ng Nam &Aacute; của SGU (St. George&rsquo;s School of Medicine).</p>\r\n\r\n<p>Hội thảo diễn ra l&uacute;c 16h ng&agrave;y 27/6 tại Unimates Education, tầng 12, to&agrave; nh&agrave; International Plaza, 343 Phạm Ngũ L&atilde;o, Quận 1, TP HCM. Học sinh lớp 11, 12; sinh vi&ecirc;n năm 1,2 hoặc đ&atilde; tốt nghiệp c&aacute;c trường nh&oacute;m ng&agrave;nh y dược, ho&aacute;, sinh v&agrave; phụ huynh muốn h&agrave;nh nghề tại Mỹ c&oacute; thể&nbsp;<a href=\"https://www.unimates.edu.vn/events/hoi-thao-du-hoc-hanhh-nghe-y-tai-my-canada-cung-sgu/\" rel=\"nofollow\">đăng k&yacute;</a>&nbsp;tham gia miễn ph&iacute;. Tại hội thảo, người tham dự c&oacute; thể mang theo bảng điểm 4 năm gần nhất để được đ&aacute;nh gi&aacute; cơ hội tr&uacute;ng tuyển v&agrave;o trường v&agrave; tư vấn lộ tr&igrave;nh th&iacute;ch hợp.</p>\r\n\r\n<p>Theo b&aacute;o c&aacute;o năm 2021 của Cục thống k&ecirc; lao động - Bộ Lao động Mỹ, mức thu nhập của c&aacute;c b&aacute;c sĩ tại nước n&agrave;y đạt khoảng 208.000 USD một năm, tương đương với hơn 4,8 tỷ đồng. Trong khi đ&oacute;, Mỹ c&oacute; khoảng 727.000 b&aacute;c sĩ đang h&agrave;nh nghề. Số việc l&agrave;m cho ng&agrave;nh cũng dự kiến tăng 3% v&agrave; cần bổ sung th&ecirc;m 22,700 b&aacute;c sĩ mỗi năm, đặc biệt l&agrave; sau Covid-19.</p>\r\n\r\n<p><a href=\"https://www.unimates.edu.vn/schools/st-georges-university/\" rel=\"nofollow\" target=\"_blank\">SGU</a>&nbsp;l&agrave; trường đại học tư thục, th&agrave;nh lập v&agrave;o năm 1976, nằm tại khu vực Great River thuộc v&ugrave;ng biển Caribbean, c&aacute;ch th&agrave;nh phố Miami, bang Florida bốn tiếng đi m&aacute;y bay. Trường c&oacute; số lượng sinh vi&ecirc;n tốt nghiệp được cấp ph&eacute;p h&agrave;nh nghề lớn nhất tại Mỹ với 13.231 người. Theo thống k&ecirc; của đơn vị năm 2021, 97% sinh vi&ecirc;n vượt qua b&agrave;i thi USMLE 1 ngay lần đầu ti&ecirc;n; hơn 19.000 b&aacute;c sĩ đ&atilde; tốt nghiệp v&agrave; đang l&agrave;m việc tại 50 quốc gia. Năm 2020, SGU cũng c&oacute; c&oacute; hơn 1,090 sinh vi&ecirc;n d&agrave;nh được cơ hội nội tr&uacute; tại bệnh viện ở Mỹ v&agrave; Canada.</p>', NULL, 1, 1, 8, '2022-06-14 07:32:14', '2022-06-14 08:12:47'),
(18, 'Cơ hội nhận học bổng MBA quốc tế đến 200 triệu đồng', 'co-hoi-nhan-hoc-bong-mba-quoc-te-den-200-trieu-dong', 'co-hoi-nhan-hoc-bong-mba-quoc-te-den-200-trieu-dong.jpg', '<p>Viện Quản trị &amp; C&ocirc;ng nghệ FSB (Đại học FPT) c&ugrave;ng Đại học Leeds Beckett (Anh) giảm đến 50% học ph&iacute; chương tr&igrave;nh Thạc sĩ Quản trị kinh doanh (MBA) quốc tế.</p>', '<p>Theo đ&oacute;, học vi&ecirc;n c&oacute; thể học v&agrave; nhận bằng Thạc sĩ Quản trị kinh doanh MBA Anh Quốc ngay trong Việt Nam, tiết kiệm chi ph&iacute; du học v&agrave; đảm bảo c&ocirc;ng việc, cuộc sống c&aacute; nh&acirc;n. Hai đơn vị hướng tới tối ưu t&agrave;i ch&iacute;nh cho học vi&ecirc;n, c&acirc;n đối chi ph&iacute; học tập thấp hơn 50% so với tại Anh v&agrave; trao&nbsp;<a href=\"http://lbu.fsb.edu.vn/hocbongmba/\" rel=\"nofollow\" target=\"_blank\">học bổng</a>&nbsp;tối đa 200 triệu đồng.</p>\r\n\r\n<p>Quỹ học bổng chương tr&igrave;nh MBA li&ecirc;n kết với Leeds Beckett mang t&ecirc;n &quot;Thủ lĩnh t&agrave;i năng 2022&quot;. Đại diện FSB cho biết, đơn vị kỳ vọng t&igrave;m thấy những ứng vi&ecirc;n t&agrave;i năng, nh&agrave; l&atilde;nh đạo c&oacute; tham vọng v&agrave; tư duy đổi mới. Từ đ&oacute;, c&aacute;c học vi&ecirc;n sau khi tốt nghiệp c&oacute; thể x&acirc;y dựng n&ecirc;n một cộng đồng thủ lĩnh chung ch&iacute; hướng, trở th&agrave;nh những nh&agrave; l&atilde;nh đạo mang tầm nh&igrave;n, hiểu biết v&agrave; kinh nghiệm quản trị quốc tế.</p>\r\n\r\n<p>Để nhận học bổng, ứng vi&ecirc;n cần c&oacute; tối thiểu hai năm kinh nghiệm l&agrave;m việc v&agrave; vượt qua hai v&ograve;ng đ&aacute;nh gi&aacute;. V&ograve;ng đầu, ứng vi&ecirc;n chia sẻ tham vọng của bản th&acirc;n khi đăng k&yacute; chương tr&igrave;nh th&ocirc;ng qua b&agrave;i luận. Vượt qua nội dung n&agrave;y, c&aacute;c bạn sẽ được mời tham gia v&ograve;ng phỏng vấn với Hội đồng gi&aacute;m khảo l&agrave; c&aacute;c giảng vi&ecirc;n, doanh nh&acirc;n, chuy&ecirc;n gia của FSB.</p>\r\n\r\n<p>Buổi phỏng vấn sẽ xoay quanh c&aacute;c vấn đề về kiến thức quản trị, kinh doanh, l&atilde;nh đạo, những chủ điểm kinh tế &quot;n&oacute;ng&quot; của Việt Nam, khu vực v&agrave; thế giới. Qua qua c&aacute;ch xử l&yacute; t&igrave;nh huống v&agrave; lập luận của ứng vi&ecirc;n, hội đồng sẽ đ&aacute;nh gi&aacute; tư duy, năng lực, sự nhạy b&eacute;n. Đ&acirc;y cũng sẽ l&agrave; cơ hội để ứng vi&ecirc;n giao lưu, kết nối với những thủ lĩnh trẻ kh&aacute;c, mở rộng quan hệ x&atilde; hội v&agrave; cơ hội kinh doanh.</p>\r\n\r\n<p>Th&ocirc;ng b&aacute;o kết quả học bổng sẽ được c&ocirc;ng bố trong v&ograve;ng năm ng&agrave;y sau khi kết th&uacute;c c&aacute;c v&ograve;ng tuyển chọn.</p>\r\n\r\n<p>Đại diện FSB cho biết, sau ảnh hưởng của đại dịch, nhu cầu v&agrave; kỳ vọng của người học MBA đ&atilde; tăng l&ecirc;n đ&aacute;ng kể. Hầu hết c&aacute;c nh&agrave; l&atilde;nh đạo, quản l&yacute; trẻ c&oacute; đủ điều kiện t&agrave;i ch&iacute;nh sẽ t&igrave;m đến chương tr&igrave;nh đ&agrave;o tạo quốc tế để c&oacute; cơ hội tiếp cận nền gi&aacute;o dục hiện đại, mở rộng con đường thăng tiến hoặc hướng tới vị tr&iacute; quản l&yacute; tại c&aacute;c tập đo&agrave;n to&agrave;n cầu.</p>\r\n\r\n<p>V&igrave; vậy, Viện Quản trị &amp; C&ocirc;ng nghệ FSB (Đại học FPT) li&ecirc;n kết với Đại học Leeds Beckett (Anh) nhằm mang đến chương tr&igrave;nh MBA chuẩn quốc tế cho người trẻ Việt.<strong>&nbsp;</strong>Vương quốc Anh l&agrave; một trong những quốc gia dẫn đầu trong hầu hết c&aacute;c bảng xếp hạng c&aacute;c chương tr&igrave;nh MBA tốt nhất thế giới. Với truyền thống l&acirc;u đời, nhiều trường đại học Anh Quốc li&ecirc;n kết chặt chẽ với doanh nghiệp, hỗ trợ t&iacute;ch cực cho hoạt động giao lưu, mở rộng mạng lưới quan hệ của học vi&ecirc;n.</p>', NULL, 1, 1, 8, '2022-06-14 07:32:22', '2022-06-14 08:13:57');
INSERT INTO `news` (`id`, `name`, `slug`, `image`, `info`, `describe`, `view`, `active`, `user_adnin`, `cat_id`, `created_at`, `updated_at`) VALUES
(19, 'Nữ sinh trúng tuyển cả 8 trường Ivy League', 'nu-sinh-trung-tuyen-ca-8-truong-ivy-league', 'nu-sinh-trung-tuyen-ca-8-truong-ivy-league.jpg', '<p>Kh&ocirc;ng chỉ được 8 trường Ivy League mời nhập học v&agrave; cấp học bổng, Ashley Adirika c&ograve;n tr&uacute;ng tuyển 7 trường top đầu kh&aacute;c ở Mỹ.</p>', '<p>Trở th&agrave;nh sinh vi&ecirc;n của một trong 8 trường thuộc Ivy League lu&ocirc;n l&agrave; mơ ước của Ashley Adirika, người Mỹ gốc Nigeria. Năm ngo&aacute;i, sau cuộc n&oacute;i chuyện với một người bạn v&agrave; được khuyến kh&iacute;ch theo đuổi ước mơ, nữ sinh trường Trung học Miami Beach, ở th&agrave;nh phố nghỉ dưỡng Miami Beach, bang Florida, đ&atilde; ứng tuyển to&agrave;n bộ 8 trường thuộc nh&oacute;m n&agrave;y.</p>\r\n\r\n<p>V&agrave;o ng&agrave;y c&aacute;c trường danh tiếng c&ocirc;ng bố kết quả, Adirika mở t&aacute;m thanh c&ocirc;ng cụ tr&ecirc;n m&aacute;y t&iacute;nh, lần lượt nhận th&ocirc;ng b&aacute;o tr&uacute;ng tuyển v&agrave; được cả t&aacute;m trường: Brown, Columbia, Cornell, Dartmouth, Harvard, Penn, Princeton v&agrave; Yale cấp học bổng.</p>\r\n\r\n<p>&quot;Em đ&atilde; quyết định nộp 8 trường xem kết quả ra sao. Em kh&ocirc;ng nghĩ sẽ được chấp nhận tất cả&quot;,<em>&nbsp;CNN</em>&nbsp;dẫn lời nữ sinh 17 tuổi nhớ lại khoảnh khắc biết tin đỗ.</p>\r\n\r\n<p>Từ năm 2018, mỗi trường thuộc Ivy League chấp nhận chưa đến 12%. Năm nay tỷ lệ chấp nhận của Yale l&agrave; 4,5%, Columbia 3,7%, trong khi Đại học Harvard chỉ 3,19% cho kh&oacute;a 2022-2026.</p>\r\n\r\n<p>Ngo&agrave;i Ivy League, Adirika c&ograve;n được 7 trường h&agrave;ng đầu kh&aacute;c chấp nhận, trong đ&oacute; c&oacute; Đại học Stanford, Vanderbilt v&agrave; Emory.</p>\r\n\r\n<p>&quot;Thật kh&oacute; tin. Cảm gi&aacute;c thật tuyệt vời khi biết nỗ lực của em đ&atilde; được đền đ&aacute;p v&agrave; c&aacute;c trường đại học cũng nhận ra điều đ&oacute;. Những giọt nước mắt bắt đầu tr&agrave;o ra. Em v&agrave; mọi người thực sự phấn kh&iacute;ch, la h&eacute;t v&agrave; nhảy v&igrave; vui sướng&quot;, Adirika, 17 tuổi, kể.</p>\r\n\r\n<p>Adirika th&iacute;ch Yale v&agrave; Harvard. L&uacute;c nộp hồ sơ, Yale l&agrave; lựa chọn số một của em. Nhưng khi nghi&ecirc;n cứu kỹ hơn về ch&iacute;nh s&aacute;ch v&agrave; ch&iacute;nh s&aacute;ch x&atilde; hội, Adirika nhận thấy Harvard c&oacute; chương tr&igrave;nh tốt hơn.</p>\r\n\r\n<p>Adirika chọn Harvard, dự định học ng&agrave;nh ch&iacute;nh phủ v&agrave; nhập học m&ugrave;a thu n&agrave;y. Mục ti&ecirc;u của em l&agrave; t&igrave;m hiểu c&aacute;ch thức hoạt động của ch&iacute;nh phủ v&agrave; c&aacute;ch c&aacute;c ch&iacute;nh s&aacute;ch c&oacute; thể gi&uacute;p khắc phục sự ch&ecirc;nh lệch kinh tế trong c&aacute;c cộng đồng.</p>\r\n\r\n<p>Tại trường trung học, Adirika tham gia v&agrave;o đội tranh biện v&agrave; l&agrave; chủ tịch hội học sinh. Việc trở th&agrave;nh th&agrave;nh vi&ecirc;n đội tranh biện gi&uacute;p em x&acirc;y dựng sự tự tin v&agrave; n&acirc;ng cao tiếng n&oacute;i của m&igrave;nh.</p>\r\n\r\n<p>&quot;Em l&agrave; người y&ecirc;u th&iacute;ch học những điều mới mẻ v&agrave; tranh biện mang lại cho em cơ hội đ&oacute;. Hơn nữa, tranh biện c&ograve;n gi&uacute;p em c&oacute; nền tảng để n&oacute;i về những điều em tin v&agrave; những điều quan trọng với em&quot;, Adirika chia sẻ.</p>\r\n\r\n<p>Bess Rodriguez, huấn luyện vi&ecirc;n tranh biện trường trung học cơ sở th&agrave;nh phố Carol gần đ&oacute;, đ&atilde; tuyển Adirika v&agrave;o đội khi em học lớp 8. Gi&aacute;o vi&ecirc;n tiếng Anh n&agrave;y cho biết, Adirika lu&ocirc;n t&ograve; m&ograve; về c&aacute;ch thế giới vận h&agrave;nh.</p>', 1, 1, 1, 5, '2022-06-14 08:15:55', '2022-06-14 08:18:03'),
(20, 'Băn khoăn về ngành An toàn thông tin', 'ban-khoan-ve-nganh-an-toan-thong-tin', 'ban-khoan-ve-nganh-an-toan-thong-tin.jpg', '<p>Em x&aacute;c định đăng k&yacute; x&eacute;t tuyển v&agrave;o ng&agrave;nh An to&agrave;n th&ocirc;ng tin trong đợt tuyển sinh đại học sắp tới nhưng vẫn c&ograve;n một số băn khoăn.</p>', '<p>Chỉ c&ograve;n một th&aacute;ng nữa, em tham gia kỳ thi tốt nghiệp THPT v&agrave; sẽ sử dụng kết quả thi để x&eacute;t tuyển đại học. Em đ&atilde; x&aacute;c định được ng&agrave;nh em chọn l&agrave; An to&agrave;n th&ocirc;ng tin nhưng c&ograve;n một số thắc mắc:</p>\r\n\r\n<p>1. Ng&agrave;nh An to&agrave;n th&ocirc;ng tin v&agrave; An ninh mạng c&oacute; điểm kh&aacute;c biệt hay kh&ocirc;ng?</p>\r\n\r\n<p>2. Trường c&ocirc;ng lập n&agrave;o c&oacute; ng&agrave;nh n&agrave;y với mức học ph&iacute; ph&ugrave; hợp? Em muốn hỏi c&acirc;u n&agrave;y v&igrave; năm nay c&aacute;c đại học đều c&oacute; xu hướng tăng học ph&iacute;, g&acirc;y kh&oacute; khăn cho gia đ&igrave;nh em.</p>\r\n\r\n<p>3. Hiện, em c&oacute; laptop nhưng đời kh&aacute; cũ. Trong hai năm đầu theo học ng&agrave;nh An to&agrave;n th&ocirc;ng tin, em c&oacute; cần sử dụng laptop thường xuy&ecirc;n kh&ocirc;ng v&agrave; cấu h&igrave;nh laptop như n&agrave;o l&agrave; đủ để học ng&agrave;nh n&agrave;y?</p>\r\n\r\n<p>4. Em c&oacute; cần phải giỏi về lập tr&igrave;nh khi học ng&agrave;nh n&agrave;y kh&ocirc;ng?</p>\r\n\r\n<p>Rất mong nhận được chia sẻ từ mọi người.</p>', NULL, 1, 1, 5, '2022-06-14 08:20:04', '2022-06-14 08:20:04'),
(21, 'Kỳ thủ cờ vua thành thủ khoa chuyên Toán', 'ky-thu-co-vua-thanh-thu-khoa-chuyen-toan', 'ky-thu-co-vua-thanh-thu-khoa-chuyen-toan.jpg', '<p>H&Agrave; NỘI-Bận thi đấu cờ vua, lớp 8 Hiếu mới bắt đầu tập trung học To&aacute;n nhưng nhanh ch&oacute;ng bật l&ecirc;n v&agrave; vừa trở th&agrave;nh thủ khoa lớp To&aacute;n chuy&ecirc;n Khoa học Tự nhi&ecirc;n.</p>', '<p>Đạt 10 điểm To&aacute;n chuy&ecirc;n, 9 To&aacute;n điều kiện v&agrave; 8,5 Ngữ văn, Ho&agrave;ng Minh Hiếu, lớp 9A, khối THCS của trường THPT chuy&ecirc;n H&agrave; Nội - Amsterdam, trở th&agrave;nh thủ khoa chuy&ecirc;n To&aacute;n, THPT chuy&ecirc;n Khoa học Tự nhi&ecirc;n, Đại học Khoa học Tự nhi&ecirc;n.</p>\r\n\r\n<p>Nam sinh cũng nằm trong top 6 th&iacute; sinh điểm cao v&agrave;o lớp chuy&ecirc;n To&aacute;n, THPT chuy&ecirc;n Đại học Sư phạm H&agrave; Nội. Tr&uacute;ng tuyển hai trường, Hiếu gỡ bỏ được nhiều &aacute;p lực v&agrave; đang tiếp tục &ocirc;n thi để chinh phục mục ti&ecirc;u THPT chuy&ecirc;n H&agrave; Nội - Amsterdam cuối tuần n&agrave;y.</p>\r\n\r\n<p>Hiếu cho hay,&nbsp;<a href=\"https://vnexpress.net/de-thi-6-mon-chuyen-vao-lop-10-truong-su-pham-ha-noi-4470780.html\" rel=\"dofollow\">đề thi năm nay</a>&nbsp;ở chuy&ecirc;n Sư phạm kh&aacute; lạ. D&ugrave; luyện quen đề những năm trước, em vẫn bất ngờ với c&aacute;c dạng b&agrave;i. Hiếu đ&aacute;nh gi&aacute; đề kh&ocirc;ng kh&oacute;, kiến thức kh&ocirc;ng nhiều v&agrave; c&oacute; thể đạt điểm 10 nếu cẩn thận. H&ocirc;m thi, Hiếu ho&agrave;n th&agrave;nh b&agrave;i thi sớm v&agrave; dự đo&aacute;n đạt hai điểm 10 m&ocirc;n To&aacute;n. L&uacute;c biết điểm, em bất ngờ v&igrave; chỉ đạt 9,5 To&aacute;n chuy&ecirc;n v&agrave; 9,75 To&aacute;n điều kiện.</p>\r\n\r\n<p>Kh&aacute;c với chuy&ecirc;n Sư phạm, Hiếu thận trọng với&nbsp;<a href=\"https://vnexpress.net/de-thi-bon-mon-chuyen-vao-lop-10-truong-khoa-hoc-tu-nhien-4472844.html\" rel=\"dofollow\">đề của chuy&ecirc;n Khoa học Tự nhi&ecirc;n</a>. Đề c&oacute; nhiều c&acirc;u v&agrave; kh&oacute;, nhất l&agrave; c&acirc;u về bất phương tr&igrave;nh v&agrave; tổ hợp. Ở đề thi chuy&ecirc;n, Hiếu dễ d&agrave;ng vượt qua những c&acirc;u đầu nhưng mất phần lớn thời gian với c&acirc;u về h&igrave;nh học 0,5 điểm. Lần n&agrave;y, Hiếu tự chấm được 9 điểm nhưng cuối c&ugrave;ng đạt điểm tuyệt đối.</p>\r\n\r\n<p>Nam sinh cũng nằm trong top 6 th&iacute; sinh điểm cao v&agrave;o lớp chuy&ecirc;n To&aacute;n, THPT chuy&ecirc;n Đại học Sư phạm H&agrave; Nội. Tr&uacute;ng tuyển hai trường, Hiếu gỡ bỏ được nhiều &aacute;p lực v&agrave; đang tiếp tục &ocirc;n thi để chinh phục mục ti&ecirc;u THPT chuy&ecirc;n H&agrave; Nội - Amsterdam cuối tuần n&agrave;y.</p>\r\n\r\n<p>Hiếu cho hay,&nbsp;<a href=\"https://vnexpress.net/de-thi-6-mon-chuyen-vao-lop-10-truong-su-pham-ha-noi-4470780.html\" rel=\"dofollow\">đề thi năm nay</a>&nbsp;ở chuy&ecirc;n Sư phạm kh&aacute; lạ. D&ugrave; luyện quen đề những năm trước, em vẫn bất ngờ với c&aacute;c dạng b&agrave;i. Hiếu đ&aacute;nh gi&aacute; đề kh&ocirc;ng kh&oacute;, kiến thức kh&ocirc;ng nhiều v&agrave; c&oacute; thể đạt điểm 10 nếu cẩn thận. H&ocirc;m thi, Hiếu ho&agrave;n th&agrave;nh b&agrave;i thi sớm v&agrave; dự đo&aacute;n đạt hai điểm 10 m&ocirc;n To&aacute;n. L&uacute;c biết điểm, em bất ngờ v&igrave; chỉ đạt 9,5 To&aacute;n chuy&ecirc;n v&agrave; 9,75 To&aacute;n điều kiện.</p>\r\n\r\n<p>Kh&aacute;c với chuy&ecirc;n Sư phạm, Hiếu thận trọng với&nbsp;<a href=\"https://vnexpress.net/de-thi-bon-mon-chuyen-vao-lop-10-truong-khoa-hoc-tu-nhien-4472844.html\" rel=\"dofollow\">đề của chuy&ecirc;n Khoa học Tự nhi&ecirc;n</a>. Đề c&oacute; nhiều c&acirc;u v&agrave; kh&oacute;, nhất l&agrave; c&acirc;u về bất phương tr&igrave;nh v&agrave; tổ hợp. Ở đề thi chuy&ecirc;n, Hiếu dễ d&agrave;ng vượt qua những c&acirc;u đầu nhưng mất phần lớn thời gian với c&acirc;u về h&igrave;nh học 0,5 điểm. Lần n&agrave;y, Hiếu tự chấm được 9 điểm nhưng cuối c&ugrave;ng đạt điểm tuyệt đối.</p>', NULL, NULL, 1, 5, '2022-06-14 08:22:11', '2022-06-14 08:22:11');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'view_new', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(2, 'view_user', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(3, 'view_user_admin', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(4, 'view_category', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(5, 'view_comment', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(6, 'view_tag', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(7, 'add_new', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(8, 'add_user', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(9, 'add_user_admin', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(10, 'add_category', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(11, 'add_comment', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(12, 'add_tag', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(13, 'edit_new', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(14, 'edit_user', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(15, 'edit_user_admin', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(16, 'edit_category', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(17, 'edit_comment', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(18, 'edit_tag', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(19, 'remove_new', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(20, 'remove_user', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(21, 'remove_user_admin', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(22, 'remove_category', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(23, 'remove_comment', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57'),
(24, 'remove_tag', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'super-admin', 'web', '2022-06-13 07:06:57', '2022-06-13 07:06:57');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebookLink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtobeLink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `settings`
--

INSERT INTO `settings` (`id`, `logo`, `info`, `facebookLink`, `youtobeLink`, `created_at`, `updated_at`) VALUES
(1, 'logo.png', 'awd', 'awd', 'awd', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tags`
--

INSERT INTO `tags` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'giao duc', 'giao-duc', '2022-06-13 07:14:20', '2022-06-13 07:14:20'),
(2, 'giao duc 2', 'giao-duc-2', '2022-06-13 07:14:29', '2022-06-13 07:14:29');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tag_new`
--

CREATE TABLE `tag_new` (
  `new_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tag_new`
--

INSERT INTO `tag_new` (`new_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(6, 1, '2022-06-14 07:30:26', '2022-06-14 07:30:26'),
(7, 1, '2022-06-14 07:30:33', '2022-06-14 07:30:33'),
(9, 1, '2022-06-14 07:30:55', '2022-06-14 07:30:55'),
(10, 1, '2022-06-14 07:31:02', '2022-06-14 07:31:02'),
(12, 1, '2022-06-14 07:31:14', '2022-06-14 07:31:14'),
(13, 1, '2022-06-14 07:31:28', '2022-06-14 07:31:28'),
(14, 1, '2022-06-14 07:31:39', '2022-06-14 07:31:39'),
(15, 1, '2022-06-14 07:31:47', '2022-06-14 07:31:47'),
(16, 1, '2022-06-14 07:31:54', '2022-06-14 07:31:54'),
(17, 1, '2022-06-14 07:32:14', '2022-06-14 07:32:14'),
(18, 1, '2022-06-14 07:32:22', '2022-06-14 07:32:22'),
(8, 2, NULL, NULL),
(19, 1, '2022-06-14 08:15:55', '2022-06-14 08:15:55'),
(20, 1, '2022-06-14 08:20:04', '2022-06-14 08:20:04'),
(21, 1, '2022-06-14 08:22:11', '2022-06-14 08:22:11');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `useradmins`
--

CREATE TABLE `useradmins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `useradmins`
--

INSERT INTO `useradmins` (`id`, `name`, `image`, `level`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Đinh Xuân Thu', 'no-img.jpg', 2, 'admin@gmail.com', NULL, '$2y$10$2gCDv5B6t8iqRw1iDeXFzOGHxkCvBtD4TCEWfTcL4OqeFXDPz4OHC', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `image`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Đinh Xuân Thu', 'no-img.jpg', 'dinhxuanthu@gmail.com', '$2y$10$Zy1lkdOMOEsUGgjoVkRw7uV1OHznLplC3zoz8ez3ojaSjeYFLwlSq', NULL, NULL),
(2, 'Huy Van', 'no-img.png', 'huyvan@gmail.com', '$2y$10$C.hYhO1KXV0M9sy9rOxEkeKP3bO6QKrUpsa0hJTKcryGxYgeT25lm', '2022-06-14 08:47:05', '2022-06-14 08:47:05'),
(3, 'huy van chu', 'no-img.png', 'vanhuy@gmail.com', '$2y$10$8OmDd/p2zzbB5mgkSp1uyOr.4Bew6iQ0DIeiFlwu3DQapVHskoTnS', '2022-06-14 08:49:34', '2022-06-14 08:49:34');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_use_id_foreign` (`use_id`);

--
-- Chỉ mục cho bảng `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_new_id_foreign` (`new_id`),
  ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Chỉ mục cho bảng `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_user_adnin_foreign` (`user_adnin`),
  ADD KEY `news_cat_id_foreign` (`cat_id`);

--
-- Chỉ mục cho bảng `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Chỉ mục cho bảng `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Chỉ mục cho bảng `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tag_new`
--
ALTER TABLE `tag_new`
  ADD KEY `tag_new_new_id_foreign` (`new_id`),
  ADD KEY `tag_new_tag_id_foreign` (`tag_id`);

--
-- Chỉ mục cho bảng `useradmins`
--
ALTER TABLE `useradmins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `useradmins_level_unique` (`level`),
  ADD UNIQUE KEY `useradmins_email_unique` (`email`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT cho bảng `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `useradmins`
--
ALTER TABLE `useradmins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_use_id_foreign` FOREIGN KEY (`use_id`) REFERENCES `useradmins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_new_id_foreign` FOREIGN KEY (`new_id`) REFERENCES `news` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `news_user_adnin_foreign` FOREIGN KEY (`user_adnin`) REFERENCES `useradmins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `tag_new`
--
ALTER TABLE `tag_new`
  ADD CONSTRAINT `tag_new_new_id_foreign` FOREIGN KEY (`new_id`) REFERENCES `news` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tag_new_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
