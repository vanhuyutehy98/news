<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(RolePermissionSeeder::class);
        $this->call(UserAdminSeeder::class);
        $this->call(UserSeeder::class);
    }
}