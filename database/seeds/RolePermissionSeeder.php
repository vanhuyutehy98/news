<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class RolePermissionSeeder extends Seeder
{/**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();
        // Tạo quyền
        // view
        Permission::create(['guard_name' => 'web', 'name' => 'view_new']);
        Permission::create(['guard_name' => 'web', 'name' => 'view_user']);
        Permission::create(['guard_name' => 'web', 'name' => 'view_user_admin']);
        Permission::create(['guard_name' => 'web', 'name' => 'view_category']);
        Permission::create(['guard_name' => 'web', 'name' => 'view_comment']);
        Permission::create(['guard_name' => 'web', 'name' => 'view_tag']);
        // add
        Permission::create(['guard_name' => 'web', 'name' => 'add_new']);
        Permission::create(['guard_name' => 'web', 'name' => 'add_user']);
        Permission::create(['guard_name' => 'web', 'name' => 'add_user_admin']);
        Permission::create(['guard_name' => 'web', 'name' => 'add_category']);
        Permission::create(['guard_name' => 'web', 'name' => 'add_comment']);
        Permission::create(['guard_name' => 'web', 'name' => 'add_tag']);
        // edit
        Permission::create(['guard_name' => 'web', 'name' => 'edit_new']);
        Permission::create(['guard_name' => 'web', 'name' => 'edit_user']);
        Permission::create(['guard_name' => 'web', 'name' => 'edit_user_admin']);
        Permission::create(['guard_name' => 'web', 'name' => 'edit_category']);
        Permission::create(['guard_name' => 'web', 'name' => 'edit_comment']);
        Permission::create(['guard_name' => 'web', 'name' => 'edit_tag']);
        // remove
        Permission::create(['guard_name' => 'web', 'name' => 'remove_new']);
        Permission::create(['guard_name' => 'web', 'name' => 'remove_user']);
        Permission::create(['guard_name' => 'web', 'name' => 'remove_user_admin']);
        Permission::create(['guard_name' => 'web', 'name' => 'remove_category']);
        Permission::create(['guard_name' => 'web', 'name' => 'remove_comment']);
        Permission::create(['guard_name' => 'web', 'name' => 'remove_tag']);

        // Tạo Role(Chức vụ) và gán quyền
        $role = Role::create(['guard_name' => 'web', 'name' => 'super-admin']);
        // view
        $role->givePermissionTo('view_new');
        $role->givePermissionTo('view_user');
        $role->givePermissionTo('view_user_admin');
        $role->givePermissionTo('view_category');
        $role->givePermissionTo('view_comment');
        $role->givePermissionTo('view_tag');
        // add
        $role->givePermissionTo('add_new');
        $role->givePermissionTo('add_user');
        $role->givePermissionTo('add_user_admin');
        $role->givePermissionTo('add_category');
        $role->givePermissionTo('add_comment');
        $role->givePermissionTo('add_tag');
        // edit
        $role->givePermissionTo('edit_new');
        $role->givePermissionTo('edit_user');
        $role->givePermissionTo('edit_user_admin');
        $role->givePermissionTo('edit_category');
        $role->givePermissionTo('edit_comment');
        $role->givePermissionTo('edit_tag');
        // remove
        $role->givePermissionTo('remove_new');
        $role->givePermissionTo('remove_user');
        $role->givePermissionTo('remove_user_admin');
        $role->givePermissionTo('remove_category');
        $role->givePermissionTo('remove_comment');
        $role->givePermissionTo('remove_tag');
    }
}
