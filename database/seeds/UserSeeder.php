<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            [
                'id' => '1',
                'name' => 'Đinh Xuân Thu',
                'email' => 'dinhxuanthu@gmail.com',
                'image' => 'no-img.jpg',
                'password' => Hash::make('dinhxuanthu'),
            ]
        ]);
    }
}
