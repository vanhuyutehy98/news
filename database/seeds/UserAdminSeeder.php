<?php

use App\Models\userAdmin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserAdminSeeder extends Seeder
{
    public function run()
    {
        DB::table('useradmins')->delete();
        DB::table('useradmins')->insert([
            [
                'id' => '1',
                'name' => 'Đinh Xuân Thu',
                'email' => 'admin@gmail.com',
                'image' => 'no-img.jpg',
                'level' => '2',
                'password' => Hash::make('admin'),
            ]
        ]);
        userAdmin::find(1)->assignRole('super-admin');
    }
}
