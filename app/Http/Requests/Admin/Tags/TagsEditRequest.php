<?php

namespace App\Http\Requests\Admin\Tags;

use Illuminate\Foundation\Http\FormRequest;

class TagsEditRequest extends FormRequest
{
   public function authorize()
   {
       return true;
   }

   public function rules()
   {
       return [
        'name'=>'required|unique:tags,name,'.$this->id,
       ];
   }
   public function messages()
   {
       return [
           'name.required' => 'Hãy chọn tên',
       ];
   }
}
