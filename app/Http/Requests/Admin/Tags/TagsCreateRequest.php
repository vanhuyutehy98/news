<?php

namespace App\Http\Requests\Admin\Tags;

use Illuminate\Foundation\Http\FormRequest;

class TagsCreateRequest extends FormRequest
{ /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
   public function authorize()
   {
       return true;
   }

   public function rules()
   {
       return [
        'name'=>'required|unique:tags,name',
       ];
   }
   public function messages()
   {
       return [
           'name.required' => 'Hãy chọn tên',
       ];
   }
}
