<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'=>'required|unique:useradmins,name,'.$this->id,
            'email'=>'required|unique:useradmins,email,'.$this->id,
            'image' => 'mimes:png,jpeg,gif,jpg|max:2048',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên',
            'name.unique' => 'Tên quản trị đã tồn tại',
            'email.required' => 'Chưa nhập email',
            'email.unique' => 'Email đã tồn tại',
        ];
    }
}
