<?php

namespace App\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;

class CategoryEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|unique:categories,name,'.$this->id,
            'image' => 'mimes:png,jpeg,gif,jpg|max:2048',
            'info'=>'required',
            'describe'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên danh mục tên',
            'describe.required' => 'Chưa nhập mô tả',
            'info.required' => 'Chưa nhập tên giới thiệu danh mục',
            'name.unique' => 'Đã tồn tại tên danh mục',
            'image.mimes' => 'Định dạng ảnh chưa chính xác: png, jpeg, gif, jpg',
            'image.max' => 'Kích cỡ ảnh vượt quá 2MB',

        ];
    }
}
