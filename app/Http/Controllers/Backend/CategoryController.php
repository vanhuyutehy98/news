<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\CategoryCreateRequest;
use App\Http\Requests\Admin\Category\CategoryEditRequest;
use App\Models\categories;
use App\Models\comments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


class CategoryController extends Controller
{
    public function index()
    {
        $data['Categories'] = categories::paginate(15);
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        return view('backend.category.index', $data);
    }
    public function add()
    {
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        return view('backend.category.add', $data);
    }
    public function store(CategoryCreateRequest $request)
    {
        if ($request->active) {
            $check = categories::where('active', '1')->get()->count();
            if ($check >= 5) {
                return back()->with('thong-bao-loi', 'Chỉ được 5 bài viết được active');
            }
        }
        $category = new Categories();
        $category->name = $request->name;
        $category->slug = Str::slug($request->name);
        $category->info = $request->info;
        $category->describe = $request->describe;
        $category->active = $request->active;
        $category->use_id  = Auth::user()->id;
        if ($request->image != '') {
            $image = $request->file('image');
            $nameValue = Str::slug($request->name);
            $nameImageValue = $nameValue . '.' . $image->extension();
            $file_old = public_path('images\category\\') . $nameImageValue;
            if (file_exists($file_old) != null) {
                unlink($file_old);
                $destinationPath = public_path('images\category\\');
                $image->move($destinationPath, $nameImageValue);
            } else {
                $destinationPath = public_path('images\category\\');
                $image->move($destinationPath, $nameImageValue);
            }
            $category->image = $nameImageValue;
        } else {
            $category->image = 'no-img.jpg';
        }
        $category->save();
        return redirect()->route('admin.category.index')->with('thong-bao-thanh-cong', 'Đã thêm thành công danh mục ' . $request->name_category);

    }
    public function edit(Request $request)
    {
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        $data['category'] = categories::find($request->cat_id);
        return view('backend.category.edit', $data);
    }
    function update(CategoryEditRequest $request, $id)
    {
        $find = Categories::find($id);
        $find->name = $request->name;
        $find->slug = Str::slug($request->name);
        $find->info = $request->info;
        $find->describe = $request->describe;
        $find->active = $request->active;
        $find->use_id  = Auth::user()->id;
        if ($request->image != '') {
            $file_old_del_file_cu = public_path('images\category\\') . $find->image;
            if (file_exists($file_old_del_file_cu) != null && $find->image != 'no-img.jpg') {
                unlink($file_old_del_file_cu);
            }
            $image = $request->file('image');
            $nameValue = Str::slug($request->name);
            $nameImageValue = $nameValue . '.' . $image->extension();
            $file_old = public_path('images\category\\') . $nameImageValue;
            if (file_exists($file_old) != null) {
                unlink($file_old);
                $destinationPath = public_path('images\category\\');
                $image->move($destinationPath, $nameImageValue);
            } else {
                $destinationPath = public_path('images\category\\');
                $image->move($destinationPath, $nameImageValue);
            }
            $find->image = $nameImageValue;
        } else {
            $find->image = $find->image;
        }

        $find->save();
        return redirect()->route('admin.category.index')->with('thong-bao-thanh-cong', 'Đã cập nhật thành công danh mục ' . $request->name);
    }
    public function remove($id, Request $request)
    {
        $category = Categories::find($id);
        $file_old_del_file_cu = public_path('category\\') . $category->image;
        if (file_exists($file_old_del_file_cu) != null) {
            if ($category->image!='no-img.jpg') {
                unlink($file_old_del_file_cu);
            }
        }
        $category->delete();
        return redirect()->route('admin.category.index')->with('thong-bao-thanh-cong', 'Đã xóa thành công danh mục ' . $request->name);
    }
}
