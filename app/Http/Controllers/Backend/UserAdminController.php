<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\AddUserRequest;
use App\Models\comments;
use App\Models\userAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserAdminController extends Controller
{
    public function index()
    {
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        $data['users'] = userAdmin::paginate(15);
        return view('backend.user_admin.index', $data);
    }
    public function add()
    {
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        return view('backend.user_admin.add', $data);
    }
    public function store(AddUserRequest $request)
    {
        if (strlen($request->password) < 8) {
            return back()->with('thong-bao-loi', 'Mật khẩu quá ngắn, nó phải dài hơn 8 kí tự');
        } elseif (strlen($request->password) > 30) {
            return back()->with('thong-bao-loi', 'Mật khẩu quá dài, nó phải ngắn hơn 30 kí tự');
        }
        $user = new userAdmin();
        $user->name = $request->name;
        $user->level = 0;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        if ($request->image != null) {
            $file = $request->file('image');
            $nameValue = Str::slug($request->name);
            $file_old = public_path('images\users\\') . $request->image;
            if (file_exists($file_old) != null && $request->image != 'no-img.jpg') {
                $nameImageValue = $nameValue . '-' . Str::random(10) . '.' . $file->extension();
            } else {
                $nameImageValue = $nameValue . '.' . $file->extension();
            }
            $destinationPath = public_path('images\users\\');
            $file->move($destinationPath, $nameImageValue);
            $user->image = $nameImageValue;
        } else {
            $user->image = 'no-img.jpg';
        }
        $user->save();
        return redirect()->route('admin.user_admin.index')->with('thong-bao-thanh-cong', 'Thêm thành công quản trị ' . $request->name);
    }
    public function edit($id)
    {
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        $data['user'] = userAdmin::find($id);
        return view('backend.user_admin.edit', $data);
    }
    public function update(Request $request, $id)
    {
        $user = userAdmin::find($id);
        // Trao quyền
        //Tìm user theo id
        //Xóa tất cả các quyền đã tồn tại của user theo id đó
        $user->revokePermissionTo(
            [
                'view_new', 'view_user', 'view_category', 'view_comment', 'view_tag',
                'add_new', 'add_user', 'add_category', 'add_comment', 'add_tag',
                'edit_new', 'edit_user', 'edit_category', 'edit_comment', 'edit_tag',
                'remove_new', 'remove_user', 'remove_category', 'remove_comment', 'remove_tag',
            ]
        );
        //Kiểm tra user đó có bất kỳ Role nào không
        if ($user->hasAnyRole(['user', 'admin'])) {
            //Gọi tên Role của user đó
            $roles = $user->getRoleNames();
            foreach ($roles as $key => $value) {
                //Xóa role của user đó
                $user->removeRole($value);
            }
        }
        //Tiến hành thêm quyền khi tích vào ô checkbox
        if ($request->view_new) {
            $user->givePermissionTo('view_new'); //Gán quyền view_new
        }
        if ($request->add_new) {
            $user->givePermissionTo('add_new'); //Gán quyền add_new
        }
        if ($request->edit_new) {
            $user->givePermissionTo('edit_new'); //Gán quyền edit_new
        }
        if ($request->remove_new) {
            $user->givePermissionTo('remove_new'); //Gán quyền remove_new
        }
        // user
        if ($request->view_user) {
            $user->givePermissionTo('view_user'); //Gán quyền view_user
        }
        if ($request->add_user) {
            $user->givePermissionTo('add_user'); //Gán quyền add_user
        }
        if ($request->edit_user) {
            $user->givePermissionTo('edit_user'); //Gán quyền edit_user
        }
        if ($request->remove_user) {
            $user->givePermissionTo('remove_user'); //Gán quyền remove_user
        }
        // category
        if ($request->view_category) {
            $user->givePermissionTo('view_category'); //Gán quyền view_category
        }
        if ($request->add_category) {
            $user->givePermissionTo('add_category'); //Gán quyền add_category
        }
        if ($request->edit_category) {
            $user->givePermissionTo('edit_category'); //Gán quyền edit_category
        }
        if ($request->remove_category) {
            $user->givePermissionTo('remove_category'); //Gán quyền remove_category
        }
        // Comment
        if ($request->view_comment) {
            $user->givePermissionTo('view_comment'); //Gán quyền view_comment
        }
        if ($request->add_comment) {
            $user->givePermissionTo('add_comment'); //Gán quyền add_comment
        }
        if ($request->edit_comment) {
            $user->givePermissionTo('edit_comment'); //Gán quyền edit_comment
        }
        if ($request->remove_comment) {
            $user->givePermissionTo('remove_comment'); //Gán quyền remove_comment
        }
        // view_tag
        if ($request->view_tag) {
            $user->givePermissionTo('view_tag'); //Gán quyền view_tag
        }
        if ($request->add_tag) {
            $user->givePermissionTo('add_tag'); //Gán quyền add_tag
        }
        if ($request->view_tag) {
            $user->givePermissionTo('view_tag'); //Gán quyền view_tag
        }
        if ($request->remove_tag) {
            $user->givePermissionTo('remove_tag'); //Gán quyền remove_tag
        }
        if ($user->level != 1) {
            $user->level = 0;
        }
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->image != null) {
            $file_old = public_path('images\users\\') . $user->image;
            if (file_exists($file_old) != null && $user->image != 'no-img.jpg') {
                unlink($file_old);
            }
            $file = $request->file('image');
            $nameValue = Str::slug($request->name);
            $nameImageValue = $nameValue . '-' . Str::random(10) . '.' . $file->extension();
            $destinationPath = public_path('images\users\\');
            $file->move($destinationPath, $nameImageValue);
            $user->image = $nameImageValue;
        } else {
            $user->image = $user->image;
        }
        $user->save();
        return redirect()->route('admin.user_admin.index')->with('thong-bao-thanh-cong', 'Cập nhật thành công quản trị');
    }
    public function remove(Request $request, $id)
    {
        $user = userAdmin::find($id);
        if ($user->hasRole('super-admin') || $user->user_level == 1) {
            return back()->with('thong-bao-loi', 'Bạn không thể xóa tài khoản này');
        } else {
            $file_old = public_path('images\users\\') . $user->image;
            if (file_exists($file_old) != null && $user->image != 'no-img.jpg') {
                unlink($file_old);
            }
            $user->delete();
            return redirect()->route('admin_user_list')->with('thong-bao-thanh-cong', 'Đã xóa thành công quản trị ' . $request->name);
        }
    }
}
