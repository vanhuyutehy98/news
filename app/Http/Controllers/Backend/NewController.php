<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\News\NewsStoreRequest;
use App\Http\Requests\Admin\News\NewsUpdateRequest;
use App\Models\categories;
use App\Models\comments;
use App\Models\news;
use App\Models\tag_news;
use App\Models\tags;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class NewController extends Controller
{
    public function index()
    {
        $data['news'] = news::paginate(15);
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        return view('backend.news.index', $data);
    }
    public function add()
    {
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        $data['categories'] = categories::all();
        $data['tags'] = tags::all();
        return view('backend.news.add', $data);
    }
    public function store(NewsStoreRequest $request)
    {
        $news = new news();
        $news->name = $request->name;
        $news->slug = Str::slug($request->name);
        $news->info = $request->info;
        $news->describe = $request->describe;
        $news->active = $request->active;
        $news->cat_id  = $request->cat_id;
        $news->user_adnin = Auth::user()->id;
        // $news->tag_id  = $request->tag_id;
        if ($request->image != '') {
            $image = $request->file('image');
            $nameValue = Str::slug($request->name);
            $nameImageValue = $nameValue . '.' . $image->extension();
            $file_old = public_path('images\news\\') . $nameImageValue;
            if (file_exists($file_old) != null) {
                unlink($file_old);
                $destinationPath = public_path('images\news\\');
                $image->move($destinationPath, $nameImageValue);
            } else {
                $destinationPath = public_path('images\news\\');
                $image->move($destinationPath, $nameImageValue);
            }
            $news->image = $nameImageValue;
        } else {
            $news->image = 'no-img.jpg';
        }
        $news->save();
        if ($request->tags) {
            foreach ($request->tags as $key => $value) {
                $tag_new = new tag_news();
                $tag_new->new_id = $news->id;
                $tag_new->tag_id = $value;
                $tag_new->save();
            }
        }
        
        return redirect()->route('admin.news.index')->with('thong-bao-thanh-cong', 'Đã thêm thành công danh mục ' . $request->name_news);

    }
    public function edit($id)
    {
        $data['new'] = news::find($id);
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        $data['categories'] = categories::all();
        $data['tags'] = tags::all();
        return view('backend.news.edit', $data);
    }
    function update(NewsUpdateRequest $request, $id)
    {
        $news = news::find($id);
        $news->name = $request->name;
        $news->slug = Str::slug($request->name);
        $news->info = $request->info;
        $news->describe = $request->describe;
        $news->active = $request->active;
        $news->cat_id  = $request->cat_id;
        $news->user_adnin = Auth::user()->id;
        if ($request->image != '') {
            $file_old_del_file_cu = public_path('images\news\\') . $news->image;
            if (file_exists($file_old_del_file_cu) != null && $news->image != 'no-img.jpg') {
                unlink($file_old_del_file_cu);
            }
            $image = $request->file('image');
            $nameValue = Str::slug($request->name);
            $nameImageValue = $nameValue . '.' . $image->extension();
            $file_old = public_path('images\news\\') . $nameImageValue;
            if (file_exists($file_old) != null) {
                unlink($file_old);
                $destinationPath = public_path('images\news\\');
                $image->move($destinationPath, $nameImageValue);
            } else {
                $destinationPath = public_path('images\news\\');
                $image->move($destinationPath, $nameImageValue);
            }
            $news->image = $nameImageValue;
        } else {
            $news->image = $news->image;
        }
        $news->save();

        $news->tags()->Sync($request->tags);

        return redirect()->route('admin.news.index')->with('thong-bao-thanh-cong', 'Đã cập nhật thành công danh mục ' . $request->name);
    }
    public function remove($id, Request $request)
    {
        $news = news::find($id);
        $file_old_del_file_cu = public_path('images\news\\') . $news->image;
        if (file_exists($file_old_del_file_cu) != null) {
            if ($news->image!='no-img.jpg') {
                unlink($file_old_del_file_cu);
            }
        }
        $news->delete();
        return redirect()->route('admin.news.index')->with('thong-bao-thanh-cong', 'Đã xóa thành công danh mục ' . $request->name);
    }
}
