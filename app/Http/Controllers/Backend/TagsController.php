<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Tags\TagsCreateRequest;
use App\Http\Requests\Admin\Tags\TagsEditRequest;
use App\Models\comments;
use App\Models\tags;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TagsController extends Controller
{
    public function index()
    {
        $data['tags'] = tags::paginate(15);
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        return view('backend.tags.index', $data);
    }
    public function add()
    {
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        return view('backend.tags.add', $data);
    }
    public function store(TagsCreateRequest $request)
    {
        $tags = new tags();
        $tags->name = $request->name;
        $tags->slug = Str::slug($request->name);
        $tags->save();
        return redirect()->route('admin.tags.index')->with('thong-bao-thanh-cong', 'Đã thêm thành công danh mục ' . $request->name);

    }
    public function edit(Request $request)
    {
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        $data['tags'] = tags::find($request->id);
        return view('backend.tags.edit', $data);
    }
    function update(TagsEditRequest $request, $id)
    {
        $tag = tags::find($id);
        $tag->name = $request->name;
        $tag->slug = Str::slug($request->name);
        $tag->save();
        return redirect()->route('admin.tags.index')->with('thong-bao-thanh-cong', 'Đã cập nhật thành công danh mục ' . $request->name);
    }
    public function remove($id, Request $request)
    {
        $tags = tags::find($id);
        $tags->delete();
        return redirect()->route('admin.tags.index')->with('thong-bao-thanh-cong', 'Đã xóa thành công danh mục ' . $request->name);
    }
}
