<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\comments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function index(Request $request)
    {
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        // if ($request->search == 'unActive') {
        //     $data['comments'] = comments::where('state', '0')->paginate(15);
        // }else{
        //     $data['comments'] = comments::where('state', '1')->paginate(15);
        // }
        $data['comments'] = comments::where('state', '0')->paginate(15);
        return view('backend.comment.index', $data);
    }
    public function edit($id)
    {
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        $data['comment'] = comments::find($id);
        return view('backend.comment.edit', $data);
    }
    public function update(Request $request, $id)
    {
        $comment = comments::find($id);
        $newComment = new comments();
        $newComment->state = 1;
        $newComment->new_id = $comment->new->id;
        $newComment->name = Auth::guard('web')->user()->name;
        $newComment->comnent = $request->comment;
        $newComment->save();

        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        $data['comment'] = comments::find($id);
        return view('backend.comment.edit', $data);
    }
    public function success(Request $request)
    {
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        $data['comments'] = comments::where('state', '1')->paginate(15);
        return view('backend.comment.index', $data);
    }

    public function remove ($id)
    {
        $data = comments::find($id);
        $data->delete();
        return redirect()->route('admin.comment.index');
    }

}
