<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\comments;
use App\Models\news;
use App\Models\settings;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    public function index()
    {
        $data['commentNews']=comments::where('state',0)->orderBy('created_at', 'DESC')->take(20)->get();
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        return view('backend.index', $data);
    }
    public function setting($name)
    {
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];

        $setting = settings::find(1);
        if (empty($setting)) {
            $setting = new settings();
            $setting->logo = 'no-image.png';
            $setting->info = 'no info';
            $setting->facebookLink = 'no info';
            $setting->youtobeLink = 'no info';
            $setting->save();
            $data['setting'] = $setting;
            $data['name'] = $name;
            return view('backend.setting.index', $data);
        }
        $data['setting'] = $setting;
        $data['name'] = $name;
        return view('backend.setting.index', $data);
    }
    public function settingPost(Request $request)
    {
        $setting = settings::find(1);
        if ($request->image != '') {
            $image = $request->file('image');
            $nameValue = Str::slug($request->name);
            $nameImageValue = $nameValue . '.' . $image->extension();
            $file_old = public_path('images\setting\\') . $nameImageValue;
            if (file_exists($file_old) != null) {
                unlink($file_old);
                $destinationPath = public_path('images\setting\\');
                $image->move($destinationPath, $nameImageValue);
            } else {
                $destinationPath = public_path('images\setting\\');
                $image->move($destinationPath, $nameImageValue);
            }
            $setting->logo = $nameImageValue;
        } else {
            $setting->logo = 'no-img.jpg';
        }
        if ($request->facebook != '') {
            $setting->facebookLink = $request->facebook;
        }
        if ($request->youtobe != '') {
            $setting->youtobeLink = $request->youtobe;
        }
        $setting->save();
        return redirect()->back()->withInput()->with('thong-bao-thanh-cong', 'Cập nhật thành công');
    }
    public function search(Request $request)
    {
        $data['news'] = news::where('name', 'like', '%' . $request->q . '%')->paginate(15);
        $data['total_cmt_blog'] = comments::where('state', 0)->count();
        $data['total_notif'] = $data['total_cmt_blog'];
        return view('backend.news.index', $data);
    }
}
