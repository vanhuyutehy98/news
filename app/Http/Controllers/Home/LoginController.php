<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\categories;
use App\Models\news;
use App\Models\settings;
use App\Models\users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login()
    {
        $data['categories'] = categories::all();
        $data['setting'] = settings::find(1);
        $data['newViewsFooter'] = news::orderBy('view','DESC')->take(2)->get();
        $data['categoryFooter'] =  categories::orderBy('created_at', 'DESC')->take(6)->get();
        return view('home.my-account', $data);
    }
    public function dk()
    {
        $data['categories'] = categories::all();
        $data['setting'] = settings::find(1);
        $data['newViewsFooter'] = news::orderBy('view','DESC')->take(2)->get();
        $data['categoryFooter'] =  categories::orderBy('created_at', 'DESC')->take(6)->get();
        return view('home.dk', $data);
    }
    public function dkPost(Request $request)
    {
        $user = new users();
        $user->name = $request->name;
        $user->image = 'no-img.png';
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        return back()->with('thong-bao-thanh-cong', 'Tạo thành công người dùng');;
    }
    public function loginPost(Request $request)
    {
        if ($request->remember_me) {
            $remember = $request->remember_me;
            if (Auth::guard('guest')->attempt([
                'email' => $request->email,
                'password' => $request->password
            ], $remember)) {
                return redirect()->route('index');
            } else {
                return redirect()->back()->withInput()->with('thongbao', 'Tài khoản khoặc mật khẩu không chính xác!');
            }
        } else {
            if (Auth::guard('guest')->attempt([
                'email' => $request->email,
                'password' => $request->password
            ])) {
                return redirect()->route('index');
            } else {
                return redirect()->back()->withInput()->with('thongbao', 'Tài khoản khoặc mật khẩu không chính xác!');
            }
        }
    }
    public function Logout()
    {
        Auth::guard('guest')->logout();
        return redirect()->route('index');
    }

}
