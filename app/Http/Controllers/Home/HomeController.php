<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\categories;
use App\Models\comments;
use App\Models\news;
use App\Models\settings;
use App\Models\tags;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        Carbon::setLocale('vi');
        $data['setting'] = settings::find(1);
        $data['newActives'] = news::where('active', '1')->get();
        $data['categoryActives'] = categories::where('active', '1')->get();
        $data['news'] = news::orderBy('created_at', 'DESC')->where('active', 1)->take(10)->get();
        $data['newsDays'] = news::whereDate('created_at', Carbon::now())->where('active', 1)->take(6)->get();
        $data['newViews'] = news::orderBy('view','DESC')->where('active', 1)->take(5)->get();
        $data['setting'] = settings::find(1);
        $data['categories'] = categories::all();
        $data['newViewsFooter'] = news::orderBy('view','DESC')->where('active', 1)->take(2)->get();
        $data['categoryFooter'] =  categories::orderBy('created_at', 'DESC')->take(6)->get();
        return view('home.index', $data);
    }
    public function detail($slug)
    {
        Carbon::setLocale('vi');
        $data['setting'] = settings::find(1);
        $data['new'] = news::where('slug', $slug)->first();
        $data['new']->view = $data['new']->view + 1;
        $data['new']->save();
        $data['tags'] = $data['new']->tags()->get();
        $data['comments'] = $data['new']->comment()->get();
        $category = categories::find($data['new']->cat_id);
        $data['relatedArticles'] = $category->news()->where('active', 1)->get();
        $data['newViews'] = news::orderBy('view','DESC')->where('active', 1)->take(5)->get();
        $data['newViewsFooter'] = news::orderBy('view','DESC')->where('active', 1)->take(2)->get();
        $data['categoryFooter'] =  categories::orderBy('created_at', 'DESC')->take(6)->get();
        $data['categories'] = categories::all();
        return view('home.detail', $data);
    }
    public function category(Request $request, $slug)
    {
        Carbon::setLocale('vi');
        $data['oneNew'] =  news::orderBy('created_at', 'DESC')->take(1)->first();
        $data['categorie'] = categories::where('slug', $slug)->first();
        if ($request->tag) {
            $tag = tags::find($request->tag);
            $data['news'] = $tag->news()->where('active', 1)->paginate(10);
        }else{
            $data['news'] =  $data['categorie']->news()->where('active', 1)->paginate(10);
        }
        $data['setting'] = settings::find(1);
        $data['newViews'] = news::orderBy('view','DESC')->where('active', 1)->take(5)->get();
        $data['newViewsFooter'] = news::orderBy('view','DESC')->where('active', 1)->take(2)->get();
        $data['categories'] = categories::all();
        $data['categoryFooter'] =  categories::orderBy('created_at', 'DESC')->take(6)->get();
        return view('home.business', $data);
    }
    public function addComment(Request $request, $id)
    {
        $comment = new comments();
        $comment->state = 0;
        $comment->new_id  = $id;
        $comment->name  = Auth::guard('guest')->user()->name;
        $comment->comnent  = $request->comment;
        $comment->save();
        return redirect()->back()->with('thong-bao-thanh-cong', 'Đã thêm thành công bình luận');
    }
    public function page404()
    {
        return view('home.page-404');
    }
    public function search(Request $request)
    {
        Carbon::setLocale('vi');
        $data['oneNew'] =  news::orderBy('created_at', 'DESC')->take(1)->first();
        $data['categorie'] = categories::where('active', 1)->first();
        $data['news'] = news::where('name', 'like', '%' . $request->search . '%')->paginate(10);
        $data['setting'] = settings::find(1);
        $data['newViews'] = news::orderBy('view','DESC')->where('active', 1)->take(5)->get();
        $data['newViewsFooter'] = news::orderBy('view','DESC')->where('active', 1)->take(2)->get();
        $data['categories'] = categories::all();
        $data['categoryFooter'] =  categories::orderBy('created_at', 'DESC')->take(6)->get();
        return view('home.search', $data);
    }

}
