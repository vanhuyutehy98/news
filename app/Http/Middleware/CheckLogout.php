<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class CheckLogout
{
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            return redirect()->route('admin.index');
        }
        else {
            return $next($request);
        }
    }
}
