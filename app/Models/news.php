<?php

namespace App\Models;

use App\Model\tag_news;
use Illuminate\Database\Eloquent\Model;

class news extends Model
{
    public function users()
    {
        return $this->belongsTo(userAdmin::class, 'user_adnin', 'id');
    }
    public function tags()
    {
        return $this->belongsToMany(tags::class, 'tag_new', 'new_id', 'tag_id');
    }
    public function category()
    {
        return $this->belongsTo(categories::class, 'cat_id', 'id');
    }
    public function comment()
    {
        return $this->hasMany(comments::class, 'new_id', 'id');
    }
}
