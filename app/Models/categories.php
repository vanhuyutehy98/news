<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class categories extends Model
{
    public function users()
    {
        return $this->belongsTo(userAdmin::class, 'use_id', 'id');
    }
    public function news()
    {
        return $this->hasMany(news::class, 'cat_id', 'id');
    }
}
