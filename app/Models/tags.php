<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tags extends Model
{
    public function news()
    {
        return $this->belongsToMany(news::class, 'tag_new', 'new_id', 'tag_id');
    }
}
